<?php

function saml_service_admin_path_builder( $context, $replacements ){
  return str_replace( array_keys($replacements), $replacements, $context['path_template'] );
}

/**
 * Page callback for admin/config/services/saml
 */
function saml_service_admin_descriptor_page( $path_template, $entityId = null, $task = null, $descriptorClassName = null, $sub_task = null ){
  if( is_null($entityId ) ){
    return saml_service_admin_federation_page();
  }

  $form_state = array(
    'path_builder'  => 'saml_service_admin_path_builder',
    'path_template' => $path_template,
    'entityId'      => $entityId
    
  );
  
  if($entityId == "add"){
    return drupal_build_form( "saml_service_add_entity_form", $form_state );
  }
  
  
  if( !is_null($descriptorClassName) ){
    $descriptorClassName = base64_decode($descriptorClassName);
  }
  
  if( $entity = SAML::getInstance()->getEntityById( $entityId ) ){
    $args = func_get_args();
    array_splice( $args, 0, 5 );
    
    return call_user_func_array( 'saml_service_admin_descriptor_router', array_merge( array($form_state, $entity, $task, $descriptorClassName, $sub_task), $args ) );
  }
  return MENU_NOT_FOUND;
}

function saml_service_admin_descriptor_router( $state_defaults, $entity, $task = null, $descriptorClassName = null, $sub_task = null ){
  $args = func_get_args();
  array_splice( $args, 0, 5 );
  
  $form_state = $state_defaults+array(
    'arguments' => $args,
    'entity'    => $entity
  );
  
  if( !is_null($task) ){
    if( $task == "delete" ){
      return drupal_build_form( 'saml_service_admin_entity_delete_form', $form_state );
    }
  
    if( !is_null($descriptorClassName) ){

      $role = saml_service_role_info($descriptorClassName);
     
      if( !$role ){
	return MENU_NOT_FOUND;
      }
      
      $descriptors = $form_state['entity']->getDescriptor()->getDescriptors();

      reset( $descriptors );
      while( list( , $entity_descriptor) = each($descriptors) ){
	if( $entity_descriptor instanceof $descriptorClassName ){
	  $form_state['descriptor'] = $entity_descriptor;
	  break;
	}
      }
      
      $form_state['entity_path'] = $form_state['path_builder']( $form_state, array( '%id' => $form_state['entity']->getId() ) );
      
      $form_state['descriptor_path'] = $form_state['path_builder']( $form_state, array( '%id' => $form_state['entity']->getId() ) )."/role/".base64_encode($descriptorClassName );
      
      $form_state['task_path'] = $form_state['path_builder']( $form_state, array( '%id' => $form_state['entity']->getId() ) )."/role/".base64_encode($descriptorClassName )."/{$sub_task}";
      
      if( !is_null($sub_task) ){
	if( array_key_exists( $sub_task, $role['tasks'] ) ){
	  if( array_key_exists( 'file', $role['tasks'][$sub_task] ) ){
	    $file = $role['tasks'][$sub_task]['file'];
	    if(
	      array_key_exists( 'path', $role['tasks'][$sub_task] ) &&
	      !empty($role['tasks'][$sub_task]['path'])
	    ){
	      $file = $role['tasks'][$sub_task]['path'] . "/{$file}";
	    }

	    if( file_exists($file) ){
	      require_once( $file );
	    }
	    else if(
	      array_key_exists( 'path', $role ) &&
	      !empty($role['path'])
	    ){
	      $file = $role['path'] . "/{$file}";
	    }
	    
	    if( file_exists($file) ){
	      require_once( $file );
	    }
	  }

	  return call_user_func_array( $role['tasks'][$sub_task]['callback'], array_merge( $role['tasks'][$sub_task]['callback arguments'],  array( &$form_state ) ) );
	}

	return MENU_NOT_FOUND;
      }
    }
  }
  
  return drupal_build_form( 'saml_service_admin_entity_form', $form_state );
}

function saml_service_admin_entity_form( $form, $form_state ){

  $form = array();
  
  $form['entityid'] = array(
    '#title'         => t('EntityId'),
    '#type'          => 'textfield',
    '#default_value' => $form_state['entity']->getEntityId(),
    '#disabled'      => $form_state['entity']->getId() != 1
  );
  
  $form['name'] = array(
    '#title' => t('Name'),
    '#type'  => 'textfield',
    '#default_value' => $form_state['entity']->getName()
  );
  
  $form['roles'] = array(
    '#title' => t('Role(s)'),
    '#description' => t('A list of all the role(s) that this application provides services for.'),
    '#type'  => 'fieldset',
  );
  
  $role_table = array(
    'header' => array(
      t('Name'),
      array(
	'data' => t('Operation(s)')
      )
    ),
    'rows' => array(
    )
  );
  
  $roles = saml_service_role_info();
  
  $descriptors = $form_state['entity']->getDescriptor()->getDescriptors();
  reset( $descriptors );
  while( list( ,$descriptor) = each($descriptors) ){
    reset($roles);
    while( list($index,$role) = each($roles) ){
      if( get_class($descriptor) == $role['descriptor']['class'] ){
	$row = array(
	  $role['name'],
	);
	
	if( $form_state['entity']->getId() ){
	  reset($role['tasks']);
	  while( list( $task_id ,$task) = each($role['tasks']) ){
	    if( $task['type'] == MENU_CALLBACK ){
	      continue;
	    }
	    
	    if( isset( $task['access'] ) ){
	      if( $task['access'] !== true && function_exists($task['access']) && !$task['access']( $form_state['entity'], $descriptor, $task_id, $task ) ){
		continue;
	      }
	    }
	    
	    $row[] = l( $task['title'], $form_state['path_builder']( $form_state, array( '%id' => $form_state['entity']->getId() ) )."/role/".base64_encode( $role['descriptor']['class'] )."/{$task_id}" );
	  }
	  
	  if( count($row) > 2 ){
	    if( !array_key_exists( 'colspan', $role_table['header'][1] ) ){
	      $role_table['header'][1]['colspan'] = 2;
	    }
	    $role_table['header'][1]['colspan'] = max( $role_table['header'][1]['colspan'], count($row)-1 );
	  }
	}
	$role_table['rows'][] = $row;
	continue 2;
      }
    }
  }
  
  $form['roles'][] = array(
    '#markup' => theme('table', $role_table)
  ); unset($role_table);
  
  $form['download'] = array(
    '#type'   => 'link',
    '#title'  => t('Download Metadata'),
    '#href'   =>  $form_state['path_builder']( $form_state, array( '%id' => $form_state['entityId'] ) )."/descriptor",
    '#attributes' => array( 'class' => array( "button" ) ),
//     '#access' => $form_state['entity']->getId() == 1
  );
  
  $form['save'] = array(
    '#type'   => 'submit',
    '#value'  => t('Save Entity'),
    '#submit' => array( 'saml_service_admin_entity_form_submit' )
  );
  
  if( arg(4) ){
    $form['cancel'] = array(
      '#markup' => l( t( 'Cancel' ),  $form_state['path_builder']( $form_state, array( '%id' => $form_state['entityId'] ) ), array( "attributes" => array( "class" => array( "button" ) ) )  )
    );
  }
  
  return $form;
}

function saml_service_admin_entity_form_submit( $form, &$form_state ){
  $entity = $form_state['entity'];
  
  $entity->setName( $form_state['values']['name'] );
  
  if( !$form['entityid']['#disabled'] ){
    $entity_descriptor = $entity->getDescriptor();
    $entity = new SamlEntity(
      $entity->getId(),
      $form_state['values']['entityid'],
      $entity->getName()
    );
    $entity_descriptor->setEntityID($entity->getEntityId());
    $entity->setDescriptor( $entity_descriptor ); unset($entity_descriptor);
  }
  
  $form_state['entity'] = SAML::getInstance()->saveEntity($entity);
  drupal_set_message( t( "%entity saved.", array( '%entity' => $entity->getName()."( ".$entity->getEntityId(). " )" ) ) );
}

function saml_service_admin_federation_page(){
$ret = array();
  
  $table = array(
    'header' => array(
      array(
	'data'  => t('Name'),
	'field' => 'e.name',
	'sort'  => 'asc'
      ),
      array(
	'data'  => t('EntityId'),
	'field' => 'e.entityid',
      ),
      array(
	'data'    => t('Operations'),
	'colspan' => 3
      )
    ),
    'rows' => array()
  );
  
  $query = db_select("saml_entity","e")->extend('PagerDefault')->extend('TableSort')->fields('e')->condition('e.id', 1, ">")->limit(25);
  
  $query->orderByHeader( $table['header'] );
  
  $result_list = $query->execute();
  
  $roles = saml_service_role_info();
  
  while( $result = $result_list->fetchAssoc() ){
    $row = array();
    
    $row[] = $result['name'];
    $row[] = $result['entityid'];
    
    $row[] = l( t('edit'), "admin/config/services/saml/federation/{$result['id']}");
    $row[] = l( t('delete'), "admin/config/services/saml/federation/{$result['id']}/delete");
    
    $table['rows'][] = $row; unset( $row );
  }
  
  if( !count($table['rows']) ){
    $table['rows'][] = array(
      array(
	'colspan' => 5,
	'data'    => t('No entitie(s) defined.')
      )
    );
  }
  
  $ret['list']['#markup'] = theme('table', $table);
  
  unset($table);
  $ret['pager']['#markup'] = theme('pager');
  
  return $ret;
}


function saml_service_add_entity_form( $form, &$form_state ){
  if( !array_key_exists('storage', $form_state) || is_null($form_state['storage']) ){
    $form_state['storage'] = array(
      'step' => "import_type"
    );
  }
  
  if( !array_key_exists( "pages.inc:saml_service:includes/admin", $form_state['build_info']['files'] ) ){
    $form_state['build_info']['files']["pages.inc:saml_service:includes/admin"] = array(
      'type'   => "pages.inc",
      'module' => 'saml_service',
      'name'   => 'includes/admin'
    );
  }
  
  switch($form_state['storage']['step']){
    case "supply":
      $callback = "saml_service_add_entity_form_supply_{$form_state['storage']['type']}";
      if( function_exists($callback) ){
	$form = $callback($form, $form_state);
      }
      else {
	drupal_set_message( t("Unknown import type %type", array( '%type' => $form_state['storage']['type'] )), 'error' );
	$form = saml_service_add_entity_form_import_type($form, $form_state);
      }
    break;
    case "configure":
      $entityDescriptor = SAML::getInstance()->fromXML(
	trim($form_state['storage']['entityDescriptor']),
	array(
	  SAML20Codec::METADATA_URN => array(
	    'KeyDescriptor' => new SAMLServiceKeyDescriptorDecoder()
	  )
	)
      );
      $entity = new SamlEntity( 0, $entityDescriptor->getEntityId(), $entityDescriptor->getEntityId() );
      $entity->setDescriptor($entityDescriptor);
      
      module_load_include( "pages.inc", "saml_service","includes/admin" );
      $form = saml_service_admin_descriptor_router($form_state,$entity);
      
      array_unshift( $form['save']['#submit'], 'saml_service_add_entity_form_configure_prepare_submit' );
      array_push( $form['save']['#submit'], 'saml_service_add_entity_form_configure_finalize_submit' );
    break;
    default:
      $form = saml_service_add_entity_form_import_type($form, $form_state);
  }
  
  return $form;
}

/**
 * A step from the saml_service_admin_federation_form form.
 */
function saml_service_add_entity_form_import_type($form, &$form_state) {
  
  $form['type'] = array(
    '#title'   => t('Please choose how you will supply the metadata'),
    '#type'    => 'radios',
    '#options' => array(
      'xml' => t('Upload XML'),
      'url' => t('Specify URL')
    ),
    '#required' => true
  );
  
  $form['next'] = array(
    '#type'   => 'submit',
    '#value'  => t('Next'),
    '#submit' => array( 'saml_service_add_entity_form_import_type_submit' )
  );
  return $form;
}

/**
 * Submit callback for form step saml_service_add_entity_form_import_type.
 */
function saml_service_add_entity_form_import_type_submit($form, &$form_state) {
  $form_state['rebuild'] = true;
  
  $form_state['storage']['type'] = $form_state['values']['type'];
  $form_state['storage']['step'] = 'supply';
}

/**
 * A step from the saml_service_admin_federation_form form.
 */
function saml_service_add_entity_form_supply_xml($form, &$form_state) {
  $form['entityDescriptor'] = array(
    '#title'    => t('XML'),
    '#type'     => 'textarea',
    '#required' => true
  );
  
  $form['next'] = array(
    '#type'   => 'submit',
    '#value'  => t('Next')
  );
  
  $form['#validate'] = array(
    'saml_service_add_entity_form_supply_xml_validate'
  );
  
  $form['#submit'] = array(
    'saml_service_add_entity_form_supply_submit'
  );
  
  return $form;
}

/**
 * Validation callback for form step saml_service_federation_admin_form_supply_xml.
 */
function saml_service_add_entity_form_supply_xml_validate($form, &$form_state) {
  $entityDescriptor = SAML::getInstance()->fromXML(
    trim($form_state['values']['entityDescriptor']),
    array(
      SAML20Codec::METADATA_URN => array(
	'KeyDescriptor' => new SAMLServiceKeyDescriptorDecoder()
      )
    )
  );

  if( !$entityDescriptor || !($entityDescriptor instanceof SAML20CodecEntityDescriptor) ){
    form_set_error( 'entityDescriptor', t('Could not understand the supplied XML.') );
  }

}

/**
 * Submit callback for form step saml_service_federation_admin_form_supply_xml.
 */
function saml_service_add_entity_form_supply_submit($form, &$form_state) {
  $form_state['rebuild'] = true;
  $form_state['storage']['step'] = 'configure';
  $form_state['storage']['entityDescriptor'] = $form_state['values']['entityDescriptor'];

}

/**
 * Submit callback for form step configure.
 */
function saml_service_add_entity_form_configure_prepare_submit($form, &$form_state){
  $entityDescriptor = SAML::getInstance()->fromXML(
    trim($form_state['storage']['entityDescriptor']),
    array(
      SAML20Codec::METADATA_URN => array(
	'KeyDescriptor' => new SAMLServiceKeyDescriptorDecoder()
      )
    )
  );
  
  $form_state['entity'] = new SamlEntity( 0, $entityDescriptor->getEntityId(), $entityDescriptor->getEntityId() );
  $form_state['entity']->setDescriptor($entityDescriptor); unset($entityDescriptor);
}

/**
 * Submit callback for form step configure.
 */
function saml_service_add_entity_form_configure_finalize_submit($form, &$form_state){
  $form_state['redirect'] = "admin/config/services/saml/federation/".$form_state['entity']->getId();
}

/**
 * Form callback for the delete action
 */
function saml_service_admin_entity_delete_form( $form, $form_state ){

  $form = array();
  
  $form['entityid'] = array(
    '#title'         => t('EntityId'),
    '#type'          => 'item',
    '#markup'        => $form_state['entity']->getEntityId(),
  );
  
  $form['name'] = array(
    '#title'  => t('Name'),
    '#type'   => 'item',
    '#markup' => $form_state['entity']->getName()
  );
  
  $form['roles'] = array(
    '#title' => t('Role(s)'),
    '#description' => t('A list of all the role(s) that this application provides services for.'),
    '#type'  => 'fieldset',
  );
  
  $role_table = array(
    'header' => array(
      t('Name')
    ),
    'rows' => array(
    )
  );
  
  $roles = saml_service_role_info();
  
  $descriptors = $form_state['entity']->getDescriptor()->getDescriptors();
  reset( $descriptors );
  while( list( ,$descriptor) = each($descriptors) ){
    reset($roles);
    while( list($index,$role) = each($roles) ){
      if( get_class($descriptor) == $role['descriptor']['class'] ){
	$row = array(
	  $role['name'],
	);
	$role_table['rows'][] = $row;
	continue 2;
      }
    }
  }
  
  $form['roles'][] = array(
    '#markup' => theme('table', $role_table)
  ); unset($role_table);
  
  
  $form['confirm'] = array(
    '#type'   => 'submit',
    '#value'  => t('Yes'),
    '#submit' => array( 'saml_service_admin_entity_delete_form_submit' )
  );
  
  $form['cancel'] = array(
    '#type'   => 'link',
    '#title' => t( 'No' ),
    '#href'   => "admin/config/services/saml/federation",
    "#attributes" => array( "class" => array( "button" ) )
  );
  
  return $form;
}

function saml_service_admin_entity_delete_form_submit( $form, &$form_state ){
  if( SAML::getInstance()->deleteEntity( $form_state['entity']->getEntityId() ) ){
    drupal_set_message( t( "%entity was deleted", array( "%entity" => $form_state['entity']->getEntityId() ) ) );
  }
  else {
    drupal_set_message( t("Unable to delete entity"), 'error' );
  }
  unset($form_state['storage']);
  $form_state['redirect'] = $form_state['path_builder']( $form_state, array( '%id' => $form_state['entityId'] ) );
}
