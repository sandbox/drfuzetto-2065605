<?php

/**
 * Page callback for the path admin/config/development/saml
 */
function saml_service_development(){
  return array(
    'logging' => array( '#weight' => -1 ) + drupal_get_form( "saml_service_development_general_form" ),
    'decode'  => array( '#weight' => 2 ) + drupal_get_form( "saml_service_development_decode_form" ),
  );
}

function saml_service_development_general_form( $form, $form_state ){
  $form = array();
  
  $form['logging_fieldset'] = array(
    '#title' => t('Logging'),
    '#type' => 'fieldset'
  );
  
  
  $form['logging_fieldset']['logging'] = array(
    '#type'     => 'select',
    '#options'  => array(
      SAML_SERVICE_LOG_MESSAGE_CONTEXT => t("SAML Message Context")
    ),
    '#default_value' => array(),
    '#multiple' => true
  );
  
  if( SAMLServiceLogging::hasFlag(SAML_SERVICE_LOG_MESSAGE_CONTEXT) ){
    $form['logging_fieldset']['logging']['#default_value'][] = SAML_SERVICE_LOG_MESSAGE_CONTEXT;
  }
  
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array( 'saml_service_development_general_form_submit' )
  );
  
  
  return $form;
}

function saml_service_development_general_form_submit( $form, &$form_state ){
  drupal_set_message( t('Changes Saved') );
  
  $log = 0;
  if( $form_state['values']['logging'] && is_array($form_state['values']['logging']) ){
    foreach($form_state['values']['logging'] as $bit){
      $log = $log | $bit;
    }
  }

  variable_set("saml_service:log" , $log); unset($log);
}

function saml_service_development_decode_form( $form, $form_state ){
  
  $form['decode_fieldset'] = array(
    '#title' => t('Decode'),
    '#type' => 'fieldset'
  );
  
  if( isset($form_state['storage']) && isset($form_state['storage']['decoded_message']) && !empty($form_state['storage']['decoded_message']) ){
    $form['decode_fieldset']['decoded_message'] = array(
      '#type' => 'item',
      '#prefix' => "<pre>",
      '#suffix' => "</pre>",
      '#markup' => htmlentities($form_state['storage']['decoded_message'])
    );
  }
  
  $form['decode_fieldset']['encoded_message'] = array(
    '#title' => t('Encoded String'),
    '#type'  => 'textarea',
    '#required' => true
  );
  
  $form['decode_fieldset']['decode'] = array(
    '#type' => 'submit',
    '#value' => t('Decode'),
    '#submit' => array( 'saml_service_development_decode_form_submit' )
  );
  
  return $form;
}

function saml_service_development_decode_form_submit( $form, &$form_state ){
  $form_state['rebuild'] = true;
  $form_state['storage'] = array();
  $form_state['storage']['decoded_message'] = base64_decode( $form_state['values']['encoded_message'] );
}

function saml_service_development_verify_form( $form, $form_state ){
  
  $form['verify_fieldset'] = array(
    '#title' => t('Verify Message'),
    '#type' => 'fieldset'
  );
  
  $form['verify_fieldset']['message'] = array(
    '#title' => t('Message'),
    '#type'  => 'textarea',
    '#required' => true
  );
  
  $form['verify_fieldset']['verify'] = array(
    '#type' => 'submit',
    '#value' => t('Verify'),
    '#submit' => array( 'saml_service_development_verify_form_submit' )
  );
  
  if( isset($form_state['storage']['report']) ){
    $form['report'] = $form_state['storage']['report']+array(
      '#type'  => 'fieldset',
      '#title' => t('Results')
    );
  }
  
  return $form;
}

function saml_service_development_verify_form_submit( $form, &$form_state ){
  $form_state['rebuild'] = true;
  
  try {
  
    $message = SAML::fromXML( $form_state['values']['message'] );
    $form_state['storage'] = array(
      'report' => array(
	'type' => array(
	  '#type' => 'item',
	  '#title' => t("Message Type:"),
	  '#markup' => t('Unrecognized'),
	)
      )
    );
    
    
    
    if( $message instanceof SOAPMessage ){
      $message = $message->getBody();
    }
    
    if( $message instanceof  SAML20CodecAuthnRequest ){
      $form_state['storage']['report']['type']['#markup'] = t('AuthnRequest');
      
      if( ($issuer = $message->getIssuer() ) && ( $entityId = $issuer->getValue() ) && !empty($entityId) ){
	$form_state['storage']['report']['issuer'] = array(
	  '#type' => 'item',
	  '#title' => t("Issuer:"),
	  '#markup' => t("%issuer, not in federation", array( "%issuer" => $issuer->getValue() ) )
	);
	
	if( $messageEntity = Saml::getInstance()->getEntity( $entityId ) ){
	  $form_state['storage']['report']['issuer'] = array(
	    '#type' => 'item',
	    '#title' => t("Issuer:"),
	    '#markup' => t("%issuer, located in federation", array( "%issuer" => $issuer->getValue() ) ),
	  );
	  
	  $form_state['storage']['report']['issued'] = array(
	    '#type' => 'item',
	    '#title' => t("Issued Timestamp:"),
	    '#markup' => format_date( $message->getIssueInstant(), 'long' ),
	  );
	  
	  $form_state['storage']['report']['request_signature'] = array(
	    '#type' => 'item',
	    '#title' => t("Request Signature:"),
	    '#markup' => t('Not Located'),
	  );
	  
	  if($signature = $message->getSignature()){
	    $form_state['storage']['report']['request_signature']['#markup'] = t('Present but not validated');
	    
	    $descriptors = $messageEntity->getDescriptor()->getDescriptors();
	    reset($descriptors);
	    while( list( ,$descriptor) = each($descriptors) ){
	      $keyDescriptors = $descriptor->getKeyDescriptors();
	      reset($keyDescriptors);
	      while( list(,$keyDescriptor) = each($keyDescriptors) ){
		if( $keyDescriptor->getUse() == SAML20CodecKeyDescriptor::USE_SIGNING ){
		  try {
		    $signature->validateElement( $keyDescriptor->getKeyPair() );
		    $form_state['storage']['report']['request_signature']['#markup'] = t('Present and valid');
		    break;
		  } catch( Exception $e ){
		  }
		  
		}
	      }
	    }
	  }
	}
      }
    }
    
    if( $message instanceof SAML20CodecResponse ){
      $form_state['storage']['report']['type']['#markup'] = t('Response');
      
      if( ($issuer = $message->getIssuer() ) && ( $entityId = $issuer->getValue() ) && !empty($entityId) ){
	$form_state['storage']['report']['issuer'] = array(
	  '#type' => 'item',
	  '#title' => t("Issuer:"),
	  '#markup' => t("%issuer, not in federation", array( "%issuer" => $issuer->getValue() ) )
	);

	if( $messageEntity = Saml::getInstance()->getEntity( $entityId ) ){
	  $form_state['storage']['report']['issuer'] = array(
	    '#type' => 'item',
	    '#title' => t("Issuer:"),
	    '#markup' => t("%issuer, located in federation", array( "%issuer" => $issuer->getValue() ) ),
	  );
	
	  $form_state['storage']['report']['issued'] = array(
	    '#type' => 'item',
	    '#title' => t("Issued Timestamp:"),
	    '#markup' => format_date( $message->getIssueInstant(), 'long' ),
	  );
	  
	  $form_state['storage']['report']['response_signature'] = array(
	    '#type' => 'item',
	    '#title' => t("Response Signature:"),
	    '#markup' => t('Not Located'),
	  );
	
	  if($signature = $message->getSignature()){
	    $form_state['storage']['report']['response_signature']['#markup'] = t('Present but not validated');
	    
	    $descriptors = $messageEntity->getDescriptor()->getDescriptors();
	    reset($descriptors);
	    while( list( ,$descriptor) = each($descriptors) ){
	      $keyDescriptors = $descriptor->getKeyDescriptors();
	      reset($keyDescriptors);
	      while( list(,$keyDescriptor) = each($keyDescriptors) ){
		if( $keyDescriptor->getUse() == SAML20CodecKeyDescriptor::USE_SIGNING ){
		  try {
		    $signature->validateElement( $keyDescriptor->getKeyPair() );
		    $form_state['storage']['report']['response_signature']['#markup'] = t('Present and valid');
		    break;
		  } catch( Exception $e ){
		  }
		  
		}
	      }
	    }
	  }
	  
	  $form_state['storage']['report']['assertions'] = array(
	    '#type' => 'item',
	    '#title' => t("Assertion(s):"),
	    '#markup' => t('None Located'),
	  );
	  
	  if( $assertions = $message->getAssertions() ){
	    $form_state['storage']['report']['assertions']['#markup'] = t( "!count assertion(s) located", array( "!count" => count($assertions) ) );
	  
	    reset($assertions);
	    while( list( ,$assertion ) = each( $assertions ) ){
	      $assertion_report = array(
		'signature' => array(
		  '#type' => 'item',
		  '#title' => t("Signature:"),
		  '#markup' => t('Not Located'),
		),
		'conditions' =>  array(
		  '#type' => 'item',
		  '#title' => t("Condition(s):"),
		  'status' => array(
		    '#markup' => t('No condition(s) supplied')
		  ),
		  'errors' => array()
		),
	      );
	      
	      if($signature = $assertion->getSignature()){
		$assertion_report['signature']['#markup'] = t('Present but not validated');
		
		$descriptors = $messageEntity->getDescriptor()->getDescriptors();
		reset($descriptors);
		while( list( ,$descriptor) = each($descriptors) ){
		  $keyDescriptors = $descriptor->getKeyDescriptors();
		  reset($keyDescriptors);
		  while( list(,$keyDescriptor) = each($keyDescriptors) ){
		    if( $keyDescriptor->getUse() == SAML20CodecKeyDescriptor::USE_SIGNING ){
		      try {
			$signature->validateElement( $keyDescriptor->getKeyPair() );
			$assertion_report['signature']['#markup'] = t('Present and valid');
			break;
		      } catch( Exception $e ){
		      }
		      
		    }
		  }
		}
	      }
      
	      if( $conditions = $assertion->getConditions() ){
		$assertion_report['conditions']['status']['#markup'] = t('Located condition(s) with no errors');

		if( ($notBefore = $conditions->getNotBefore()) && !intval($notBefore) && time() < $notBefore ){
		  $assertion_report['conditions']['errors']['notbefore']['#markup'] = t('NotBefore value is in the future');
		}
		
		if( ($notOnOrAfter = $conditions->getNotOnOrAfter()) ){
		  $assertion_report['conditions']['errors']['notafter']['#markup'] = t('NotAfter value is in the past');
		}
		
		if( $assertion_report['conditions']['errors'] ){
		  $assertion_report['conditions']['status']['#markup'] = t('Located condition(s), please not errors below');
		}
		
	      } unset($conditions);
	      
	      $form_state['storage']['report'][ $assertion->getId() ] = $assertion_report+array(
		'#type'        => 'fieldset',
		'#title'       => t( "Assertion #%id", array( "%id" => $assertion->getId() ) ),
		'#collapsible' => true,
		'#collapsed'   => true
	      ); unset($assertion_report);
	    }
	  }
	}
      }
    }
  
  } catch ( Exception $e ){
  }
}