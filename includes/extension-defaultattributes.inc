<?php

class SAML20ExtenstionAssertionAttributesCodec {
  const DEFAULT_URN = "urn:bluecoat:names:SAML:2.0:extenstion:assertionattributes";
  const DEFAULT_SCHEMA_LOCATION = "https://authentication.bluecoat.com/schema/saml20-extension-assertion-attributes.xsd";
}

class SAML20ExtenstionAssertionAttributesEncoder extends AbstractXmlEncoder {
  public function xmlEncode( XmlEncodeContext $context, $node ){
    if( $node instanceof SAML20ExtenstionAssertionAttributes ){
      return $this->processAssertionAttributes( $context, $node );
    }
    if( $node instanceof SAML20ExtenstionAssertionAttribute ){
      return $this->processAssertionAttribute( $context, $node );
    }
  }
  
  private function processAssertionAttributes( XmlEncodeContext $context, SAML20ExtenstionAssertionAttributes $source  ){   
    $ret = $context->getNode();
    if( is_null($ret) ){
      if( $context->getDocument()->firstChild ){
	$ret = $context->createElementNS( SAML20ExtenstionAssertionAttributesCodec::DEFAULT_URN, "AssertionAttributes", "bc-saml" );
	//add this namespace to the root element
	$context->getDocument()->firstChild->setAttributeNS(XmlCodec::DEFAULT_URN, "xmlns:bc-saml", SAML20ExtenstionAssertionAttributesCodec::DEFAULT_URN );
	$context->getDocument()->firstChild->setAttributeNS(XmlCodec::DEFAULT_URN, "xmlns:saml", SAML20Codec::ASSERTION_URN );
      }
      else {
	$ret = $context->createDocumentElementNS( SAML20ExtenstionAssertionAttributesCodec::DEFAULT_URN, "AssertionAttributes", "bc-saml" );
	$ret->setAttributeNS(XmlCodec::DEFAULT_URN, "xmlns:bc-saml", SAML20ExtenstionAssertionAttributesCodec::DEFAULT_URN );
	$ret->setAttributeNS(XmlCodec::DEFAULT_URN, "xmlns:saml", SAML20Codec::ASSERTION_URN );
      }
    }
    
    $this->processList( $context, $ret, $source->getAttributes() );
    
    return $ret;
  }
  
  private function processAssertionAttribute( XmlEncodeContext $context, SAML20ExtenstionAssertionAttribute $source  ){   
    $domNode = $context->createElementNS( SAML20ExtenstionAssertionAttributesCodec::DEFAULT_URN, "Attribute", "bc-saml" );
    if( ($value = $source->getName()) ){
      if( empty($value) ){
	throw new Exception("Name is a required attribute.");
      }
      $domNode->setAttribute("Name", $value );
    } unset($value);
    
    if( ($value = $source->getNameFormat()) && !empty($value) ){
      $domNode->setAttribute( "NameFormat", $value );
    } unset($value);
    
    if( ($value = $source->getFriendlyName()) && !empty($value) ){
      $domNode->setAttribute( "FriendlyName", $value );
    } unset($value);
    
    $domDocument = $context->getDocument();
    $children = $source->getValues();
    reset($children);
    while( list( , $child) = each( $children ) ){
      if( is_string($child) ){
	$childNode = $domDocument->createElementNS(SamlConst::NS_2_0_ASSERTION, "saml:AttributeValue", $child );
	
	//add XMLSchema namespace if it doesn't exist
	$xsi_prefix = $domDocument->lookupPrefix("http://www.w3.org/2001/XMLSchema-instance");
	if( empty($xsi_prefix) ){
	  $rootNode = $domDocument->childNodes->item(0);
	  if( is_null($rootNode) ){
	    $rootNode = $domParent;
	  }
	  $xsi_prefix="xsi";
	  $rootNode->setAttributeNS( 'http://www.w3.org/2000/xmlns/', "xmlns:{$xsi_prefix}", "http://www.w3.org/2001/XMLSchema-instance" );
	  unset($rootNode);
	}
	
	$xs_prefix = $domDocument->lookupPrefix("http://www.w3.org/2001/XMLSchema");
	if( empty($xs_prefix) ){
	  $rootNode = $domDocument->childNodes->item(0);
	  if( is_null($rootNode) ){
	    $rootNode = $domParent;
	  }
	  $xs_prefix = "xs";
	  $rootNode->setAttributeNS( 'http://www.w3.org/2000/xmlns/', "xmlns:{$xs_prefix}", "http://www.w3.org/2001/XMLSchema" );
	  unset($rootNode);
	}
	
	$childNode->setAttributeNS("http://www.w3.org/2001/XMLSchema-instance", "{$xsi_prefix}:type", "{$xs_prefix}:string");
	$domNode->appendChild($childNode);
      }
      else if( $childNode = $this->xmlEncode($child, $domNode) ){
	$domNode->appendChild($childNode);
      } unset($child); unset($childNode);
      
    } unset($children);
    
    return $domNode; 
  }
}

class SAML20ExtenstionAssertionAttributesDecoder extends AbstractXmlDecoder {
  public function xmlDecode( XmlDecodeContext $context, DOMNode $node ){
    switch( $node->namespaceURI ){
      case SAML20ExtenstionAssertionAttributesCodec::DEFAULT_URN:
	switch( $node->localName ){
	  case "AssertionAttributes":
	    return $this->processAssertionAttributes( $context, $node );
	  break;
	  case "Attribute":
	    return $this->assertionAttribute( $context, $node );
	}
      break;
    }
  }
  
  private function processAssertionAttributes( XmlDecodeContext $context, DOMNode $node  ){   
    $ret = new SAML20ExtenstionAssertionAttributes();
    
    for( $i = 0; $i < $node->childNodes->length; $i++ ){
      if( $obj = $this->xmlDecode( $context, $node->childNodes->item($i) ) ){
	$ret->addAttribute( $obj );
      }
    }
    
    return $ret;
  }
  
  private function assertionAttribute( XmlDecodeContext $context, DOMNode $domNode ){
    $ret = new SAML20ExtenstionAssertionAttribute();
    
    if( $attr = $domNode->attributes->getNamedItem("Name") ){
      $ret->setName( $attr->nodeValue );
    }
    
    if( $attr = $domNode->attributes->getNamedItem("NameFormat") ){
      $ret->setNameFormat( $attr->nodeValue );
    }
    
    if( $attr = $domNode->attributes->getNamedItem("FriendlyName") ){
      $ret->setFriendlyName( $attr->nodeValue );
    }
    
    {//parse values
      for( $i = 0; $i < $domNode->childNodes->length; $i++ ){
	$domAttributeValue = $domNode->childNodes->item($i);
	if( $domAttributeValue->localName == "AttributeValue" ){
	  for( $i1 = 0; $i1 < $domAttributeValue->childNodes->length; $i1++ ){
	    $obj = $context->xmlDecode( $domAttributeValue->childNodes->item($i1) );
	    if( !is_null($obj) ){
	      $ret->addValue($obj);
	    } unset($obj);
	  }
	}unset($domAttributeValue);
      }
    }
    
    return $ret;
  }
  
}

/**
 * Reference element[name="AssertionAttributes"] in Saml 2.0 XSD https://authentication.bluecoat.com/schema/saml20-extension-assertion-attributes.xsd
 */
class SAML20ExtenstionAssertionAttributes {

  private $attributes;
  
  public function __construct(){
    $this->attributes = array();
  }
  
  public function addAttribute( SAML20ExtenstionAssertionAttribute $attribute ){
    $this->attributes[] = $attribute;
  }
  
  public function getAttributes( ){
    return $this->attributes;
  }

}

/**
 * Reference element[name="Attribute"] in Saml 2.0 XSD https://authentication.bluecoat.com/schema/saml20-extension-assertion-attributes.xsd
 */
class SAML20ExtenstionAssertionAttribute extends Saml20CodecAttribute { }