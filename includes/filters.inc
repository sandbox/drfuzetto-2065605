<?php

class SamlServiceKeyDecriptorFilter {

  public static function createFilterCallback( $use ){
    return array( new SamlServiceKeyDecriptorFilter($use), "filter" );
  }

  private $use;
  
  public function __construct( $use ){
    $this->use = $use;
  }
  
  public function filter( $descriptor ){
    if( $descriptor instanceof SAML20CodecKeyDescriptor ){
      return $descriptor->getUse() == $this->use;
    }
    
  }

}

class SamlServiceRoleDecriptorFilter {

  public static function createFilterCallback( $className ){
    return array( new SamlServiceRoleDecriptorFilter($className), "filter" );
  }

  private $className;
  
  public function __construct( $className ){
    $this->className = $className;
  }
  
  public function filter( $descriptor ){
    if( is_array( $this->className ) ){
      foreach( $this->className  as $className ){
	if($descriptor instanceof $className ){
	  return true;
	}
      }
      return false;
    }
    return $descriptor instanceof $this->className;
  }

}

class SamlAttributeFilterCriterion {

  const FIELD_NAME = "name";
  const FIELD_NAMEFORMAT = "nameFormat";
  const FIELD_FRIENDLYNAME = "friendlyName";

  private $field, $value;

  public function __construct( $field, $value ){
    if( !in_array( $field, array( self::FIELD_NAME, self::FIELD_NAMEFORMAT, self::FIELD_FRIENDLYNAME ) ) ){
      throw new Exception("{$field} is not a valid field name.");
    }
    $this->field = $field;
    $this->value = $value;
  }
  
  public function getField(){
    return $this->field;
  }
  
  public function getValue(){
    return $this->value;
  }

}

class SamlAttributeFilter {

  public static function createFilterCallback( array $criteria ){
    return array( new SamlAttributeFilter($criteria), "filter" );
  }

  private $criteria;
  
  public function __construct( array  $criteria ){
    $this->criteria = $criteria;
  }
  
  public function filter( $obj ){
    if( $obj instanceof Saml20CodecAttribute ){
      $matches = 0;
      foreach( $this->criteria as $criterion  ){
	switch( $criterion->getField() ){
	  case SamlAttributeFilterCriterion::FIELD_NAME :
	    if( $obj->getName() == $criterion->getValue() ){
	      $matches++;
	    }
	  break;
	  case SamlAttributeFilterCriterion::FIELD_NAMEFORMAT :
	    if( $obj->getNameFormat() == $criterion->getValue() ){
	      $matches++;
	    }
	  break;
	  case SamlAttributeFilterCriterion::FIELD_FRIENDLYNAME :
	    if( $obj->getFriendlyName() == $criterion->getValue() ){
	      $matches++;
	    }
	  break;
	}
      }
      
      return $matches == count($this->criteria);
    }
    
  }

}