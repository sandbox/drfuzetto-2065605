<?php

/**
 * Task callback for managing keys for a descriptor
 */
function saml_service_role_task_keys( $context  ){
  
  if( count( $context['arguments'] ) ){
    switch( $context['arguments'][0] ){
      case "dl_public": {
	if( ($cid = $context['arguments'][1]) && ( $cache = cache_get($cid) ) ){
	  $uri = "temporary://". Saml::generateId();
	  if( $fp = fopen( drupal_realpath( $uri ), "w" ) ){
	    fwrite( $fp, "-----BEGIN CERTIFICATE-----\n" );
	    fwrite( $fp, chunk_split(str_replace(array("\r", "\n", "\t", ' '), '',$cache->data), 64) );
	    fwrite( $fp, "-----END CERTIFICATE-----\n" );
	    register_shutdown_function( 'file_delete', (object)array( "uri" => $uri) );
	    return file_transfer(
	      $uri,
	      array(
		"Content-type" => "application/octet-stream; filename=\"public.cer\"",
		"Content-Disposition" => "attachment; filename=\"public.cer\"",
	      )
	    );
	  }
	}
	return MENU_NOT_FOUND;
      } break;
      case "dl_private": {
	if( ($cid = $context['arguments'][1]) && ( $cache = cache_get($cid) ) ){
	  $uri = "temporary://". Saml::generateId();
	  if( $fp = fopen( drupal_realpath( $uri ), "w" ) ){
	    fwrite( $fp, $cache->data);
	    register_shutdown_function( 'file_delete', (object)array( "uri" => $uri) );
	    return file_transfer(
	      $uri,
	      array(
		"Content-type" => "application/octet-stream; filename=\"private.key\"",
		"Content-Disposition" => "attachment; filename=\"private.key\"",
	      )
	    );
	  }
	}
	return MENU_NOT_FOUND;
      } break;
    }
  }

  return drupal_build_form("saml_service_role_task_keys_form", $context);
}

/**
 * Task callback for managing keys for a descriptor
 */
function saml_service_role_task_keys_form( $form, &$form_state ){
  if( !isset($form_state['storage']['keyDescriptors']) ){
    $form_state['storage']['keyDescriptors'] = array();
    
    $keyDescriptors = $form_state['descriptor']->getKeyDescriptors();
    reset($keyDescriptors);
    while( list(,$keyDescriptor) = each($keyDescriptors) ){
      if( $keyDescriptor->getKeyPairId() && !array_key_exists($keyDescriptor->getKeyPairId(), $form_state['storage']['keyDescriptors']) ){
	$form_state['storage']['keyDescriptors'][$keyDescriptor->getKeyPairId()] = (object)array(
	  'id'      => $keyDescriptor->getKeyPairId(),
	  'use'     => array(),
	  'keyPair' => $keyDescriptor->getKeyPair()
	);
	
      }
      
      array_push($form_state['storage']['keyDescriptors'][$keyDescriptor->getKeyPairId()]->use, $keyDescriptor->getUse() );
    }
  }
  
  $task = "saml_service_role_task_keys_form_op_list";

  if( isset($form_state['storage']['operation']) ){
    $task = "saml_service_role_task_keys_form_op_{$form_state['storage']['operation']}";
  }

  return $task( $form, $form_state );
}

function saml_service_role_task_keys_form_op_list( $form, &$form_state ){

  $form['operation_container'] = array(
    '#type' => 'fieldset',
    '#title' => t('Operation'),
    '#attributes' => array( 'class' => array( 'container-inline' ) )
  );
  
  $form['operation_container']['operation'] = array(
    '#type' => 'select',
    '#options'  => array(
      'new'        => t('New'),
      'manage'     => t('Manage'),
      'delete'     => t('Delete'),
      'dl_public'  => t('Download Public Key'),
      'dl_private' => t('Download Private Key')
    ),
  );
  
  $form['operation_container']['continue'] = array(
    '#type' => 'submit',
    '#value' => t('Execute')
  );
  
  $form['key'] = array(
    '#type' => 'tableselect',
    '#header' => array(
      'use'       => t('Use(s)'),
      'private'   => t('Private'),
      'public'    => t('Public'),
    ),
    '#options'  => array(),
    '#multiple' => false,
    '#empty'    => t('No keys defined.'),
  );
  
  foreach($form_state['storage']['keyDescriptors']  as $index => $keyPairMeta ){
    $placeholders = array();
  
    foreach( $keyPairMeta->use as $placeholder => $value){
      $placeholders["!{$placeholder}"] = t(strtoupper($value));
    }
  
    $form['key']['#options'][$index] = array(
      'use'       => t(implode(", ", array_keys($placeholders)), $placeholders),
      'private'   => t( ( $keyPairMeta->keyPair && $keyPairMeta->keyPair->getPrivateKey() ) ? "Present":"None" ),
      'public'    => t( "None" ),
    );
    
    if(
      $keyPairMeta->keyPair &&
      ($key = $keyPairMeta->keyPair->getPublicKey()) &&
      !empty($key) &&
      $details = openssl_x509_parse( SAMLSignatureUtils::publicKeyToX509($key) )
    ){
      $detail_element =  array(
	'#type'   => 'saml_certificate_details',
	'#title'   => t('Details'),
	'#weight'  => -1,
	'#details' => $details,
      );
      
      $form['key']['#options'][$index]['public'] = drupal_render($detail_element);
      unset($detail_element);
    }
  }
  
  $form['#validate'][] = 'saml_service_role_task_keys_form_op_list_validate';
  $form['#submit'][] = 'saml_service_role_task_keys_form_op_list_submit';
  
  return $form;
}

function saml_service_role_task_keys_form_op_list_validate( $form, &$form_state ){
  if( $form_state['values']['operation'] != "new"  ){
    if( empty($form_state['values']['key']) ){
      form_set_error('key',t('You must select a key for the operation %op', array( "%op", $form['operation_container']['operation']['#options'][$form_state['values']['operation']] ) ) );
    }
    else if( $form_state['values']['operation'] == "dl_public" ){
      $keyMeta = $form_state['storage']['keyDescriptors'][$form_state['values']['key']];
      if(!$keyMeta->keyPair || !$keyMeta->keyPair->getPublicKey()){
	form_set_error( 'key', t('Unable to retrieve the public key from the selection.' ) );
      }
    }
    else if( $form_state['values']['operation'] == "dl_private" ){
      $keyMeta = $form_state['storage']['keyDescriptors'][$form_state['values']['key']];
      if(!$keyMeta->keyPair || !$keyMeta->keyPair->getPrivateKey()){
	form_set_error( 'key', t('Unable to retrieve the private key from the selection.' ) );
      }
    }
  }
}

function saml_service_role_task_keys_form_op_list_submit( $form, &$form_state ){
  
  
  if( $form_state['values']['op'] == t('Execute') && !empty($form_state['values']['operation'] ) ){
    switch($form_state['values']['operation']){
      case "dl_public":
	$keyMeta = $form_state['storage']['keyDescriptors'][$form_state['values']['key']];
	if($keyMeta->keyPair && $keyMeta->keyPair->getPublicKey()){
	  $cid = false;
	  do {
	    $cid =  uniqid('', true);
	  } while( cache_get($cid) );
	  
	  cache_set( $cid, $keyMeta->keyPair->getPublicKey(), 'cache', strtotime( "+5 minutes" ) );
	  
	  $form_state['redirect'] = "{$form_state['task_path']}/dl_public/{$cid}";
	}
      break;
      case "dl_private":
	$keyMeta = $form_state['storage']['keyDescriptors'][$form_state['values']['key']];
	if($keyMeta->keyPair && $keyMeta->keyPair->getPrivateKey()){
	  $cid = false;
	  do {
	    $cid =  uniqid('', true);
	  } while( cache_get($cid) );
	  
	  cache_set( $cid, $keyMeta->keyPair->getPrivateKey(), 'cache', strtotime( "+5 minutes" ) );
	  
	  $form_state['redirect'] = "{$form_state['task_path']}/dl_private/{$cid}";
	}
      break;
      default:
	$form_state['rebuild'] = true;
	$form_state['storage']['operation'] = $form_state['values']['operation'];
	$form_state['storage']['keyIndex'] = $form_state['values']['key'];
    }
  }
}

function saml_service_role_task_keys_form_op_manage( $form, &$form_state ){

  $keyMeta = $form_state['storage']['keyDescriptors'][$form_state['storage']['keyIndex']];
  
  $form['#attributes'] = array('enctype' => "multipart/form-data");
  
  $form['uses'] = array(
    '#type' => 'select',
    '#title' => t('Uses'),
    '#options' => array(
      SAML20CodecKeyDescriptor::USE_SIGNING => t(ucwords(SAML20CodecKeyDescriptor::USE_SIGNING)),
      SAML20CodecKeyDescriptor::USE_ENCRYPTION => t(ucwords(SAML20CodecKeyDescriptor::USE_ENCRYPTION))
    ),
    '#default_value' => $keyMeta->use,
    '#multiple' => true,
    '#required' => true
  );

  $form["keys"] = array(
    '#type' => 'vertical_tabs', 
  );
  
  $form['private'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Private'),
    '#collapsible' => true,
    '#group'       => "keys",
    '#tree'        => true
  );

  
  $form['private']['generate'] = array(
    '#type'   => 'submit',
    '#value'  => t('Generate Key'),
    '#submit' => array( 'saml_service_role_task_keys_form_op_manage_private_generate' ),
    '#weight' => 1
  );
  
  $form['private']['supply'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Supply a key'),
    '#collapsible' => true,
    '#collapsed'   => true,
    '#weight'      => 2
  );
  
  $form['private']['supply']['passphrase'] = array(
    '#type'   => 'textfield',
    '#title'  => t('Passphrase'),
  );
  
  $form['private']['supply']['private_key'] = array(
    '#title'  => t('Key File'),
    '#type'   => 'file',
  );
  
  $form['private']['supply']['private_upload'] = array(
    '#type'   => 'submit',
    '#name'   => 'private_upload',
    '#value'  => t('Upload'),
    '#submit' => array( 'saml_service_role_task_keys_form_op_manage_private_upload' )
  );

  if(
    $keyMeta->keyPair &&
    ($key = $keyMeta->keyPair->getPrivateKey()) &&
    !empty($key)
  ){
    $form['private'][] = array(
      '#type'        => 'fieldset',
      '#title'       => t('Passphrase'),
      '#collapsible' => true,
      '#collapsed'   => true,
      array(
	'#markup'      => "<blockquote><pre>".$keyMeta->keyPair->getPrivateKeyPassphrase()."</pre></blockquote>",
      ),
    );
    
    $form['private'][] = array(
      '#type'        => 'fieldset',
      '#title'       => t('Key'),
      '#collapsible' => true,
      '#collapsed' => true,
      array(
	'#markup'      => "<blockquote><pre>{$key}</pre></blockquote>",
      ),
    );
    
    //A private key is required to generate a public key
    
    $form['public'] = array(
      '#type'        => 'fieldset',
      '#title'       => t('Public'),
      '#collapsible' => true,
      '#group'       => "keys",
      '#tree'        => true,
    );
    
    if(
      ($key = $keyMeta->keyPair->getPublicKey()) &&
      !empty($key)
    ){
	
	$form['public']['key'] = array(
	  '#type'        => 'fieldset',
	  '#title'       => t('Key'),
	  '#collapsible' => true,
	  '#collapsed' => true,	array(
	    '#markup'      => "<blockquote><pre>{$key}</pre></blockquote>",
	  )
	);
	
	if( $details = openssl_x509_parse( SAMLSignatureUtils::publicKeyToX509($key) ) ){   
	  $form['public']['details'] = array(
	    '#type'   => 'saml_certificate_details',
	    '#title'   => t('Details'),
	    '#weight'  => -1,
	    '#details' => $details,
	  );
	}
      }
	
      require_once( DRUPAL_ROOT . "/includes/iso.inc" );
    
      $expires = strtotime( "+2 Years" );
      
      $form['public']['options'] = array(
	'#type'              => 'fieldset',
	'#title'             => t('Generate'),
	'#collapsible'       => true,
	'#collapsed'         => true,
	
	"countryName"        => array(
	  '#title'         => t('Country Name (2 letter code)'),
	  '#type'          => 'select',
	  '#options'       => _country_get_predefined_list(),
	  '#default_value' => 'US',
	),
	"stateOrProvinceName" => array(
	  '#title'       => t('State or Province Name (full name)'),
	  '#type'        => 'textfield',
	  '#description' => t('Defaults to %default', array( '%default' => 'Unspeficied' ))
	),
	"localityName"        => array(
	  '#title'       => t('Locality Name (eg, city)'),
	  '#type'        => 'textfield',
	  '#description' => t('Defaults to %default', array( '%default' => 'Unspeficied' ))
	),
	"organizationName"   => array(
	  '#title'       => t('Organization Name (eg, company)'),
	  '#type'        => 'textfield',
	  '#description' => t('Defaults to %default', array( '%default' => 'Unspeficied' ))
	),
	"organizationalUnitName" => array(
	  '#title'       => t('Organizational Unit Name (eg, section)'),
	  '#type'        => 'textfield',
	  '#description' => t('Defaults to %default', array( '%default' => 'Unspeficied' ))
	),
	"commonName"             =>  array(
	  '#title'       => t("Common Name (eg, your name or your server's hostname)"),
	  '#type'        => 'textfield',
	  '#description' => t('Defaults to %default', array( '%default' => str_replace(array("https://","http://"),"", $GLOBALS['base_url']) ))
	),
	"emailAddress"           => array(
	  '#title'       => t('Email Address'),
	  '#type'        => 'textfield',
	  '#description' => t('Defaults to %default', array( '%default' => variable_get('site_mail',"") ))
	),
	"expires"                => array(
	  '#title'         => t('Valid To'),
	  '#type'          => 'date',
	  '#default_value' => array( "year" => date("Y", $expires), "day" => date("d", $expires), "month" => date("n", $expires))
	),
	
	'submit'                 => array(
	  '#type'   => 'submit',
	  '#value'  => t('Process'),
	  '#submit' => array( 'saml_service_role_task_keys_form_op_manage_public_generate' )
	),
      );
      
      $form['public']['supply'] = array(
	'#type'        => 'fieldset',
	'#title'       => t('Supply a key'),
	'#collapsible' => true,
	'#collapsed'   => true,
	'#weight'      => 2
      );
      
      $form['public']['supply']['public_key'] = array(
	'#title'  => t('Key File'),
	'#type'   => 'file',
      );
      
      $form['public']['supply']['public_upload'] = array(
	'#type'   => 'submit',
	'#name'   => 'public_upload',
	'#value'  => t('Upload'),
	'#submit' => array( 'saml_service_role_task_keys_form_op_manage_public_upload' )
      );
    
  }
  
  $form['save'] = array(
    '#type'  => 'submit',
    '#value' => t('Save'),
    '#submit' => array( 'saml_service_role_task_keys_form_op_manage_save' )
  );
  
  $form['cancel'] = array(
    '#type'   => 'link',
    '#title'  => t('Cancel'),
    '#href'   => $form_state['task_path'],
    '#attributes' => array( 'class' => array( "button" ) ),
  );
  
  return $form;
}

function saml_service_role_task_keys_form_op_manage_private_generate( $form, &$form_state ){
  $form_state['rebuild'] = true;
  
  drupal_set_message( t('Private key generated.') );
  
  $form_state['storage']['keyDescriptors'][$form_state['storage']['keyIndex']]->keyPair = SAMLSignatureUtils::generateKeyPair(); 
}

function saml_service_role_task_keys_form_op_manage_private_upload( $form, &$form_state ){
  $form_state['rebuild'] = true;
  
  $form_state['storage']['keyDescriptors'][$form_state['storage']['keyIndex']]->keyPair  = new StringXMLKeyPair( array(
    'privateKeyPassphrase' => $form_state['values']['private']['supply']['passphrase'],
    'privateKey'           =>  file_get_contents($_FILES['files']['tmp_name']['private']) ,
    'publicKey'            => "",
  ));
  
  drupal_set_message( t('Private Key updated.') );
  
}

function saml_service_role_task_keys_form_op_manage_public_generate( $form, &$form_state ){
  $form_state['rebuild'] = true;
  
  $keyPair = $form_state['storage']['keyDescriptors'][$form_state['storage']['keyIndex']]->keyPair;
  
  $form_options = $form_state['values']['public']['options'];
  
  $options = array(
    'privateKey' => $keyPair->getPrivateKey(),
    'passphrase' => $keyPair->getPrivateKeyPassphrase(),
    'publicKey'  => true
  );
  
  if( ($expires = $form_options['expires']) && is_array($expires) ){
    $options['expires'] = ceil( (mktime(0,0,0,$expires['month'],$expires['day'],$expires['year'])-time())/( 60 * 60 * 24 ) );
  }
  
  $dn_keys = array( 'countryName', 'stateOrProvinceName', 'localityName', 'organizationName', 'organizationalUnitName', 'commonName', 'emailAddress' );
  
  foreach($dn_keys as $key){
    if(
      isset( $form_options[$key] ) && 
      !empty( $form_options[$key] )
    ){
      $options['dn'][$key] = $form_options[$key];
    }
  }
  
  $form_state['storage']['keyDescriptors'][$form_state['storage']['keyIndex']]->keyPair = SAMLSignatureUtils::generateKeyPair($options); 
  
  drupal_set_message( t('Public Key Generated'));
}

function saml_service_role_task_keys_form_op_manage_public_upload( $form, &$form_state ){
  $form_state['rebuild'] = true;
  
  $keyPair = $form_state['storage']['keyDescriptors'][$form_state['storage']['keyIndex']]->keyPair;
  
  $form_state['storage']['keyDescriptors'][$form_state['storage']['keyIndex']]->keyPair  = new StringXMLKeyPair( array(
    'privateKeyPassphrase' => $keyPair->getPrivateKeyPassphrase(),
    'privateKey'           => $keyPair->getPrivateKey(),
    'publicKey'            => SAMLSignatureUtils::x509toPublicKey( file_get_contents($_FILES['files']['tmp_name']['public']) )
  ));
  
  drupal_set_message( t('Public Key updated with value supplied'));
}

function saml_service_role_task_keys_form_op_manage_save( $form, &$form_state ){
  $keyMeta = $form_state['storage']['keyDescriptors'][$form_state['storage']['keyIndex']];
  $keyMeta->use = $form_state['values']['uses'];
  
  $entity = $form_state['entity'];
  
  if( $entityDescriptor = $entity->getDescriptor() ){
    $descriptors = $form_state['entity']->getDescriptor()->getDescriptors();
    $entity->getDescriptor()->clearDescriptors();
    reset($descriptors);
    while( list(,$descriptor) = each($descriptors) ){
      if(get_class($descriptor) == get_class($form_state['descriptor'])){
	$descriptor->clearKeyDescriptors();
	reset($form_state['storage']['keyDescriptors']);
	while( list(,$keyMeta) = each($form_state['storage']['keyDescriptors']) ){
	  foreach( $keyMeta->use as $use ){
	    if($keyMeta->keyPair && $keyMeta->id <= 0){
	      $record = (object)array(
		'privatekeypassphrase' => $keyMeta->keyPair->getPrivateKeyPassphrase(),
		'privatekey'           => $keyMeta->keyPair->getPrivateKey(),
		'publickey'            => $keyMeta->keyPair->getPublicKey(),
	      );
	      
	      drupal_write_record( "saml_keypair", $record );
	      
	      $keyMeta->id = $record->id; unset($record);
	    }
	    $keyDescriptor = new SAMLServiceKeyDescriptor($keyMeta->id);
	    $keyDescriptor->setUse($use);
	    $keyDescriptor->setKeyPair($keyMeta->keyPair);
	    $descriptor->addKeyDescriptor($keyDescriptor);
	  }
	}
      }
      $entity->getDescriptor()->addDescriptor($descriptor); unset($descriptor);
    }
    SAML::getInstance()->saveEntity($entity); unset($entity);
  }


  drupal_set_message( t( 'Key Saved') );
}

function saml_service_role_task_keys_form_op_delete( $form, &$form_state ){
  $keyMeta = $form_state['storage']['keyDescriptors'][$form_state['storage']['keyIndex']];
  
  $form['statement'] = array(
    '#prefix' => '<h3>',
    '#suffix' => '</h3>',
    '#markup' => t('Are you sure you want to delete this key?')
  );
  
  $form['notice'] = array(
    '#prefix' => '<h4>',
    '#suffix' => '</h4>',
    '#markup' => t('If other entities are using this key the data will no longer exist.')
  );
  
  $form["keys"] = array(
    '#type' => 'vertical_tabs', 
  );
  
  $form['private'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Private'),
    '#collapsible' => true,
    '#group'       => "keys",
  );
  
  $form['public'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Public'),
    '#collapsible' => true,
    '#group'       => "keys",
  );
    
  if(
    $keyMeta->keyPair
  ){
  
    if(
      ($key = $keyMeta->keyPair->getPrivateKey()) &&
      !empty($key)
    ){
      $form['private'][] = array(
	'#type'        => 'fieldset',
	'#title'       => t('Key'),
	'#collapsible' => true,

	array(
	  '#markup'      => "<blockquote><pre>{$key}</pre></blockquote>",
	),
      );
    }
    
    if(
      ($key = $keyMeta->keyPair->getPublicKey()) &&
      !empty($key)
    ){
	
	$form['public']['key'] = array(
	  '#type'        => 'fieldset',
	  '#title'       => t('Key'),
	  '#collapsible' => true,
	  '#collapsed' => true,	array(
	    '#markup'      => "<blockquote><pre>{$key}</pre></blockquote>",
	  )
	);
	
	if( $details = openssl_x509_parse( SAMLSignatureUtils::publicKeyToX509($key) ) ){   
	  $form['public']['details'] = array(
	    '#type'   => 'saml_certificate_details',
	    '#title'   => t('Details'),
	    '#weight'  => -1,
	    '#details' => $details,
	  );
	}
    }
  }
  

  $form['save'] = array(
    '#type'   => 'submit',
    '#value'  => t( 'Yes' ),
    '#submit' => array( 'saml_service_role_task_keys_form_op_delete_submit' )
  );
  
  $form['no'] = array(
    '#markup' => l( t( 'No' ), $form_state['task_path'], array( "attributes" => array( 'class' => array( 'button' ) ) ) )
  );
  
  return $form;
}

function saml_service_role_task_keys_form_op_delete_submit( $form, &$form_state ){
  $keyMeta = $form_state['storage']['keyDescriptors'][$form_state['storage']['keyIndex']];
  
  if( $keyMeta->id ){
    db_delete( "saml_keypair" )->condition("id",$keyMeta->id)->execute();
  }
  
  drupal_set_message( t('Key was deleted.') );
  
  unset( $form_state['storage']['keyDescriptors'][$form_state['storage']['keyIndex']] );
  
  $form_state['rebuild'] = true;
  unset($form_state['storage']['operation']); unset($form_state['storage']['keyIndex']);
}

function saml_service_role_task_keys_form_op_new( $form, &$form_state ){
  
  if( !array_key_exists('new_index', $form_state['storage']) ){
    $form_state['storage']['new_index'] = -count($form_state['storage']['keyDescriptors']);
  }
  
  $form_state['storage']['keyIndex'] = $form_state['new_index'];
  
  return saml_service_role_task_keys_form_op_manage( $form, $form_state );
}