<?php

/**
 * This interface handles to enable different log datastores
 */
class SAMLServiceLogEvent {

  private $flags, $entity, $account, $context;

  public function __construct( $flags, $entity, $account, $context = array() ){
    $this->flags = $flags;
    $this->entity = $entity;
    $this->account = $account;
    $this->context = $context;
  }
  
  /**
   * The logging flags that relate to this event.
   **/
  public function getFlags(){
    return $this->flags;
  }
  
  public function hasFlag( $flag ){
    return ( $this->flags & $flag ) == $flag;
  }
  
  /**
   * The type of entity which is the source of the event.
   **/
  public function getEntity(){
    return $this->entity;
  }
  
  /**
   * The user account which is the source of the event.
   **/
  public function getAccount(){
    return $this->account;
  }
  
  /**
   * Additional context to enable additional data about the event to be
   * distributed to logging system.
   **/
  public function getContext(){
    return $this->context;
  }
  
}

/**
 * This interface handles to enable different log datastores
 */
interface SAMLServiceLogger {

  public function logEvent( SAMLServiceLogEvent $event );
  
}

/**
 * This class stores all logs in the database using the drupal_write_record method
 */
class DatabaseSAMLServiceLogger implements SAMLServiceLogger { 

  public function logEvent( SAMLServiceLogEvent $event ){
    if( !SAMLServiceLogging::hasFlag(SAML_SERVICE_LOG_MESSAGE_CONTEXT) || !$event->hasFlag(SAML_SERVICE_LOG_MESSAGE_CONTEXT) ){
      return ;
    }
    
    $record = array(
      'created'  => time(),
    );
    
    if( $eventContext = $event->getContext() ){
    
      if( array_key_exists( 'messageContext', $eventContext ) && ( $context = $eventContext['messageContext'] ) ) {
      
	if($context->request){
	  $request = $context->request;
	  if( $request instanceof SOAPMessage ){
	    $request = $request->getBody();
	  }
	  
	  if( ($request instanceof SAML20CodecAuthnRequest) &&  ($issuer = $request->getIssuer() ) ){
	    $record['request_entityid']  = $issuer->getValue();
	  } unset($issuer);
	  
	  $record['request']  = $context->requestXML;
	  
	  unset( $request );
	}
	
	if($context->response){
	  $response = $context->response;
	  if( $response instanceof SOAPMessage ){
	    $response = $response->getBody();
	  }
	
	  if( ( $response instanceof SAML20CodecResponse ) && ($issuer = $response->getIssuer() ) ){
	    $record['response_entityid']  = $issuer->getValue();
	  } unset($issuer);
	  
	  $record['response']  = $context->responseXML;
	  
	  unset( $response );
	}
	
	if( isset($record['request']) || isset($record['response']) ){
	  drupal_write_record( 'saml_service_log', $record );
	}
	unset($record);
      }
    }
  }

}

class SAMLServiceLogging implements SAMLServiceLogger {

  private static $inst = null;
  
  public static function canidateCompare($a, $b ){
    if ($a['weight'] == $b['weight']) {
	return 0;
    }
    return ($a['weight'] < $b['weight']) ? -1 : 1;
  }
  
  public static function hasFlag( $flag ){
    return ( variable_get("saml_service:log",0) & $flag ) == $flag;
  }

  public static function getLogger(){
    if( is_null(self::$inst) ){
      self::$inst = new SAMLServiceLogging(  );
    }
    return self::$inst;
  }
  
  private $canidates;
  
  public function __construct(){
    $this->canidates = null;
  }
  
  private function &getCanidates(){
    if( is_null($this->canidates) ){
      $this->canidates = array( );
	    
      $cid = "saml:log:canidates";
      if( $cache = cache_get($cid) ){
	$this->canidates = $cache->data;
      } else {
	
	$modules = module_implements( "saml_log_canidates" );
	foreach( $modules as $module ){
	  $callback = "{$module}_saml_log_canidates";
	  if(
	    function_exists($callback) &&
	    ( $module_canidates = $callback() )
	  ){
	    foreach( $module_canidates as $canidate ){
	      if( SAMLServiceLogging::hasFlag($canidate['flag']) ){
		$canidate += array( 'weight' => $canidate['flag'], 'instance' => null );
		if( !array_key_exists( $canidate['flag'], $this->canidates ) ){
		  $this->canidates[$canidate['flag']] = $canidate;
		}
		else if( $this->canidates[$canidate['flag']]['weight'] < $canidate['weight'] ){
		  $this->canidates[$canidate['flag']] = $canidate;
		}
	      }
	    }
	  }
	}
	
	usort( $this->canidates, array( 'SAMLServiceLogging' , "canidateCompare") );
	
	cache_set($cid, $this->canidates, 'cache', CACHE_PERMANENT);
      }
    }
    return $this->canidates;
  }
  
  public function logEvent( SAMLServiceLogEvent $event ){
    $canidates =& $this->getCanidates();
    
    foreach( $canidates as $canidate ){
      if( $event->hasFlag($canidate['flag']) ){
	if( is_null( $canidate['instance'] ) ){
	  $canidate['instance'] = new $canidate['class']();
	}
	$canidate['instance']->logEvent( $event );
      }
    }
  }
  
}