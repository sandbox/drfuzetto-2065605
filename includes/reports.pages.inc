<?php

function saml_service_reports_overview(){

  $build = array();
  
  $build['dblog_filter_form'] = drupal_get_form('saml_service_reports_filter_form');
  
  $filter = saml_service_reports_filter_query();
  $rows = array();
  
  $header = array(
    array(
      'data' => t('Created'),
      'field' => 'log.created',
      'sort' => 'desc',
    ),
    array(
      'data' => t('Requestor EntityId'),
      'field' => 'log.request_entityid',
    ),
    array(
      'data' => t('Responder EntityId'),
      'field' => 'log.response_entityid',
    ),    
    array('data' => t('Operations')),
  );

  $query = db_select('saml_service_log', 'log')->extend('PagerDefault')->extend('TableSort');
  $query
    ->fields('log', array( 'id', 'request_entityid', 'response_entityid', 'created'));
    
  if (!empty($filter['where'])) {
    $query->where($filter['where'], $filter['args']);
  }
  $result = $query
    ->limit(50)
    ->orderByHeader($header)
    ->execute();

  foreach ($result as $entry) {
    $row = array(
      'data' => array(
	format_date($entry->created, 'short'),
        '',
        '',
        l( t('view'), "admin/reports/saml/{$entry->id}" )
      ),
    );
    
    if( $entry->request_entityid ){
      $row['data'][1] = $entry->request_entityid . " " . l( t('xml'), "admin/reports/saml/{$entry->id}/request" );
    }
    
    if( $entry->response_entityid ){
      $row['data'][2] = $entry->response_entityid . " " . l( t('xml'), "admin/reports/saml/{$entry->id}/response" );
    }
    
    $rows[] = $row;
  }

  $build['dblog_table'] = array(
    '#theme' => 'table', 
    '#header' => $header, 
    '#rows' => $rows, 
    '#attributes' => array('id' => 'admin-dblog'), 
    '#empty' => t('No log messages available.'),
  );
  
  $build['dblog_pager'] = array('#theme' => 'pager');

  return $build;

}

function saml_service_reports_filter_query() {
  if (empty($_SESSION['saml_service_reports_overview'])) {
    return;
  }

  $filters = saml_service_reports_filters();

  // Build query
  $where = $args = array();
  foreach ($_SESSION['saml_service_reports_overview'] as $key => $filter) {
    $filter_where = array();
    foreach ($filter as $value) {
      $filter_where[] = $filters[$key]['where'];
      $args[] = $value;
    }
    if (!empty($filter_where)) {
      $where[] = '(' . implode(' OR ', $filter_where) . ')';
    }
  }
  $where = !empty($where) ? implode(' AND ', $where) : '';

  return array(
    'where' => $where, 
    'args' => $args,
  );
}

function saml_service_reports_filters() {
  $filters = array();

  $requestors = array();
  
  $resultset = db_query("SELECT DISTINCT(request_entityid) FROM {saml_service_log} WHERE request_entityid IS NOT NULL AND request_entityid <> '' ORDER BY request_entityid");
  
  foreach ($resultset as $result) {
    $requestors[$result->request_entityid] = $result->request_entityid;
  }
  
  unset($resultset);

  if ( $requestors ) {
    $filters['request_entityid'] = array(
      'title' => t('Requestor EntityId'), 
      'where' => "log.request_entityid = ?", 
      'options' => $requestors,
    );
  }
  unset($requestors);
  
  $responders = array();
  
  $resultset = db_query("SELECT DISTINCT(response_entityid) FROM {saml_service_log} WHERE response_entityid IS NOT NULL AND response_entityid <> '' ORDER BY response_entityid");
  
  foreach ($resultset as $result) {
    $responders[$result->response_entityid] = $result->response_entityid;
  }
  
  unset($resultset);

  if ( $responders ) {
    $filters['response_entityid'] = array(
      'title' => t('Responder EntityId'), 
      'where' => "log.response_entityid = ?", 
      'options' => $responders,
    );
  }
  unset($responders);

  return $filters;
}

function saml_service_reports_filter_form($form) {

  $filters = saml_service_reports_filters();

  $form['filters'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Filter log messages'), 
    '#collapsible' => TRUE, 
    '#collapsed' => empty($_SESSION['saml_service_reports_overview']),
  );

  
  foreach ($filters as $key => $filter) {
    $form['filters']['status'][$key] = array(
      '#title' => $filter['title'], 
      '#type' => 'select', 
      '#multiple' => TRUE, 
      '#size' => 8, 
      '#options' => $filter['options'],
    );
    if (!empty($_SESSION['saml_service_reports_overview'][$key])) {
      $form['filters']['status'][$key]['#default_value'] = $_SESSION['saml_service_reports_overview'][$key];
    }
  }

  $form['filters']['actions'] = array(
    '#type' => 'actions', 
    '#attributes' => array('class' => array('container-inline')),
  );
  
  if( $filters ){
    $form['filters']['actions']['submit'] = array(
      '#type' => 'submit', 
      '#value' => t('Filter'),
    );
  }
  else {
    $form['filters']['empty'] = array(
      '#type'   => "item",
      '#markup' => t("No filter criteria available"),
      '#prefix' => "<h3>",
      '#suffix' => '</h3>'
    );
  }
  
  
  if (!empty($_SESSION['saml_service_reports_overview'])) {
    $form['filters']['actions']['reset'] = array(
      '#type' => 'submit', 
      '#value' => t('Reset'),
    );
  }
  return $form;
}

function saml_service_reports_filter_form_submit($form, &$form_state) {
  $op = $form_state['values']['op'];
  $filters = saml_service_reports_filters();
  switch ($op) {
    case t('Filter'):
      foreach ($filters as $name => $filter) {
        if (isset($form_state['values'][$name])) {
          $_SESSION['saml_service_reports_overview'][$name] = $form_state['values'][$name];
        }
      }
      break;
    case t('Reset'):
      $_SESSION['saml_service_reports_overview'] = array();
      break;
  }
  return 'admin/reports/saml';
}


function saml_service_report_entry($id, $section = null) {

  if ($entry = $result = db_query('SELECT log.* FROM {saml_service_log} log WHERE log.id = :id', array(':id' => $id))->fetchObject()) {
    if( !is_null($section) && in_array( $section, array( "request", "response") ) ){
      if(!empty($entry->{$section})){
	while (ob_get_level()) {
	  ob_end_clean();
	}
	drupal_add_http_header("Content-type", "text/xml");
	drupal_send_headers();
	echo $entry->{$section};
	drupal_exit();
      }
      
      drupal_not_found();
      drupal_exit();
    }
    
    $rows = array(
      array(
        array(
          'data' => t('Date'),
          'header' => TRUE,
        ),
        format_date($entry->created, 'long'),
      ),
      array(
        array(
          'data' => t('Requestor EntityId'),
          'header' => TRUE,
        ),
        $entry->request_entityid,
      ),
      array(
        array(
          'data' => t('Request XML'),
          'header' => TRUE,
        ),
        "<pre>".htmlentities( $entry->request )."</pre>",
      ),
      array(
        array(
          'data' => t('Responder EntityId'),
          'header' => TRUE,
        ),
        $entry->response_entityid,
      ),
      array(
        array(
          'data' => t('Response XML'),
          'header' => TRUE,
        ),
        "<pre>".htmlentities( $entry->response )."</pre>",
      ),
    );
    $build['dblog_table'] = array(
      '#theme' => 'table', 
      '#rows' => $rows, 
      '#attributes' => array('class' => array('dblog-event')),
    );
    return $build;
  }
  else {
    return '';
  }
}