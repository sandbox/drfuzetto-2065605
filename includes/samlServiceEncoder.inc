<?php

class SamlService20Encoder extends Saml20Encoder {

  public function xmlEncode( XmlEncodeContext $context, $source ){

    if( $obj = parent::xmlEncode( $context, $source ) ){
      return $obj;
    }
    
    //start metadata conditions
    if( $source instanceOf SamlIdpMetaData ){
      return $this->metadataIDPSSODescriptor( $context, $source);
    }
    
    if( $source instanceOf SamlSpMetaData ){
      return $this->metadataSPSSODescriptor( $context, $source);
    }
        
    if( ($source instanceOf SamlIndexedEndpointType ) && !is_null( $context->getNode() ) ){
      $this->metadataEndpointType( $context, $source );
      $this->metadataIndexedEndpointType( $context, $source );
      return $context->getNode();
    }
    
    if( ($source instanceOf SamlEndpointType ) && !is_null( $context->getNode() ) ){
      $this->metadataEndpointType( $context, $source );
      return $context->getNode();
    }
    
  }
  
  //start metadata
  private function metadataEndpointType( XmlEncodeContext $context, SamlEndpointType $source ){
    $domNode =& $context->getNode();
    $domNode->setAttribute( "Binding", $source->getBinding() );
    $domNode->setAttribute( "Location", $source->getLocation() );
    if( $attr = $source->getResponseLocation() ){
      $domNode->setAttribute( "ReponseLocation", $attr );
    } unset($attr);
  }
  
  private function metadataIndexedEndpointType( XmlEncodeContext $context, SamlIndexedEndpointType $source ){
    $domNode =& $context->getNode();
    $attr = $source->getIndex()."";
    if( strlen($attr) > 0 ){
      $domNode->setAttribute( "index", $attr );
    } unset($attr);
    if( $attr = $source->isDefault() ){
      $domNode->setAttribute( "isDefault", $attr );
    } unset($attr);
  }
  
  private function metadataSPSSODescriptor( XmlEncodeContext $context, SamlSpMetaData $metaData){
    $EntityDescriptor = $context->createDocumentElementNS( SamlConst::NS_2_0_METADATA, "EntityDescriptor", "md" );
    $EntityDescriptor->setAttributeNS('http://www.w3.org/2000/xmlns/', "xmlns:md", SamlConst::NS_2_0_METADATA);
    $EntityDescriptor->setAttribute("entityID", $metaData->getEntityId());
    
    $SPSSODescriptorType = $context->createChildNS( SamlConst::NS_2_0_METADATA, "SPSSODescriptor" );
    if( $metaData->mustSignAssertions() ){
      $SPSSODescriptorType->setAttribute("WantAssertionsSigned", "true");
    }
    
    if( $cert = $metaData->getCertificate() ){
      {//signing use certificate
	$KeyInfo = $context->createElementNS( SamlConst::NS_XML_SIG, "KeyInfo", "ds" );
	$KeyInfo->setAttributeNS('http://www.w3.org/2000/xmlns/', "xmlns:ds", SamlConst::NS_XML_SIG);
	
	$X509Data = $context->createElementNS( SamlConst::NS_XML_SIG, "X509Data", "ds" );
	$KeyInfo->appendChild( $X509Data );
	
	$X509Data->appendChild( $context->createElementNS( SamlConst::NS_XML_SIG, "X509Certificate", "ds", $cert->getPublic() ) );
	
	$KeyDescriptor = $context->createElementNS( SamlConst::NS_2_0_METADATA, "KeyDescriptor", "md" );
	$KeyDescriptor->setAttribute("use", SamlConst::KEY_USE_SIGN );
	$SPSSODescriptorType->appendChild( $KeyDescriptor );
	
	$KeyDescriptor->appendChild( $KeyInfo );
      }
      {//signing use certificate
	$KeyInfo = $context->createElementNS( SamlConst::NS_XML_SIG, "KeyInfo", "ds" );
	$KeyInfo->setAttributeNS('http://www.w3.org/2000/xmlns/', "xmlns:ds", SamlConst::NS_XML_SIG);
	
	$X509Data = $context->createElementNS( SamlConst::NS_XML_SIG, "X509Data", "ds" );
	$KeyInfo->appendChild( $X509Data );
	
	$X509Data->appendChild( $context->createElementNS( SamlConst::NS_XML_SIG, "X509Certificate", "ds", $cert->getPublic() ) );
	
	$KeyDescriptor = $context->createElementNS( SamlConst::NS_2_0_METADATA, "KeyDescriptor", "md" );
	$KeyDescriptor->setAttribute("use", SamlConst::KEY_USE_ENC );
	$SPSSODescriptorType->appendChild( $KeyDescriptor );
	
	$KeyDescriptor->appendChild( $KeyInfo );
      }
    }
    
    //Start Processing of child elements
    $encoderData = new XmlEncodeContextData();
    $encoderData->setRoot( $SPSSODescriptorType );
    
    $endPoints = $metaData->getEndpoints();
    reset($endPoints);
    while( list($type, $bindings) = each($endPoints) ){
      reset($bindings);
      while( list(, $binding) = each($bindings) ){
	$encoderData->setSource( $context->createElementNS( SamlConst::NS_2_0_METADATA, $type ) );
	if( $domChild = $context->xmlEncode( $binding, $encoderData ) ){
	  $SPSSODescriptorType->appendChild($domChild);
	}
      }
    } unset($endPoints);
    return $EntityDescriptor;
  }
  
  private function metadataIDPSSODescriptor( XmlEncodeContext $context, SamlIdpMetaData $metaData){
    $EntityDescriptor = $context->createElementNS( SamlConst::NS_2_0_METADATA, "EntityDescriptor", "md" );
    $context->getDocument()->appendChild($EntityDescriptor);
    
    $EntityDescriptor->setAttributeNS('http://www.w3.org/2000/xmlns/', "xmlns:md", SamlConst::NS_2_0_METADATA);
    $EntityDescriptor->setAttribute("entityID", $metaData->getEntityId());
    
    $IDPSSODescriptor = $context->createElementNS( SamlConst::NS_2_0_METADATA, "IDPSSODescriptor" );
    $EntityDescriptor->appendChild($IDPSSODescriptor);
    $IDPSSODescriptor->setAttribute("protocolSupportEnumeration", "urn:oasis:names:tc:SAML:2.0:protocol");
    
    if( $cert = $metaData->getCertificate() ){
      {//signing use certificate
	$KeyInfo = $context->createElementNS( SamlConst::NS_XML_SIG, "KeyInfo", "ds" );
	$KeyInfo->setAttributeNS('http://www.w3.org/2000/xmlns/', "xmlns:ds", SamlConst::NS_XML_SIG);
	
	$X509Data = $context->createElementNS( SamlConst::NS_XML_SIG, "X509Data", "ds" );
	$KeyInfo->appendChild( $X509Data );
	
	$X509Data->appendChild( $context->createElementNS( SamlConst::NS_XML_SIG, "X509Certificate", "ds", $cert->getPublic() ) );
	
	$KeyDescriptor = $context->createElementNS( SamlConst::NS_2_0_METADATA, "KeyDescriptor", "md" );
	$KeyDescriptor->setAttribute("use", SamlConst::KEY_USE_SIGN );
	$IDPSSODescriptor->appendChild( $KeyDescriptor );
	
	$KeyDescriptor->appendChild( $KeyInfo );
      }
      {//signing use certificate
	$KeyInfo = $context->createElementNS( SamlConst::NS_XML_SIG, "KeyInfo", "ds" );
	$KeyInfo->setAttributeNS('http://www.w3.org/2000/xmlns/', "xmlns:ds", SamlConst::NS_XML_SIG);
	
	$X509Data = $context->createElementNS( SamlConst::NS_XML_SIG, "X509Data", "ds" );
	$KeyInfo->appendChild( $X509Data );
	
	$X509Data->appendChild( $context->createElementNS( SamlConst::NS_XML_SIG, "X509Certificate", "ds", $cert->getPublic() ) );
	
	$KeyDescriptor = $context->createElementNS( SamlConst::NS_2_0_METADATA, "KeyDescriptor", "md" );
	$KeyDescriptor->setAttribute("use", SamlConst::KEY_USE_ENC );
	$IDPSSODescriptor->appendChild( $KeyDescriptor );
	
	$KeyDescriptor->appendChild( $KeyInfo );
      }
    }
    
    $NameIDFormat = SamlConst::NAMEIDFORMAT_2_0_TRANSIENT;
    if( isset($metaData->NameIDFormat) ){
      $NameIDFormat = $metaData->NameIDFormat;
    }
    $IDPSSODescriptor->appendChild( $context->createElementNS( SamlConst::NS_2_0_METADATA, "NameIDFormat", null, $NameIDFormat ) );
    
    //Start Processing of child elements
    $encoderData = new XmlEncodeContextData();
    $encoderData->setRoot( $IDPSSODescriptor );
    
    $endPoints = $metaData->getEndpoints();
    reset($endPoints);
    while( list($type, $bindings) = each($endPoints) ){
      reset($bindings);
      while( list(, $binding) = each($bindings) ){
	$encoderData->setSource( $context->createElementNS( SamlConst::NS_2_0_METADATA, $type ) );
	if( $domChild = $context->xmlEncode( $binding, $encoderData ) ){
	  $IDPSSODescriptor->appendChild($domChild);
	}
      }
    } unset($endPoints);
    return $EntityDescriptor;
  }
  
}