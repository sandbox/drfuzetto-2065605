<?php


interface SamlConst {
    //Constants as defined in SAML Specification 8.2 Attribute Name Format Identifiers (http://docs.oasis-open.org/security/saml/v2.0/saml-core-2.0-os.pdf)
  const NAMEIDFORMAT_1_1_UNSPECIFIED                = "urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified";
  const NAMEIDFORMAT_1_1_EMAILADDRESS               = "urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress";
  const NAMEIDFORMAT_1_1_X509SUBJECTNAME            = "urn:oasis:names:tc:SAML:1.1:nameid-format:X509SubjectName";
  const NAMEIDFORMAT_1_1_WINDOWSDOMAINQUALIFIEDNAME = "urn:oasis:names:tc:SAML:1.1:nameid-format:WindowsDomainQualifiedName";
  const NAMEIDFORMAT_2_0_BASIC                      = "urn:oasis:names:tc:SAML:2.0:attrname-format:basic";
  const NAMEIDFORMAT_2_0_URI                        = "urn:oasis:names:tc:SAML:2.0:attrname-format:uri";
  const NAMEIDFORMAT_2_0_KERBEROS                   = "urn:oasis:names:tc:SAML:2.0:nameid-format:kerberos";
  const NAMEIDFORMAT_2_0_ENTITY                     = "urn:oasis:names:tc:SAML:2.0:nameid-format:entity";
  const NAMEIDFORMAT_2_0_PERSISTENT                 = "urn:oasis:names:tc:SAML:2.0:nameid-format:persistent";
  const NAMEIDFORMAT_2_0_TRANSIENT                  = "urn:oasis:names:tc:SAML:2.0:nameid-format:transient";
  
  const ATTRNAMEFORMAT_2_0_UNSPECIFIED              = "urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified";
  
  const BINDING_2_0_HTTP_REDIRECT                   = "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect";
  const BINDING_2_0_HTTP_POST                       = "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST";
  const BINDING_2_0_HTTP_ARTIFACT                   = "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Artifact";
  const BINDING_2_0_SOAP                            = "urn:oasis:names:tc:SAML:2.0:bindings:SOAP";
  
  
  const METHOD_2_0_BEARER                           = "urn:oasis:names:tc:SAML:2.0:cm:bearer";
  
  const ENCODING_2_0_DEFLATE                        = "urn:oasis:names:tc:SAML:2.0:bindings:URL-Encoding:DEFLATE";
  
  const NS_2_0_ASSERTION = "urn:oasis:names:tc:SAML:2.0:assertion";
  const NS_2_0_PROTOCOL  = "urn:oasis:names:tc:SAML:2.0:protocol";
  const NS_2_0_METADATA  = "urn:oasis:names:tc:SAML:2.0:metadata";
  const NS_XML_SIG       = "http://www.w3.org/2000/09/xmldsig#";
  
  const KEY_USE_SIGN     = "signing";
  const KEY_USE_ENC      = "encryption";
  
}
