<?php

/**
 * This interface is used to identify any class that can be returned
 * from a services handleXXX method.
 */
interface FederationMessageContext {
}

/**
 * This class is the return value from any samlService handleXXX method.
 */
class SamlMessageContext extends SamlDependent implements FederationMessageContext {
  private $data;
  private $parentContext;
  private $initialized;
  private $encoderMap;
  private $decoderMap;
  
  public function __construct(  ){
    
    $this->initialized = false;
    $this->parentContext = null;
    $this->encoderMap = array();
    $this->decoderMap = array();
    $this->data = array(
      'requestXML'          => null,
      'responseXML'         => null,
      'binding'             => false,
      'bindingType'         => null,
      'encoding'            => false,
      'relayState'          => false,
      'request'             => null,
      'response'            => null,
      'authnEntity'         => null,
      'authnRoleDescriptor' => null,
      'serviceEntity'       => null,
      'serviceDescriptor'   => null
    );
  }
  
  public function addDecoderMapping( $ns, $map ){
    $this->decoderMap[$ns] = $map;
  }
  
  public function clearDecoderMappings(){
    $this->decoderMap = array();
  }
  
  public function addEncoderMap( array $map ){
    $this->encoderMap += $map;
  }
  
  public function clearEncoderMappings(){
    $this->encoderMap = array();
  }
  
  public function __set( $key, $value ){
    if( !array_key_exists( $key, $this->data ) ){
      throw new Exception( "{$key} is not a valid property for a SamlMessageContext");
    }

    switch( $key ){
      case "encoding":
	if($value != SamlConst::ENCODING_2_0_DEFLATE){
	  throw new Exception('Unknown SAMLEncoding: ' . $value);
	}
      break;
      case "requestXML":
	$this->data['request'] = null;
	$encoding = $this->data['encoding'];
	if( substr($value, 0, 1) != "<" ){
	  $encoding = SamlConst::ENCODING_2_0_DEFLATE;
	}
    
	if ( $encoding !== false  ) {
	  switch($encoding){
	    case SamlConst::ENCODING_2_0_DEFLATE:
	      $value = gzinflate($value);
	    break;
	    default:
	      throw new Exception('Unknown SAMLEncoding: ' . $encoding);
	    }
	} unset($encoding);
      break;
      case "responseXML":
	$this->data['response'] = null;
      break;
      case 'request':
	$this->data['requestXML'] = null;
      break;
      case 'response':
	$this->data['responseXML'] = null;
      break;
      
      case "authnEntity": case "authnRoleDescriptor": case "serviceDescriptor": case "serviceEntity":
	if( $this->parentContext ){
	  $this->parentContext->{$key} = $value;
	}
      break;
    }
    $this->data[$key] = $value;
  }
  
  public function __get( $key ){
    if( !array_key_exists( $key, $this->data ) ){
      throw new Exception( "{$key} is not a valid property for a SamlMessageContext");
    }
    switch( $key ){
      case "responseXML":
	if( !is_null($this->data['response']) && is_null($this->data['responseXML']) ){
	    if( $xml = $this->getAPI()->xmlEncode( $this->data['response'], array( 'map' => $this->encoderMap ) ) ){
	      $this->data["responseXML"] = $xml;
	    }
	}
      break;
      case "requestXML":
	if( is_null($this->data['requestXML']) && !is_null($this->data['request']) ){
	    if( $xml = $this->getAPI()->xmlEncode( $this->data['request'], array( 'map' => $this->encoderMap ) ) ){
	      $this->data["requestXML"] = $xml;
	    }
	}
      break;
      case "request":
	if( !is_null($this->data['requestXML']) && is_null( $this->data["request"] ) ){
	    $this->data["request"] = $this->getAPI()->xmlDecode( $this->data['requestXML'], array( 'map' => $this->decoderMap ) );
	}
      break;
      case "response":
	if( !is_null($this->data['responseXML']) && is_null( $this->data["response"] ) ){
	    $this->data["response"] = $this->getAPI()->xmlDecode( $this->data['responseXML'], array( 'map' => $this->decoderMap ) );
	}
      break;
    }
    return $this->data[$key];
  }
  
  public function __isset( $key ){
    return array_key_exists( $key, $this->data );
  }
  
  public function getParentContext(){
    return $this->parentContext;
  }
  
  public function createChildContext(){
    $ctx = new SamlMessageContext($this->getAPI());
    $ctx->data = (array)$this->data;
    $ctx->parentContext = &$this;
    $ctx->initialized = $this->initialized;
    $ctx->encoderMap = &$this->encoderMap;
    $ctx->decoderMap = &$this->decoderMap;
    return $ctx;
  }
  
  public function initialize(){
    if( $this->initialized ){
      return ;
    }
    
    $this->initialized = true;
    
    if( isset($_SERVER['CONTENT_TYPE']) &&  strtolower( $_SERVER['CONTENT_TYPE'] ) == "application/soap+xml" ){
      $this->data['bindingType'] = SAML20Names::BINDINGS_SOAP;
    }
    
    if( isset($_SERVER['HTTP_ACCEPTS']) &&  strtolower( $_SERVER['HTTP_ACCEPTS'] ) == "application/soap+xml" ){
      $this->data['bindingType'] = SAML20Names::BINDINGS_SOAP;
    }
    
    if( $this->data['bindingType'] ==  SAML20Names::BINDINGS_SOAP ){
      $message = "";
      if( $fp = fopen( "php://input", "r" ) ){
	while( $chunk = fread($fp, 128) ){
	  $message .= $chunk;
	}
      }
      
      if( empty($message) ){
	throw new Exception("Unable to read SOAP message;");
      }
      
      $this->data['requestXML'] = $message;  unset($message);
    }
    else {
      switch( strtoupper($_SERVER['REQUEST_METHOD'] )  ){
	case "GET":
	  if( isset( $_GET['SAMLEncoding']) ){
	    $this->data['encoding'] = $_GET['SAMLEncoding'];
	  }
	  
	  if( isset( $_GET['SAML_RelayState'])  ){
	    $this->data['relayState'] = $_GET['SAML_RelayState'];
	  }
	  
	  if( isset($_GET['SAMLRequest']) ){
	    $this->data['requestXML'] = base64_decode($_GET['SAMLRequest']);
	  }
	  
	  if( isset($_GET['SAMLResponse']) ){
	    $this->data['responseXML'] = base64_decode($_GET['SAMLResponse']);
	  }
	  
	  if( isset( $_GET['SAMLEncoding']) ){
	    $this->data['encoding'] = $_GET['SAMLEncoding'];
	  }
	  
	  if( $this->data['requestXML'] ||  $this->data['responseXML'] ){
	    $this->data['bindingType'] = SAML20Names::BINDINGS_HTTP_REDIRECT;
	  }
	  
	break;
	case "POST":
	  if( isset( $_POST['SAMLEncoding']) ){
	    $this->data['encoding'] = $_POST['SAMLEncoding'];
	  }
	  
	  if( isset( $_POST['SAML_RelayState'])  ){
	    $this->data['relayState'] = $_POST['SAML_RelayState'];
	  }
	  
	  if( isset($_POST['SAMLRequest']) ){
	    $this->data['requestXML'] = base64_decode($_POST['SAMLRequest']);
	  }
	  
	  if( isset($_POST['SAMLResponse']) ){
	    $this->data['responseXML'] = base64_decode($_POST['SAMLResponse']);
	  }
	  
	  if( $this->data['requestXML'] || $this->data['responseXML'] ){
	    $this->data['bindingType'] = SAML20Names::BINDINGS_HTTP_POST;
	  }
	break;
      }
    }
    
  }
  
}
/**
 * This class provides handles all the 
 */
class Saml implements SamlConst {

  private static $instance = null;
  
  public static function getInstance(  ){
    if(is_null( self::$instance ) ){
      self::$instance = new Saml();
    }
    return self::$instance;
  }
  
  public static function fromXML( $source, $map = array() ){
    $options = array(
      'map' => $map
    );
    return self::getInstance()->XMLdecode( $source, $options );
  }
  
  public static function toXML( $source, $options = array() ){
    return self::getInstance()->XMLencode( $source, $options );
  }
  
  public static function generateId($length = 21){
    return "_".Saml::stringToHex( Saml::generateRandomBytes($length) );
  }

  public static function stringToHex($bytes){
      $ret = '';
      for($i = 0; $i < strlen($bytes); $i++) {
	      $ret .= sprintf('%02x', ord($bytes[$i]));
      }
      return $ret;
  }

  public static function generateRandomBytes($length) {

	$data = '';
	for($i = 0; $i < $length; $i++) {
	  $data .= chr(mt_rand(0, 255));
	}

	return $data;
  }

  private $services;
  private $encoderMap;
  private $decoderMap;
  
  public function __construct(){
    $this->services = array();
    
    {//init encoderMap
      $this->encoderMap = array(
	XmlCodec::DEFAULT_URN => new SamlService20Encoder(),
      );
      
      $this->encoderMap['SAML20CodecEntityDescriptor'] =
      $this->encoderMap['SAML20CodecIDPSSODescriptor'] =
      $this->encoderMap['SAML20CodecSPSSODescriptor'] =
      $this->encoderMap['SAML20CodecSSODescriptor'] =
      $this->encoderMap['SAML20CodecAuthnAuthorityDescriptor'] =
      $this->encoderMap['SAML20CodecRoleDescriptor'] =
      $this->encoderMap['SAML20CodecEndpoint'] =
      $this->encoderMap['SAML20CodecIndexedEndpoint'] =
      $this->encoderMap['SAML20CodecKeyDescriptor'] =
      $this->encoderMap['SAMLServiceKeyDescriptor'] =
      $this->encoderMap['SAMLServiceKeyDescriptor'] =
      $this->encoderMap['SAML20CodecRequestedAttribute'] =
      $this->encoderMap['SAML20CodecAttributeConsumingService'] =
      new Saml20CodecMetadataEncoder();
      
      $this->encoderMap['SAML20ExtenstionAssertionAttribute'] =
      $this->encoderMap['SAML20ExtenstionAssertionAttributes'] =
      new SAML20ExtenstionAssertionAttributesEncoder();
      
      $this->encoderMap['SOAPMessage'] = new SOAPEncoder();
      $this->encoderMap['WSSEUsernameToken'] =  $this->encoderMap['WSSE11EncryptedHeader'] = new WSSEEncoder();
    }
    
    {//init decoderMap
      $wsse = new WSSEDecoder();
      $this->decoderMap = array(
	XmlCodec::DEFAULT_URN => array(
	  XmlCodec::DEFAULT_URN => new Saml20Decoder()
	),
	SAML20Codec::METADATA_URN => array(
	 XmlCodec::DEFAULT_URN => new Saml20CodecMetadataDecoder()
	),
	SAML20ExtenstionAssertionAttributesCodec::DEFAULT_URN => array(
	  XmlCodec::DEFAULT_URN => new SAML20ExtenstionAssertionAttributesDecoder()
	),
	WSSECodec::WSSE_URN => array(
	  XmlCodec::DEFAULT_URN => $wsse
	),
	WSSECodec::WSSE11_URN => array(
	  XmlCodec::DEFAULT_URN => $wsse
	),
	WSSECodec::WSU_URN => array(
	  XmlCodec::DEFAULT_URN => $wsse
	),
	SOAPCodec::NS_URN => array(
	  XmlCodec::DEFAULT_URN => new SOAPDecoder()
	)
      );
      
      unset($wsse);
    }
    
    
    $hook = "saml_init";
    $args = array(
      &$this
    );
    foreach (module_implements($hook) as $module) {
      $function = $module . '_' . $hook;
      if (function_exists($function)) {
	call_user_func_array($function, $args);
      }
    }
  }

  public function addDecoderMap( array $map ){
    $this->decoderMap += $map;
  }
  
  public function clearDecoderMappings(){
    $this->decoderMap = array();
  }
  
  public function addEncoderMap( array $map ){
    $this->encoderMap += $map;
  }
  
  public function clearEncoderMappings(){
    $this->encoderMap = array();
  }
  
  public function xmlEncode( $source, $options = array() ){
  
    $map = $this->encoderMap;
    
    if( array_key_exists( 'map', $options ) ){
      $map = array_merge( $map, $options['map'] );
    }

    return XmlCodec::encode( $source, $map, $options );
  }
  
  public function xmlDecode( $source, $options = array() ){
  
    $map = $this->decoderMap;
    
    if( array_key_exists( 'map', $options ) ){
      $map = array_merge_recursive( $map, $options['map'] );
    }

    return XmlCodec::decode( $source, $map );
  }
  
  public function getService( $name ){
    return $this->services[$name];
  }
  
  public function registerService( $name, SamlService $service ){
    $this->services[$name] = $service;
  }
  
  public function getEntity( $entityId ){
    if( $result = db_select("saml_entity","e")
    ->fields('e')
    ->condition( "e.entityid", $entityId )
    ->execute()->fetchObject() ){
      return new SamlEntity( $result->id, $result->entityid, $result->name );
    }
  }
  
  public function getEntityById( $id ){
    if( $result = db_select("saml_entity","e")
    ->fields('e')
    ->condition( "e.id", $id )
    ->execute()->fetchObject() ){
      return new SamlEntity( $result->id, $result->entityid, $result->name );
    }
  }
  
  public function saveEntity( SamlEntity $entity ){
    $updates = array();
    
    $entity_record = (object) array(
      'name'     => $entity->getName(),
      "entityid" => $entity->getEntityId(),
      'options'  => $entity->getOptions()
    );
    
    if( $stored_entity = $this->getEntity($entity->getEntityId()) ){
      $updates[] = 'id';
      $entity_record->id = $stored_entity->getId();
    }
    else if( $entity->getId() > 0 && db_select("saml_entity","e")->fields('e', array('id'))->condition( "e.id", $entity->getId() )->execute()->fetchColumn() ){
      $updates[] = 'id';
      $entity_record->id = $entity->getId();
    }

    drupal_write_record( "saml_entity", $entity_record, $updates );
    
    $ret = new SAMLEntity( $entity_record->id, $entity_record->name, $entity_record->entityid);
    
    unset($entity_record);
    
    $descriptors = $entity->getDescriptor()->getDescriptors();
    $entity->getDescriptor()->clearDescriptors();
    reset($descriptors);
    while( list(,$descriptor) = each($descriptors) ){
      if( $keyDescriptors = $descriptor->getKeyDescriptors() ){
	$descriptor->clearKeyDescriptors();
	reset($keyDescriptors);
	while( list(,$keyDescriptor) = each($keyDescriptors) ){
	  if( $keyPair = $keyDescriptor->getKeyPair() ){
	    $updates = array();
	    $record = (object)array(
	      'privatekeypassphrase' => $keyPair->getPrivateKeyPassphrase(),
	      'privatekey'           => $keyPair->getPrivateKey(),
	      'publickey'            => $keyPair->getPublicKey(),
	    );
	      
	    if( ($keyDescriptor instanceof SAMLServiceKeyDescriptor) && $keyDescriptor->getKeyPairId() > 0 ){
	      $updates[] = "id";
	      $record->id = $keyDescriptor->getKeyPairId();
	    }
	    drupal_write_record( "saml_keypair", $record, $updates );
	    
	    $storedKeyDescriptor = new SAMLServiceKeyDescriptor( $record->id );
	    $storedKeyDescriptor->setkeyPair($keyPair);
	    $storedKeyDescriptor->setUse($keyDescriptor->getUse());
	    
	    $descriptor->addKeyDescriptor($storedKeyDescriptor); unset($storedKeyDescriptor);
	  }
	  else {
	    $descriptor->addKeyDescriptor($keyDescriptor);
	  }
	}
      }
      $entity->getDescriptor()->addDescriptor($descriptor); unset($descriptor);
    }
    
    $updates = array();
        
    $descriptor_record = (object) array(
      "entityid" => $ret->getId(),
      'data'     => $this->xmlEncode( $entity->getDescriptor(), array(
	'map' => array(
	  'SAMLServiceKeyDescriptor' => new SAMLServiceKeyDescriptorEncoder()
	)
      ) )
    );
    
    if( $id = db_query("SELECT id FROM {saml_entity_descriptor} WHERE entityid = :entityid", array( ":entityid" => $ret->getId() ))->fetchColumn() ){
      $updates[] = 'id';
      $descriptor_record->id = $id ;
    }
    
    drupal_write_record( "saml_entity_descriptor", $descriptor_record, $updates );
    
    $hook = "saml_entity_update";
    $args = array(
      &$ret,
      &$this
    );
    foreach (module_implements($hook) as $module) {
      $function = $module . '_' . $hook;
      if (function_exists($function)) {
	call_user_func_array($function, $args);
      }
    } unset($args); unset($hook);
    
    return $ret;
  }
  
  public function deleteEntity( $entityId ){
    if( $entity = $this->getEntity($entityId) ){
      $hook = "saml_entity_delete";
      $args = array(
	&$entity,
	&$this
      );
      foreach (module_implements($hook) as $module) {
	$function = $module . '_' . $hook;
	if (function_exists($function)) {
	  call_user_func_array($function, $args);
	}
      } unset($args); unset($hook);
      
      
      db_delete("saml_entity_descriptor")->condition("entityid",$entity->getId())->execute();
      db_delete("saml_entity")->condition("id",$entity->getId())->execute();
      
      return true;
    }
    return false;
  
  }
  
}

class SamlDependent {
  private $api;
  
  public function __construct(Saml $api){
    $this->api = $api;
  }
  
  protected function getAPI(){
    if( is_null($this->api) ){
      $this->api = Saml::getInstance();
    }
    return $this->api;
  }
}

abstract class SamlService extends SamlDependent {
  
  protected function verifyResponseIsUnique( Saml20CodecResponse $message ){
     if( !($issuer = $message->getIssuer()) || ( !($value = $issuer->getValue()) || empty( $value ) ) ){
      throw new Exception("Unable to determine the issuer of the message");
    } unset($issuer); unset($value);
    
    $this->verifyMessageIsUnique(
      $message->getId(),
      $message->getIssuer()->getValue(),
      $message->getissueInstant()
    );
  }
  
  protected function verifyRequestIsUnique( SAML20CodecAuthnRequest $message ){
     if( !($issuer = $message->getIssuer()) || ( !($value = $issuer->getValue()) || empty( $value ) ) ){
      throw new Exception("Unable to determine the issuer of the message");
    } unset($issuer); unset($value);
    
    $this->verifyMessageIsUnique(
      $message->getId(),
      $message->getIssuer()->getValue(),
      $message->getissueInstant()
    );
  }
  
  protected function verifyMessageIsUnique( $id, $entityId, $issueInstance ){
  
    $record = (object)array(
      'mid'          => $id,
      'entityId'     => $entityId,
      'issueInstant' => $issueInstance
    );
    
    if( ($db_record = db_query( "SELECT mid, entityId as entityid FROM {saml_service_processed_messages} WHERE mid = :mid", array( ":mid" => $record->mid) )->fetchAssoc() ) && $db_record['entityid'] == $record->entityId ){
      throw new Exception("Message was already received.");
    } unset($db_record);
    
    drupal_write_record('saml_service_processed_messages', $record);  unset($record);
  }
  
  
  protected function verifySaml20Conditions( Saml20CodecConditions $conditions ){
    if( ($notBefore = $conditions->getNotBefore()) && !empty($notBefore) && time() < $notBefore ){
      throw new Exception("Message cannot be accepted before ".date("r",$notBefore));
    }
    
    if( ($notOnOrAfter = $conditions->getNotOnOrAfter()) && !empty($notOnOrAfter) && time() >= $notOnOrAfter ){
      throw new Exception("Message cannot be accepted on or after ".date("r",$notOnOrAfter));
    }
  }
  
}

abstract class SamlMetaData extends SamlDependent  {
  private $id;
  private $name;
  private $entityId;
  
  public function __construct(Saml $api){
    parent::__construct($api);
    
    $this->entityId = $this->name = $this->id = null;
  }
  
  public function getId(){
    return $this->id;
  }
  
  public function setId( $id ){
    $this->id = $id;
  }
  
  public function getName(){
    return $this->name;
  }
  
  public function setName( $name ){
    $this->name = $name;
  }
  
  public function getEntityId(){
    return $this->entityId;
  }
  
  public function setEntityId( $entityId ){
    $this->entityId = $entityId;
  }

  public function save(){
    $this->getAPI()->saveMetaData($this);
  }

  public function delete(){
    $this->getAPI()->deleteMetaData($this);
  }
  
  abstract public function descriptorUnserialize( $data );
  
  abstract public function descriptorSerialize();
}

class SamlEntity {

  private $id;
  private $name;
  private $entityId;
  private $descriptor;
  private $options;
  
  public function __construct( $id, $entityId, $name = null, $descriptor = null ){
    $this->id = $id;
    $this->entityId = $entityId;
    if( is_null($name) ){
      $name = $entityId;
    }
    $this->name = $name;
    
    if( !is_null( $descriptor) ){
      if( !( $descriptor instanceof SAML20CodecEntityDescriptor ) ){
	throw new Exception("descriptor may only be an instance of SAML20CodecEntityDescriptor");
      }
      $this->descriptor = $descriptor;
    }
    $this->options = null;
  }
  
  public function getId(){
    return $this->id;
  }
  
  public function getEntityId(){
    return $this->entityId;
  }
  
  public function getName(){
    return $this->name;
  }
  
  public function setName( $name ){
    $this->name = $name;
  }
  
  public function getDescriptor( $filter = null ){
    if( is_null($this->descriptor) ){
      if( $data = db_select("saml_entity_descriptor","descriptor")
      ->fields("descriptor",array( "data" ))
      ->condition("descriptor.entityid", $this->getId())
      ->execute()->fetchColumn() ){
	$this->descriptor = SAML::fromXML($data,array(
	  SAML20Codec::METADATA_URN => array(
	    'KeyDescriptor' => new SAMLServiceKeyDescriptorDecoder()
	  )
	));
      }
      
    }
    if( $this->descriptor && !is_null( $filter ) ){
      $ret = array_filter( $this->descriptor->getDescriptors(), $filter);
      if( count($ret) == 1 ){
	return array_shift( $ret );
      }
      return $ret;
    }
    return $this->descriptor;
  }
  
  public function setDescriptor( SAML20CodecEntityDescriptor $descriptor ){
    $this->descriptor = $descriptor;
  }
  
  public function &getOptions(){
    if( is_null($this->options) ){
      $this->options = array();
      if(
	($serialized_options = db_select("saml_entity","entity")->fields("entity",array( "options" ))->condition("entity.id", $this->getId())->execute()->fetchColumn() ) &&
	( $options = @unserialize($serialized_options) )
      ){
	$this->options = $options;
      }
    }
    return $this->options;
  }
  
  public function getOption( $name ){
    $options =& $this->getOptions();
    if( array_key_exists( $name, $options ) ){
      return $options[$name];
    }
  }
  
  public function putOption( $name, $value ){
    $options =& $this->getOptions();
    $options[$name] = $value;
  }
  

}
