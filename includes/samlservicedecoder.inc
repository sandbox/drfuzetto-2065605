<?php

class SamlService20Decoder extends Saml20Decoder {
  public function xmlDecode( XmlDecodeContext $context, DOMNode $domNode ){
    if( is_null($domNode)  ){
      throw new Exception("Cannot process a null node.");
    }
    
    if( $obj = parent::xmlDecode( $context, $domNode ) ){
      return $obj;
    }
      
    switch( $domNode->lookupNamespaceURI($domNode->prefix) ){
      case SamlConst::NS_2_0_METADATA:
	switch( $domNode->localName ){
	  case "EntityDescriptor":
	    $document = $domNode->ownerDocument;
	    $xpath = new DOMXpath($document);
	    
	    if( ( $domNodeList = $xpath->query( $document->lookupPrefix(SamlConst::NS_2_0_METADATA).":SPSSODescriptor" ) ) && $domNodeList->length ){
	      $metadata = new SamlSpMetaData( Saml::getInstance() );
	      $this->metadataSSODescriptor( $metadata, $context, $domNode, $domNodeList->item(0));
	      return $metadata;
	    }
	    
	    if( ( $domNodeList = $xpath->query( $document->lookupPrefix(SamlConst::NS_2_0_METADATA).":IDPSSODescriptor" ) ) && $domNodeList->length ){
	      $metadata = new SamlIdpMetaData( Saml::getInstance() );
	      $domDescriptor = $domNodeList->item(0);
	      $this->metadataSSODescriptor( $metadata, $context, $domNode, $domDescriptor);
	      $this->metadataIDPSSODescriptor( $metadata, $context, $domNode, $domDescriptor);
	      return $metadata;
	    }
	    
	    break;
	  case "SingleSignOnService":
	    $endPoint = new SamlEndpointType();
	    $this->metadataSamlEndpointType( $endPoint, $context, $domNode );
	    return $endPoint;
	  case "SingleLogoutService":
	    $endPoint = new SamlEndpointType();
	    $this->metadataSamlEndpointType( $endPoint, $context, $domNode );
	    return $endPoint;
	  case "AssertionConsumerService":
	    $endPoint = new SamlIndexedEndpointType();
	    $this->metadataSamlEndpointType( $endPoint, $context, $domNode );
	    $this->metadataIndexedEndpointType( $endPoint, $context, $domNode );
	    return $endPoint;
	}
      break;
    }
  }
  
  private function metadataSSODescriptor( SamlSSODescriptor &$metaData, XmlDecodeContext $context, DOMNode $domEntityDescriptor, DOMNode $domDescriptor ){
    
    $metaData->setEntityId( $domEntityDescriptor->getAttribute("entityID") );
    $this->xpathParseChildren(
      $metaData,
      array(
	"NameIDFormat"             => array( 'ns' => SamlConst::NS_2_0_METADATA, "method" => "setNameIDFormat" ),
	"SingleLogoutService"      => array( 'ns' => SamlConst::NS_2_0_METADATA, "method" => "addSingleLogoutService", "max" => 0 ),
	"AssertionConsumerService" => array( 'ns' => SamlConst::NS_2_0_METADATA, "method" => "addAssertionConsumerService", "max" => 0  ),
      ),
      $context,
      $domDescriptor
    );
    
    $xpath = new DOMXpath($domEntityDescriptor->ownerDocument);
    if( ($domNodeList = $xpath->query( $domEntityDescriptor->ownerDocument->lookupPrefix(SamlConst::NS_2_0_METADATA).":KeyDescriptor" ,$domDescriptor) )  ){
      for( $i = 0; $i < $domNodeList->length; $i++ ){
	if( $obj = $context->xmlDecode( $domNodeList->item($i) ) ){
	  $metaData->addKeyDescriptor( $obj );
	}
      }
    } unset($xpath);

  }
  
  private function metadataIDPSSODescriptor( SamlIdpMetaData &$metaData, XmlDecodeContext $context, DOMNode $domEntityDescriptor, DOMNode $domDescriptor ){
    $this->xpathParseChildren(
      $metaData,
      array(
	"SingleSignOnService"      => array( 'ns' => SamlConst::NS_2_0_METADATA, "method" => "addSingleSignOnService", "max" => 0 ),
      ),
      $context,
      $domDescriptor
    );


  }
  
  private function metadataSamlEndpointType( SamlEndpointType $endPoint, XmlDecodeContext $context, DOMNode $domNode ){
    if( $attr = $domNode->attributes->getNamedItem("Binding") ){
      $endPoint->setBinding( $attr->nodeValue );
    }
    else {
      throw new Exception("Binding is a required attribute, and cannot be blank");
    }
    
    if( $attr = $domNode->attributes->getNamedItem("Location") ){
      $endPoint->setLocation( $attr->nodeValue );
    }
    else {
      throw new Exception("Location is a required attribute, and cannot be blank");
    }
    
    if( $attr = $domNode->attributes->getNamedItem("ResponseLocation") ){
      $endPoint->setResponseLocation( $attr->ResponseLocation );
    }
  }
  
  private function metadataIndexedEndpointType( SamlIndexedEndpointType $endPoint, XmlDecodeContext $context, DOMNode $domNode ){
    if( $attr = $domNode->attributes->getNamedItem("index") ){
      $endPoint->setIndex( $attr->nodeValue );
    }
    else {
      throw new Exception("Index is a required attribute, and cannot be blank");
    }
    
    if( $attr = $domNode->attributes->getNamedItem("isDefault") ){
      $endPoint->setDefault( $attr->nodeValue );
    }
  }
  
}
