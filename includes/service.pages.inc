<?php


function saml_service_self_descriptor_load( $id = 1 ){
  if( $entity = SAML::getInstance()->getEntityById($id) ){
    return array(
      '#markup' => SAML::toXML( $entity->getDescriptor() )
    );
  }
  return MENU_NOT_FOUND;
}