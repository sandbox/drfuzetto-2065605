<?php

class SAMLSignatureUtils {

  private static function generateRandomPassphrase(){
      {//create private name_space
	list($usec, $sec) = explode(' ', microtime());
	mt_srand( (float) $sec + ((float) $usec * 100000) );
      }
      return md5( mt_rand() );
  }
  
  public static function publicKeyToX509( $key ){
    $ret = "-----BEGIN CERTIFICATE-----\n";
    $ret .= chunk_split(str_replace(array("\r", "\n", "\t", ' '), '', $key), 64);
    $ret .= "-----END CERTIFICATE-----\n";
    return $ret;
  }
  
  public static function x509toPublicKey( $in ){
    $pattern = '/^-----BEGIN CERTIFICATE-----([^-]*)^-----END CERTIFICATE-----/m';
    if (preg_match($pattern, $in, $matches)) {
      return preg_replace('/\s+/', '', $matches[1]);
    }
  }

  public static function generateKeyPair( $options = array() ){
    global $base_url;
    $commonName = str_replace(array("https://","http://"),"", $base_url);
    $options += array(
      'passphrase' => self::generateRandomPassphrase(),
      'privateKey' => '',
      'expires'    => 365,
      'dn'         => array(),
      'config'     => array(),
      'publicKey'  => false
    );
    
    $options['dn'] += array(
	"countryName" => "US",
	"stateOrProvinceName" => "Unspeficied",
	"localityName" => "Unspeficied",
	"organizationName" => "Unspeficied",
	"organizationalUnitName" => "Unspeficied",
	"commonName" => $commonName,
	"emailAddress" => "webmaster@{$commonName}"
    );
    
    $options['config'] += array(
      'digest_alg' => 'sha1',
      "private_key_bits" => 2048,
      'private_key_type' => OPENSSL_KEYTYPE_RSA,
      'encrypt_key'      => false,
    );
    unset($commonName);
    
    $out_private_key = "";
    $privkey = false;
    
    if( empty($options['privateKey']) ){
      // Generate a new private key
      $privkey = openssl_pkey_new($options['config']);
      openssl_pkey_export($privkey, $out_private_key, $options['passphrase'], $options['config']);
    }
    else {
      $out_private_key = $options['privateKey'];
      $privkey = openssl_pkey_get_private($options['privateKey'], $options['passphrase'] );
    }
    
    $sig_array = array(
      'privateKeyPassphrase' => $options['passphrase'],
      'privateKey'           => $out_private_key,
      'publicKey'            => "",
    );
    
    if( $options['publicKey'] ){
      if( !is_numeric($options['expires']) ){
	throw new Exception("Expires option must be a valid integer.");
      }
          
      // Generate a certificate signing request
      $csr = openssl_csr_new($options['dn'], $privkey, $options['config']);
    
      // You will usually want to create a self-signed certificate at this
      // point until your CA fulfills your request.
      // This creates a self-signed cert that is valid for 365 days
      $cert = openssl_csr_sign($csr, null, $privkey, (int)$options['expires']);
    
      $out_public_key = "";
      openssl_x509_export($cert, $out_public_key);
          
      $pattern = '/^-----BEGIN CERTIFICATE-----([^-]*)^-----END CERTIFICATE-----/m';
      if (preg_match($pattern, $out_public_key, $matches)) {
	$sig_array['publicKey'] = preg_replace('/\s+/', '', $matches[1]);
      }
      unset($pattern); unset($out_public_key);
          
      openssl_x509_free( $cert );
      unset($cert);
    
    }
    
    openssl_pkey_free($privkey);
    unset($privkey);
	
    return new StringXMLKeyPair( $sig_array );
  }

}

class SAMLServiceKeyDescriptor extends SAML20CodecKeyDescriptor {

  private $keyPairId;

  public function __construct( $keyPairId = 0 ){
    parent::__construct($keyPairId);
    $this->keyPairId = $keyPairId;
  }
  
  public function getKeyPairId(){
    return $this->keyPairId;
  }
  
  public function getKeyPair(){
    $ret = parent::getKeyPair();
    if( is_null($ret) && !is_null($this->keyPairId) ){
      if( $keypair = db_query( "SELECT * FROM {saml_keypair} WHERE id = :id ", array( ":id" => $this->keyPairId ) )->fetchObject() ){
	parent::setKeyPair( new StringXMLKeyPair(array(
	  "privateKeyPassphrase" => $keypair->privatekeypassphrase,
	  "privateKey" => $keypair->privatekey,
	  "publicKey" => $keypair->publickey,
	) ) );
      }
    }

    return parent::getKeyPair();
  }
  
  public function setKeyPair( XMLKeyPair $keyPair ){
    parent::setKeyPair( $keyPair );
  }
  
}

class SAMLServiceKeyDescriptorEncoder extends AbstractXmlEncoder {

  public function xmlEncode( XmlEncodeContext $context, $source ){
    if( $source instanceof SAMLServiceKeyDescriptor ){
      return $this->keyDescriptor( $context, $source );
    }
  }

  private function keyDescriptor( XmlEncodeContext $context, SAMLServiceKeyDescriptor $source  ){
    $KeyInfo = $context->createElementNS( XMLDigitalSignature::XMLNS, "KeyInfo", "ds" );
    $KeyInfo->setAttributeNS('http://www.w3.org/2000/xmlns/', "xmlns:ds", XMLDigitalSignature::XMLNS);
    
    if( $keyPair = $source->getKeyPair() ){
      $KeyInfo->appendChild( $context->createElementNS( XMLDigitalSignature::XMLNS, "KeyName", "ds", $source->getKeyPairId() ) );
    }
    else {
      $KeyInfo->appendChild( $context->createElementNS( XMLDigitalSignature::XMLNS, "KeyName", "ds", $source->getKeyPairId() ) );
    }
    
    $KeyDescriptor = $context->createElementNS( SAML20Codec::METADATA_URN, "KeyDescriptor", "md" );
    $KeyDescriptor->setAttribute("use", $source->getUse() );
    
    $KeyDescriptor->appendChild( $KeyInfo );
    
    return $KeyDescriptor;
  }
}

class SAMLServiceKeyDescriptorDecoder extends AbstractXmlDecoder {
  public function xmlDecode( XmlDecodeContext $context, DOMNode $node ){
    switch( $node->namespaceURI ){
      case SAML20Codec::METADATA_URN:
	switch( $node->localName ){
	  case "KeyDescriptor":
	    return $this->keyDescriptor( $context,  $node );
	}
      break;
    }
  }
  
  private function keyDescriptor( XmlDecodeContext $context, DOMNode $domNode ){
    $xpath = new DOMXpath($domNode->ownerDocument);
    $xpath->registerNamespace("ds", XMLDigitalSignature::XMLNS);
    $xpath->registerNamespace("md", SAML20Codec::METADATA_URN);
  
    if(
      ( $domElements = $xpath->query("ds:KeyInfo/ds:KeyName", $domNode) ) &&
      $domElements->length > 0
    ){

      $ret = new SAMLServiceKeyDescriptor( $domElements->item(0)->nodeValue );
      if( $attr = $domNode->attributes->getNamedItem("use") ){
	$ret->setUse($attr->nodeValue);
      } unset($attr);
      return $ret;
    }
    
    if(
      ( $domElements = $xpath->query("ds:KeyInfo/ds:X509Data/ds:X509Certificate", $domNode) ) &&
      $domElements->length > 0
    ){
      $ret = new SAMLServiceKeyDescriptor(0);
      if( $attr = $domNode->attributes->getNamedItem("use") ){
	$ret->setUse($attr->nodeValue);
      } unset($attr);
      
      $ret->setKeyPair( new StringXMLKeyPair( array( 'publicKey' => $domElements->item(0)->nodeValue ) ) );
      
      return $ret;
    }
    
    
  }
}