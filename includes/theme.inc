<?php

/**
 * Returns a Data Table with the details of the saml_certificate element
 **/
function theme_saml_certificate_details( $variables ){
  drupal_add_css( drupal_get_path("module", "saml_service"). "/misc/saml_certificate_details.css" );

  $details = $variables['certificate']['#details'];

  $ret = '<dl class="saml-certificate-details">';
  
  $ret .= "<dt>".t('Organization Name (eg, company)').":</dt>";
  $ret .= "<dd>".$details['subject']['O']."&nbsp;</dd>";
  
  $ret .= "<dt>".t('Organizational Unit Name (eg, section)').":</dt>";
  $ret .= "<dd>".$details['subject']['OU']."&nbsp;</dd>";
  
  $ret .= "<dt>".t("Common Name (eg, your name or your server's hostname)").":</dt>";
  $ret .= "<dd>".$details['subject']['CN']."&nbsp;</dd>";
  
  $ret .= "<dt>".t('Email Address').":</dt>";
  $ret .= "<dd>".$details['subject']['emailAddress']."&nbsp;</dd>";
  
  $ret .= "<dt>".t('Valid To').":</dt>";
  $ret .= "<dd>".format_date( saml_service_openssl_to_timestamp($details['validTo']), 'long' )."</dd>";
  
  $ret .= "</dl>";
  return $ret;
}