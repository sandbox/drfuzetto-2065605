<?php


abstract class Certificate implements Serializable {
  
  /**
   * Returns a string containing the key data.
   * @returns a string containing the key data
   */
  public abstract function getPrivate();
  
  /**
   * Returns a string containing the key data.
   * @returns a string containing the key data
   */
  public abstract function getPublic();
  
}

class X509Certificate extends Certificate  {

  const STORAGE_TYPE_STR = "str";
  const STORAGE_TYPE_PEM_STR = "pem_str";
  const STORAGE_TYPE_FILE = "file";
  
  private static function generateRandomPassphrase(){
      {//create private name_space
	list($usec, $sec) = explode(' ', microtime());
	mt_srand( (float) $sec + ((float) $usec * 100000) );
      }
      return md5( mt_rand() );
  }
  
  public static function generate( $options = array() ){
    global $base_url;
    $commonName = str_replace(array("https://","http://"),"", $base_url);
    $options += array(
      'passphrase' => self::generateRandomPassphrase(),
      'expires'    => 3652,
      'dn'         => array(
	  "countryName" => "US",
	  "stateOrProvinceName" => "Unspeficied",
	  "localityName" => "Unspeficied",
	  "organizationName" => "Unspeficied",
	  "organizationalUnitName" => "Unspeficied",
	  "commonName" => $commonName,
	  "emailAddress" => "webmaster@{$commonName}"
      ),
      'config'  => array(
	'digest_alg' => 'sha1',
	"private_key_bits" => 2048,
	'private_key_type' => OPENSSL_KEYTYPE_RSA,
	'encrypt_key'      => false,
      )
    );
    unset($commonName);
    
    if( !is_numeric($options['expires']) ){
      throw new Exception("Expires option must be a valid integer.");
    }
    
    // Generate a new private (and public) key pair
    $privkey = openssl_pkey_new($options['config']);
    
    
    // Generate a certificate signing request
    $csr = openssl_csr_new($options['dn'], $privkey, $options['config']);
    
    // You will usually want to create a self-signed certificate at this
    // point until your CA fulfills your request.
    // This creates a self-signed cert that is valid for 365 days
    $cert = openssl_csr_sign($csr, null, $privkey, 365);
        
    $out_private_key = ""; $out_public_key = "";
    openssl_pkey_export($privkey, $out_private_key, $options['passphrase'], $options['config']); openssl_pkey_free($privkey);
    openssl_x509_export($cert, $out_public_key); openssl_x509_free( $cert );
    return new X509Certificate($options['passphrase'], array( "private" => $out_private_key, "public" => $out_public_key, 'storage_type' => X509Certificate::STORAGE_TYPE_PEM_STR ));
  }
  
  private $passphrase, $pub_stor, $priv_stor, $stor_type, $pub_out, $priv_out;
  
  public function __construct( $passphrase, $options = array() ){
    $options += array(
      "storage_type" => null,
      "private"   => null,
      "public"    => null
    );
    
    $this->passphrase = $passphrase;
    
    $this->priv_stor = $options['private'];
    $this->pub_stor = $options['public'];
    $this->stor_type = $options['storage_type'];
  }
  
  public function getPassphrase(){
    return $this->passphrase;
  }
  
  public function getPrivate(){
    if( is_null($this->priv_out) ){
      if($this->stor_type == self::STORAGE_TYPE_FILE){
	$this->priv_out = file_get_contents($this->priv_stor );
      }
      else {
	$this->priv_out = $this->priv_stor;
      }
      if( is_null($this->priv_out) ){
	throw new Exception("Unable to load private key");
      }
    }
    return $this->priv_out;
  }
  
  public function getPublic(){
    if( is_null($this->pub_out) ){
      $pattern = '/^-----BEGIN CERTIFICATE-----([^-]*)^-----END CERTIFICATE-----/m';
      switch($this->stor_type){
	case self::STORAGE_TYPE_FILE:
	  $data = file_get_contents($this->pub_stor);
	  if (preg_match($pattern, $data, $matches)) {
	    $this->pub_out = preg_replace('/\s+/', '', $matches[1]);
	  } unset($data);
	break;
	case self::STORAGE_TYPE_PEM_STR:
	  if (preg_match($pattern, $this->pub_stor, $matches)) {
	    $this->pub_out = preg_replace('/\s+/', '', $matches[1]);
	  }
	break;
	default:
	  $this->pub_out = $this->pub_stor;
      } unset($pattern);
      if( is_null($this->pub_out) ){
	throw new Exception("Unable to load public key");
      }
    }
    return $this->pub_out;
  }
  
  public function getPublicAsPEM(){
    if( is_null($this->pub_out) ){
      switch($this->stor_type){
	case self::STORAGE_TYPE_FILE:
	  $this->pub_out = file_get_contents($this->pub_stor);
	break;
	default:
	  $this->pub_out = $this->pub_stor;
      }
      if( is_null($this->pub_out) ){
	throw new Exception("Unable to load public key");
      }
    }
    return $this->pub_out;
  }
  
  public function exportPublic( $file ){
    switch($this->stor_type){
      case self::STORAGE_TYPE_FILE:
	copy( $this->pub_stor, $file );
      break;
      case self::STORAGE_TYPE_PEM_STR:
	$cert = openssl_x509_read( $this->pub_stor  );
    
	openssl_x509_export_to_file( $cert, $file);
	openssl_x509_free( $cert );
      break;
    } 
  }
  
  public function exportPrivate( $file ){
    switch($this->stor_type){
      case self::STORAGE_TYPE_FILE:
	copy( $this->priv_stor, $file );
      break;
      case self::STORAGE_TYPE_PEM_STR:
	if( $fp = fopen($file, "w") ){
	  fwrite( $fp, $this->priv_stor );
	  fclose( $fp );
	}
      break;
    } 
  }
  
  public function serialize() {
    return serialize( array( "passphrase" => $this->passphrase, "stor_type" => $this->stor_type, "pub_stor" => $this->pub_stor, "priv_stor" => $this->priv_stor ) );
  }
  
  public function unserialize($data) {
      $data = unserialize($data);
      foreach( $data as $key=>$value ){
	$this->{$key} = $value;
      }
      unset($data);
  }
}

class SamlEndpointType implements SamlConst, Serializable {
  
  private $binding, $location, $responseLocation;
  
  public function getBinding(){
    return $this->binding;
  }
  
  public function setBinding($binding){
    $this->binding = $binding;
  }
  
  public function getLocation(){
    return $this->location;
  }
  
  public function setLocation( $location ){
    $this->location = $location;
  }
  
  public function getResponseLocation(){
    return $this->responseLocation;
  }
  
  public function setResponseLocation( $responseLocation ){
    $this->responseLocation = $responseLocation;
  }
  
  
  public function serialize() {
      return serialize( array( "binding" => $this->binding, "location" => $this->location, "responseLocation" => $this->responseLocation ));
  }
  
  public function unserialize($data) {
      $data = unserialize($data);
      foreach( $data as $key=>$value ){
        $this->{$key} = $value;
      }
      unset($data);
  }
}
  

class SamlIndexedEndpointType extends SamlEndpointType {
  
  private $index, $_default;
  
  public function __construct(){
    $this->index = 0;
  }
  
  public function getIndex(){
    return $this->index;
  }

  public function setIndex( $index ){
    $this->index = $index;
  }
  
  public function isDefault(){
    return $this->_default;
  }
  
  public function setDefault( $default ){
    $this->_default = $default;
  }

  public function serialize() {
      return serialize( array( "setBinding" => $this->getBinding(), "setLocation" => $this->getLocation(), "setResponseLocation" => $this->getResponseLocation(), "setDefault" => $this->_default, "setIndex" => $this->index ));
  }
  
  public function unserialize($data) {
      $data = unserialize($data);
      foreach( $data as $key=>$value ){
        if( !is_array($value) ){
          $value = array( $value );
        }
        call_user_func_array( array( &$this, $key ), $value );
      }
      unset($data);
  }

}

interface SamlMetaDataSource extends Serializable {
  public function getMetadata();
}

class SamlXMLMetaDataSource implements SamlMetaDataSource {
  
  private $xml;
  
  public function __construct( $xml ){
    if( is_string($xml) ){
      $this->xml = $xml;
    }
  }
  
  public function getXML(){
    return $this->xml;
  }
  
  public function setXML( $xml ){
    $this->xml = $xml;
  }
  
  public function getMetadata( $callback = null ){
    if( is_null($callback) ){
      $callback = array( "SAML", "fromXML" );
    }

    if( $ret = call_user_func( $callback, $this->getXML() ) ){
      $ret->setSource($this);
      return $ret;
    }
  }
  
  public function serialize(){
    return serialize( array( 'setXML' => base64_encode( $this->getXML() ) ) );
  }
  
  public function unserialize($data) {
    if( $data_array = @unserialize($data) ){
      $this->setXML( base64_decode($data_array['setXML']) );
    }unset($data_array);
  }
  
}