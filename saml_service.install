<?php

/**
 * Implements hook_schema()
 */
function saml_service_schema(){
  $schema = array();
  
  $schema['saml_service_processed_messages'] = array(
    'fields' => array(
      'id'  => array(
	'type'     => 'serial'
      ),
      'mid'   => array(
	'type'      => 'varchar',
	'length'    => 128
      ),
      'entityId'   => array(
	'type'      => 'text',
      ),
      'issueInstant'     => array(
	'type'      => 'int',
	'unsigned'  => true,
	'length'    => 10
      )
    ),
    'primary key' => array(
      'id'
    ),
    'indexes'     => array(
      'mid'         => array( 'mid' )
    )
  );
  
  $schema['saml_entity'] = array(
    'fields' => array(
      'id'   => array(
	'type' => 'serial'
      ),
      'name' => array(
	'type'   => 'varchar',
	'length' => 255,
      ),
      'entityid' => array(
	'type'   => 'varchar',
	'length' => 255
      ),
      'options'  => array(
	'type'      => 'text',
	'size'      => 'big',
	'serialize' => true
      )
    ),
    'primary key' => array(
      'id'
    ),
    'unique keys' => array(
      'name'     => array( 'name' ),
      'entityid' => array( 'entityid' )
    )
  );
  
  $schema['saml_entity_descriptor'] = array(
    'fields' => array(
      'id'   => array(
	'type' => 'serial'
      ),
      'entityid' => array(
	'type'   => 'int',
      ),
      'data'      => array(
	'type'      => 'text',
	'size'      => 'big'
      )
    ),
    'primary key' => array(
      'id'
    ),
    'unique keys' => array(
      'entityid'     => array( 'entityid' )
    )
  );
  
  $schema['saml_keypair'] = array(
    'fields' => array(
      'id'   => array(
	'type' => 'serial'
      ),
      'privatekeypassphrase' => array(
	'type'      => 'text'
      ),
      'privatekey'      => array(
	'type'      => 'text'
      ),
      'publickey'      => array(
	'type'      => 'text'
      )
    ),
    'primary key' => array(
      'id'
    )
  );
  
  $schema['saml_service_log'] = array(
    'fields' => array(
      'id'  => array(
	'type'     => 'serial'
      ),
      'created'   => array(
	'type'      => 'int',
	'unsigned'  => true,
	'length'    => 10
      ),
      'request_entityid' => array(
	'type'      => 'text',
      ),
      'request'   => array(
	'type'      => 'text',
	'size'      => 'big'
      ),
      'response_entityid' => array(
	'type'      => 'text',
      ),
      'response'  => array(
	'type'      => 'text',
	'size'      => 'big'
      )
    ),
    'primary key' => array(
      'id'
    )
  );
  
  return $schema;
}

/**
 * Changes:<br/>
 *   Adds new tables: saml_entity, and saml_entity_descriptor.<br/>
 *   Upgrades all meta data to use the new SAMLEntity class.<br/>
 */
function saml_service_update_7200(&$sandbox){  
  drupal_load("module", "codecs");
  registry_rebuild();
  
  if (!class_exists('SAML')) {
    throw new Exception('Missing required class: SAML');
    return; 
  }  
  
  if (!class_exists('SAML20CodecEntityDescriptor')) {
    throw new Exception('Missing required class: SAML20CodecEntityDescriptor');
    return; 
  }
   
  db_create_table(
    'saml_entity',
    array(
      'fields' => array(
	'id'   => array(
	  'type' => 'serial'
	),
	'name' => array(
	  'type'   => 'varchar',
	  'length' => 255,
	),
	'entityid' => array(
	  'type'   => 'varchar',
	  'length' => 255
	),
	'options'  => array(
	  'type'      => 'text',
	  'size'      => 'big',
	  'serialize' => true
	)
      ),
      'primary key' => array(
	'id'
      ),
      'unique keys' => array(
	'name'     => array( 'name' ),
	'entityid' => array( 'entityid' )
      )
    )
  );
  
  db_create_table(
    'saml_entity_descriptor',
    array(
      'fields' => array(
	'id'   => array(
	  'type' => 'serial'
	),
	'entityid' => array(
	  'type'   => 'varchar',
	  'length' => 255
	),
	'data'      => array(
	  'type'      => 'text',
	  'size'      => 'big'
	)
      ),
      'primary key' => array(
	'id'
      ),
      'unique keys' => array(
	'entityid'     => array( 'entityid' )
      )
    )
  );
  
  db_create_table(
    'saml_keypair',
    array(
      'fields' => array(
	'id'   => array(
	  'type' => 'serial'
	),
	'privatekeypassphrase' => array(
	  'type'      => 'text'
	),
	'privatekey'      => array(
	  'type'      => 'text'
	),
	'publickey'      => array(
	  'type'      => 'text'
	)
      ),
      'primary key' => array(
	'id'
      )
    )
  );
  

  $entityDescriptor = new SAML20CodecEntityDescriptor(url("saml2/descriptor",array('absolute'=>true)));
  
  $record = (object) array(
    'id' => 1,
    'name'     => '',
    "entityid" => $entityDescriptor->getEntityId()
  );
  
  drupal_write_record( "saml_entity", $record );
  
  $record = (object) array(
    'id'       => 1,
    "entityid" => $entityDescriptor->getEntityId(),
    'data'     => SAML::toXML($entityDescriptor)
  );
  
  drupal_write_record( "saml_entity_descriptor", $record );
}

/**
 * Implements hook_enable()
 **/
function saml_service_enable(){
  if( !saml_service_application_entity() ){
    $entityId = url( "<front>", array( 'absolute'=>true ) );
    $entity = new SamlEntity( 1, $entityId, $entityId, new SAML20CodecEntityDescriptor($entityId) );
    SAML::getInstance()->saveEntity( $entity );
  }
}

/**
 * Changes:<br/>
 *   Adds new tables: saml_service_log.
 */
function saml_service_update_7203(){  

  db_create_table(
    'saml_service_log',
    array(
      'fields' => array(
	'id'  => array(
	  'type'     => 'serial'
	),
	'created'   => array(
	  'type'      => 'int',
	  'unsigned'  => true,
	  'length'    => 10
	),
	'request_entityid' => array(
	  'type'      => 'text',
	),
	'request'   => array(
	  'type'      => 'text',
	  'size'      => 'big'
	),
	'response_entityid' => array(
	  'type'      => 'text',
	),
	'response'  => array(
	  'type'      => 'text',
	  'size'      => 'big'
	)
      ),
      'primary key' => array(
	'id'
      )
    )
  );
  
}