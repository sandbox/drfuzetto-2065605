<?php

/**
 * Sub task for used in page callback saml_service_federation_admin_page
 */
function saml_service_idp_admin_ssodescriptor_access_form( $form, $form_state ){
  
  
  $form['header'] = array(
    '#markup' => t("Below is a list of role(s) that are able to access this application.")
  );
  
  $form['associated_roles'] = array(
    '#type'   => 'tableselect',
    '#header' => array(
	t('Role(s)')
    ),
    '#empty'   => t("There are no role(s) associated to this application. All users are denied access."),
    '#options' => array()
  );
    
  $query = db_select("saml_service_entity_roles","acl")
    ->condition("acl.entityid", $form_state['entity']->getId() )
    ->extend('PagerDefault')
    ->limit(25);
  
  $query->leftJoin('role', 'r', 'r.rid = acl.rid');
  $query->addTag('translatable');
  $query->addField('r', "name", "role_name");
  $query->addField('acl', "rid");
  
  $results = $query->execute();
  
  while( $result = $results->fetchAssoc() ){
    $form['associated_roles']['#options'][ $result['rid'] ] = array( 
      '#title' => $result['role_name'],
      $result['role_name']
    );
  }
  
  $form['delete'] = array(
    '#type'         => 'submit',
    '#value'        => t('Delete Selected'),
    '#submit'       => array( 'saml_service_idp_admin_ssodescriptor_access_form_delete' )
  );
  
  
  $form['new_role'] = array(
    '#title'        => '',
    '#type'         => 'select',
    '#empty_option' => t('- Available Role(s) -'),
    '#options'      => array()
  );
  
  $sub_query = db_select( "saml_service_entity_roles","acl" )
    ->fields("acl", array( "rid" ))
    ->condition("acl.entityid", $form_state['entity']->getId());
  
  $query = db_select("role","r")
    ->fields("r")
    ->condition("r.rid", DRUPAL_ANONYMOUS_RID, ">")
    ->condition( "r.rid", $sub_query, "NOT IN");
  
  $results = $query->execute();
  while( $result = $results->fetchObject() ){
    $form['new_role']['#options'][$result->rid] = t($result->name);
  }
  
  $form['add'] = array(
    '#type'   => 'submit',
    '#value'  => t('Add'),
    '#submit' => array('saml_service_idp_admin_ssodescriptor_access_form_add')
  );
  
  $form['pager'] = array(
    '#markup' => theme('pager' )
  );
  
  $form['Back'] = array(
    '#type'  => 'link',
    '#title' => t('Back'),
    '#href'  => 'admin/config/services/saml/federation/'.$form_state['entity']->getId(),
    '#attributes' => array(
      'class' => array( 'button')
    )
  );
  
  
  return $form;
}

/**
 * Validation callback for form saml_service_federation_permission_form
 */
function saml_service_idp_admin_ssodescriptor_access_form_validate( $form, &$form_state ){
  switch( $form_state['values']['op'] ){
    case $form['add']['#value']:
      if( !isset( $form_state['values']['new_role'] ) || empty( $form_state['values']['new_role'] ) ){
	form_set_error( 'add', t('No role selection was made, unable to add.') );
      }
    break;
    case $form['delete']['#value']:
      if( !isset( $form_state['values']['associated_roles'] ) || ( is_array( $form_state['values']['associated_roles'] ) && !$form_state['values']['associated_roles'] ) ||empty( $form_state['values']['associated_roles'] ) ){
	form_set_error( 'delete', t('No role selection was made, unable to delete.') );
      }
    break;
  }
}

/**
 * Submit callback for form saml_service_federation_permission_form
 */
function saml_service_idp_admin_ssodescriptor_access_form_add( $form, &$form_state ){
  $ace = (object) array(
    'entityid' => $form_state['entity']->getId(),
    'rid' => $form_state['values']['new_role']
  );
  if( drupal_write_record( "saml_service_entity_roles", $ace ) !== false ){
    drupal_set_message( t( "%name added to access control list.", array( '%name' => $form['new_role']['#options'][$ace->rid] )) );
  }
}

/**
 * Submit callback for form saml_service_federation_permission_form
 */
function saml_service_idp_admin_ssodescriptor_access_form_delete( $form, &$form_state ){
  if( isset($form_state['values']['associated_roles']) && is_array( $form_state['values']['associated_roles'] )){
    db_delete( "saml_service_entity_roles" )->condition("entityid", $form_state['entity']->getId())->condition( "rid", $form_state['values']['associated_roles'], "in")->execute();
    
    drupal_set_message( t("The selected item(s) were removed.") );
  }
}

function saml_service_idp_admin_idpssodescriptor_config_form( $form, &$form_state ){
  if( !isset($form_state['storage']) ){
    $form_state['storage'] = array(
      'bindings'         => array(),
      'accessControl'    => false
    );
    
    $services = $form_state['descriptor']->getSingleSignOnService();
    reset($services);
    while( list( ,$service ) = each($services) ){
      if( !in_array( $service->getBinding(), $form_state['storage']['bindings'] ) ){
	$form_state['storage']['bindings'][] = $service->getBinding();
      }
    }
    unset($services);
  }
    
  $form['supportedBindings'] = array(
    '#title'         => t('Supported Binding(s)'),
    '#type'          => 'select',
    '#options'       => array(
      SamlConst::BINDING_2_0_HTTP_POST     => "HTTP Post",
      SamlConst::BINDING_2_0_HTTP_REDIRECT => "HTTP Redirect",
      SamlConst::BINDING_2_0_SOAP          => "SOAP",
    ),
    '#default_value' => $form_state['storage']['bindings'],
    '#description'   => t("The bindings that are accpeted by this server." ),
    '#required'      => true,
    '#multiple'      => true
  );
  
  $form['#submit'][] = 'saml_service_idp_admin_idpssodescriptor_config_form_submit';
  
  $form['save'] = array(
    '#type'  => 'submit',
    '#value' => t( 'Save' )
  );
  
  return $form;
}

function saml_service_idp_admin_idpssodescriptor_config_form_submit( $form, &$form_state ){
  $form_state['storage']['bindings'] = $form_state['values']['supportedBindings'];
  
  $entity = $form_state['entity'];
  
  if( $entityDescriptor = $entity->getDescriptor() ){
    if( $descriptors = $entityDescriptor->getDescriptors() ){
      $entity->getDescriptor()->clearDescriptors();
      reset($descriptors);
      while( list(,$descriptor) = each($descriptors) ){
	if( $descriptor instanceof SAML20CodecIDPSSODescriptor ){
	  
	  $descriptor->clearSingleSignOnService();
	  $descriptor->clearArtifactResolutionService();
	  $descriptor->clearSingleLogoutService();
	  
	  $bindingIndex = 0;
	
	  reset($form_state['storage']['bindings']);
	  while( list(,$binding) = each($form_state['storage']['bindings']) ){
	      $entity_service = new SAML20CodecEndpoint();
	      $entity_service->setLocation( url( "saml2/service/idp/singleLogoutService", array( 'absolute' => true ) ) );
	      $entity_service->setBinding( $binding );
	      $descriptor->addSingleLogoutService($entity_service); unset($entity_service);
	      
	      $entity_service = new SAML20CodecEndpoint();
	      $entity_service->setLocation( url( "saml2/service/idp/singleSignonService", array( 'absolute' => true ) ) );
	      $entity_service->setBinding( $binding );
	      $descriptor->addSingleSignOnService($entity_service); unset($entity_service);
	      
	      $entity_service = new SAML20CodecIndexedEndpoint();
	      $entity_service->setIndex( $bindingIndex++ );
	      $entity_service->setLocation( url( "saml2/service/idp/artifactResolutionService", array( 'absolute' => true ) ) );
	      $entity_service->setBinding( $binding );
	      $descriptor->addArtifactResolutionService($entity_service); unset($entity_service);
	  }
	  
	}
	
	$entity->getDescriptor()->addDescriptor($descriptor); unset($descriptor);
      }
    }
  }
  
  SAML::getInstance()->saveEntity($entity); unset($entity);

  $form_state['rebuild'] = false;
  unset($form_state['storage']);
}

function saml_service_idp_access_control_page(){
  return array(
    'content' => array(
      '#markup' => "Test"
    )
  );
}
