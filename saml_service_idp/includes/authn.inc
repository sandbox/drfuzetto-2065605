<?php

function saml_service_idp_saml_authncontext_class_constructor( $className, $info = null ){
  return new $className();
}


interface SamlServiceAuthnEvaluator {
  
  public function validate( Saml20CodecSubjectId $nameID, Saml20CodecSubject $subject, SAML20CodecAuthnRequest $request, SamlMessageContext $messageContext );
  
}

class SamlServicePreviousSessionAuthnEvaluator implements SamlServiceAuthnEvaluator {
 
 public function validate( Saml20CodecSubjectId $nameID, Saml20CodecSubject $subject, SAML20CodecAuthnRequest $request, SamlMessageContext $messageContext ){
    global $user, $is_https;
    if( ($session = db_query( "SELECT * FROM {sessions} WHERE sid = :sid OR ssid = :ssid", array( ":sid" => $nameID->getValue(), ":ssid" => $nameID->getValue() ) )->fetchObject()) && ($account = user_load( intval( $session->uid ) ) ) ){
      $old_session_id = false;
      
      if (drupal_session_started()) {
	$old_session_id = session_id();
      }
      
      if ( isset($session->ssid) && $session->ssid ) {
	session_id($session->ssid);
      }
      else {
	session_id($session->sid);
      }
      
      if (isset($old_session_id)) {
	$params = session_get_cookie_params();
	$expire = $params['lifetime'] ? REQUEST_TIME + $params['lifetime'] : 0;
	setcookie(session_name(), session_id(), $expire, $params['path'], $params['domain'], $params['secure'], $params['httponly']);
      }
      
      drupal_session_start();    
      
      $user = $account;
      
      $authnStatement = new Saml20CodecAuthnStatement();
      $authnContext = new Saml20CodecAuthnContext();
      $authnContext->setAuthnContextClassRef(SAML20Names::AUTHNCONTEXT_CLASS_PREVIOUS_SESSION);
      
      $authnStatement->setAuthnContext($authnContext); unset($authnContext);
      
      $authnStatement->setSessionIndex( session_id() );
      
      $assertion = new Saml20CodecAssertion(); 
      
      $assertion->addStatement( $authnStatement ); unset($authnStatement);
      
      return $assertion;
    }
    
    throw new Exception( "Previous Session (".$nameID->getValue().") could not be located" );
 }
 
}