<?php

class SAMLServiceIDP_Development {

  public static function development_general_form_alter( &$form, &$form_state ){
    $form_state['build_info']['files'][] = drupal_get_path("module", "saml_service_idp")."/includes/development.classes.inc";
    
    $form['logging_fieldset']['logging']['#options'][SAML_SERVICE_IDP_LOG_ENTITY_ACCESS] = t('SAML Entity Access');
  
    if( SAMLServiceLogging::hasFlag(SAML_SERVICE_IDP_LOG_ENTITY_ACCESS) ){
      $form['logging_fieldset']['logging']['#default_value'][] = SAML_SERVICE_IDP_LOG_ENTITY_ACCESS;
    }
    
    $form['access_fieldset'] = array(
      '#title' => t('Access'),
      '#type' => 'fieldset'
    );
    
    $form['access_fieldset']['access_retention'] = array(
      '#type'  => 'select',
      '#title' => t('Retention Duration'),
      '#options' => array(
	'-6 months'  => t('6 months'),
	'-1 year'    => t('1 year'),
	'-2 years'   => t('2 years'),
	'-5 years'   => t('5 years'),
	'-10 years'  => t('10 years'),
	-1           => t('Indefinitely')
      ),
      '#default_value' => variable_get( "saml_service_idp:access_retention", "-1 year" )
    );
    
    $form['save']['#weight'] = 10;
    $form['save']['#submit'][] = 'SAMLServiceIDP_Development_form_submit';
  }

}

function SAMLServiceIDP_Development_form_submit( $form, &$form_state ){
  variable_set( "saml_service_idp:access_retention", $form_state['values']['access_retention'] );
}