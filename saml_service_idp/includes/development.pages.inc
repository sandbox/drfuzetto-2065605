<?php

function saml_service_idp_development_descriptor_page( $op = false ){
  $context= array(
    'cid'        => 'idp:desc:'.$GLOBALS['user']->uid,
    'entityid'   => '',
    'descriptor' => '',
    'keys'       => array()
  );
  
  if( $cache = cache_get($context['cid']) ){
    $context['entityid'] = $cache->data['entityid'];
    $context['descriptor'] = $cache->data['descriptor'];
  }

  if( $op == "download" && !empty($context['descriptor']) && ( $entityDescriptor = SAML::fromXML( $context['descriptor'] ) ) ){
    return array(
      '#prefix' => '<pre>',
      '#suffix' => '</pre>',
      '#markup' => htmlentities( $context['descriptor'] )
    );
  }
  
//   if( empty($context['entityid']) ){
    $form_state= array(
      'storage' => $context
    );
    return drupal_build_form( 'saml_service_idp_development_descriptor_form_gather_entityid', $form_state );
//   }
}

function saml_service_idp_development_descriptor_form_gather_entityid( $form, &$form_state ){
  $form = array();  
  
  $form['entityid'] = array(
    '#type' => 'textfield',
    '#title' => t('Entity Id'),
    '#required' => true,
    '#default_value' => $form_state['storage']['entityid']
  );
  
  $form['continue'] = array(
    '#type'  => 'submit',
    '#value' => t('Continue'),
    '#submit' => array( 'saml_service_idp_development_descriptor_form_gather_entityid_submit' )
  );
  
  return $form;
}

function saml_service_idp_development_descriptor_form_gather_entityid_submit( $form, &$form_state  ){
  
  $form_state['storage']['entityid'] = $form_state['values']['entityid'];

  $entityDescriptor = new SAML20CodecEntityDescriptor( $form_state['storage']['entityid'] );
  $entityDescriptor->addDescriptor( new SAML20CodecSPSSODescriptor( "urn:oasis:names:tc:SAML:2.0:protocol" ) );

  $data = array(
    'entityid'   => $form_state['storage']['entityid'],
    'descriptor' =>  SAML::toXML( $entityDescriptor )
  );
 
  cache_set( $form_state['storage']['cid'], $data, 'cache', strtotime( '+1 hour' ) );
  
  unset($form_state['storage']);
  $form_state['redirect'] = "admin/config/development/saml/descriptor/download";
}