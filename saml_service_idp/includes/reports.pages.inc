<?php


function saml_service_idp_access_operation_form(){
  $form = array();
  
  $form['fieldset'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Operations'),
    '#collapsible' => true,
    '#collapsed'   => true
  );
  
  $form['fieldset']['download'] = array(
    '#type'  => 'link',
    '#title' =>  t('Download'),
    '#href'  => 'admin/reports/saml/access/download',
    '#attributes' => array( 'class' => array( 'button' ) )
  );
  
  return $form;
}


/**
 * Page callback for path admin/reports/saml/access
 **/
function saml_service_idp_access_overview(){
  $build = array(
    'operations' => drupal_get_form('saml_service_idp_access_operation_form'),
  );

  $header = array(
    array(
      'data' => t('Access Time'),
      'field' => 'access.access',
      'sort' => 'desc',
    ),
    array(
      'data' => t('User'),
      'field' => 'user.name',
    ),
    array(
      'data' => t('Application'),
      'field' => 'saml.entityid',
    )
  );

  $query = db_select('saml_entity_access', 'access')->extend('PagerDefault')->extend('TableSort');
  
  $query->join( "users", "user", "user.uid = access.uid" );
  $query->join( "saml_entity", "saml", "saml.id = access.seid" );
  
  $query
    ->fields('access', array( 'access'))
    ->fields('user', array( 'uid' ))
    ->fields('saml', array( 'entityid', 'name' ));
    
  $result = $query
    ->limit(50)
    ->orderByHeader($header)
    ->execute();
    
   

  $build['dblog_table'] = array(
    '#theme' => 'table', 
    '#header' => $header, 
    '#rows' => array(), 
    '#attributes' => array('id' => 'admin-dblog'), 
    '#empty' => t('No log messages available.'),
  ); 
  
  foreach ($result as $entry) {
    $row = array(
      'data' => array(
	format_date($entry->access, 'short'),
        theme( 'username', array( "account" => user_load($entry->uid) ) ),
        $entry->entityid
      ),
    );
    
    $build['dblog_table']['#rows'][] = $row; unset($row);
  }
  
  $build['dblog_pager'] = array('#theme' => 'pager');

  return $build;
}

/**
 * Page callback for path admin/reports/saml/access
 **/
function saml_service_idp_access_download(){
  while(ob_get_level()) {
    ob_end_clean();
  }
    
  $headers = array(
    'Content-Type' => "text/csv",
    'Content-Disposition' => 'attachment; filename="federation_access_log.csv"',
    'Cache-Control' => 'private',
  );
  
  foreach ($headers as $name => $value) {
    drupal_add_http_header($name, $value);
  }
  drupal_send_headers();
  
  
  $query = db_select('saml_entity_access', 'access');
  
  $query->join( "users", "user", "user.uid = access.uid" );
  $query->join( "saml_entity", "saml", "saml.id = access.seid" );
  
  $query
    ->fields('access', array( 'access'))
    ->fields('user', array( 'uid' ))
    ->fields('saml', array( 'entityid', 'name' ));
    
      
  $result = $query
    ->execute();
    
  echo "\"access time\",\"user\",\"user profile\",\"application\"\n";
  flush();
  foreach ($result as $entry) {
    $account = user_load($entry->uid);
    echo "\"";
    echo implode(
      array(
	format_date($entry->access, 'short'),
        $account->name,
        url( "user/{$entry->uid}", array( 'absolute' => true ) ),
        $entry->entityid
      ),
      '","'
    );
    echo "\"\n";
    flush();
  }
  
  
  drupal_exit();
  
}