<?php

class Saml20IdpService extends SamlService {

  public function createFailedResponse( SamlMessageContext &$messageContext, $message = "" ){
    $response = new Saml20CodecResponse();
        
    if(
      ( $keyDescriptor = array_shift( $messageContext->authnRoleDescriptor->getKeyDescriptors( SamlServiceKeyDecriptorFilter::createFilterCallback( SAML20CodecKeyDescriptor::USE_SIGNING ) ) ) )
    ){
      $signature = new XMLDigitalSignature(  );
      $signature->setIncludePubKey( true );
      $signature->setKeyPair( $keyDescriptor->getKeyPair() ); unset($signatureInfo);
      
      $response->setSignature( $signature );
    } unset($signature);
    
    $response->setIssuer( new Saml20CodecNameID() );
    $response->getIssuer()->setValue( $messageContext->authnEntity->getEntityId() );
    
    $status = new Saml20CodecStatus();

    $statusCode = new Saml20CodecStatusCode();
    $statusCode->setValue(SAML20Names::STATUS_FAILURE);
    $status->setStatusCode($statusCode); unset($statusCode);
    
    $status->setStatusMessage( $message );
    
    $response->setStatus($status); unset($status);
    
    return $response;
  }
  
  private function createSuccessfulResponse( SamlMessageContext &$messageContext ){
    $response = new Saml20CodecResponse();
    
    if(
      ( $keyDescriptor = array_shift( $messageContext->authnRoleDescriptor->getKeyDescriptors( SamlServiceKeyDecriptorFilter::createFilterCallback( SAML20CodecKeyDescriptor::USE_SIGNING ) ) ) )
    ){
      $signature = new XMLDigitalSignature(  );
      $signature->setIncludePubKey( true );
      $signature->setKeyPair( $keyDescriptor->getKeyPair() ); unset($signatureInfo);
      
      $response->setSignature( $signature );
    } unset($signature);
    
    $response->setIssuer( new Saml20CodecNameID() );
    $response->getIssuer()->setValue( $messageContext->authnEntity->getEntityId() );
    
    $status = new Saml20CodecStatus();
    
    $statusCode = new Saml20CodecStatusCode();
    $statusCode->setValue(SAML20Names::STATUS_SUCCESS);
    $status->setStatusCode($statusCode); unset($statusCode);
    
    $response->setStatus($status); unset($status);
    
    return $response;
  }
  
  public function processAuthnRequest( SamlMessageContext &$messageContext ){  
    if( !$messageContext->serviceEntity ){
      $messageContext->serviceEntity = $this->getApi()->getEntity( $messageContext->request->getIssuer()->getValue() );
    }
    
    if( !$messageContext->serviceEntity ){
      throw new Exception("This application is not configured to handle SAML assertions.");
    }
    
    if( !$messageContext->serviceDescriptor ){
      $messageContext->serviceDescriptor = $messageContext->serviceEntity->getDescriptor( SamlServiceRoleDecriptorFilter::createFilterCallback("SAML20CodecSPSSODescriptor") );
    }
    
    if( !$messageContext->serviceDescriptor ){
      throw new Exception("This unable to determine the service provider.");
    }
    
    $nameID = null;
    
    if( $messageContext->request->getSubject() && $messageContext->request->getSubject()->getId() ){
      $messageNameID = $messageContext->request->getSubject()->getId();
      if($messageNameID instanceof SAML20CodecEncryptedID){
	try {
	  $keyDescriptors = $messageContext->authnRoleDescriptor->getKeyDescriptors();

	  reset($keyDescriptors);
	  while( list(,$keyDescriptor) = each($keyDescriptors) ){
	    if( $keyDescriptor->getUse() == SAML20CodecKeyDescriptor::USE_ENCRYPTION ){
	      $encKey = new XMLEncryption();
	      $encKey->setKeyPair( $keyDescriptor->getKeyPair() );
	      
	      $domDocument = new DOMDocument("1.0", "UTF-8" );
	      if( $messageContext->getParentContext() ){
		$domDocument->loadXML( $messageContext->getParentContext()->requestXML );
	      }
	      else {
		$domDocument->loadXML( $messageContext->requestXML );
	      }
	      
	      $xpath = new DOMXPath($domDocument);
	      foreach( $xpath->query('namespace::*', $domDocument->documentElement) as $node ) {
		if( $prefix = $domDocument->lookupPrefix($node->nodeValue) ){
		  $encKey->addNamespace( $node->nodeValue, $prefix  );
		}
	      }
	      unset($node); unset($xpath);
	      
	      $encKey->setDefaultNS( $domDocument->documentElement->namespaceURI );
	      
	      $messageNameID->setKey( $encKey );
	      try {
		$nameID = $this->getAPI()->xmlDecode( $messageNameID->getData() );
	      } catch( Exception $e ) {
		//allow for multiple encryption keys in the descriptor
	      }
	      if( !is_null($nameID) ){
		break;
	      }
	    }
	  }
	  
	} catch( Exception $e ) {
	  
	}
      }
      else {
	$nameID = $messageNameID;
      }
      unset($messageNameID);
    }
    
    if( is_null($nameID) ){
      $messageContext->response = $this->createFailedResponse( $messageContext, "Unable to determine identifier for requestor." );
      return;
    }
    
    //verify signature      
    $validated = false;
      
    try {
      if( $signature = $messageContext->request->getSignature() ){
	$keyDescriptors = false;
      
	switch( $messageContext->request->getIssuer()->getValue() ){
	  case $messageContext->authnEntity->getEntityId():
	    $keyDescriptors = $messageContext->authnRoleDescriptor->getKeyDescriptors( SamlServiceKeyDecriptorFilter::createFilterCallback( SAML20CodecKeyDescriptor::USE_SIGNING ) );
	  break;
	  case $messageContext->serviceEntity->getEntityId():
	    $keyDescriptors = $messageContext->serviceDescriptor->getKeyDescriptors( SamlServiceKeyDecriptorFilter::createFilterCallback( SAML20CodecKeyDescriptor::USE_SIGNING ) );
	  break;
	}
	
	if( $keyDescriptors ){

	  reset($keyDescriptors);
	  while( list(,$keyDescriptor) = each($keyDescriptors) ){
	    if( $keyDescriptor->getUse() == SAML20CodecKeyDescriptor::USE_SIGNING ){
	      $signature->validateElement( $keyDescriptor->getKeyPair() );
	      $validated = true;
	    }
	  }
	}
      }
      else {
	if( !$messageContext->serviceEntity->getOption("allowUnsignedAuthnRequests") ){
	  $messageContext->response = $this->createFailedResponse( $messageContext, "Message was not signed" );
	  return;
	}
	$validated = true;
      }

    } catch( Exception $e ){
      watchdog( "saml_service", "Key validation message: !message", array( "!message" => $e->getMessage() ), WATCHDOG_DEBUG );
    }
      
    if( !$validated ){
      $messageContext->response = $this->createFailedResponse( $messageContext, "Unable to validate signature for entity ".$messageContext->serviceEntity->getEntityId() );
      return;
    }
    unset($validated);
    
    $this->verifyRequestIsUnique($messageContext->request);
    
    global $user;
    $assertion = new Saml20CodecAssertion(); 
    $subject = new Saml20CodecSubject();
    
    if( $requestedContext = $messageContext->request->getRequestedContext() ){
      $registry = saml_service_idp_authncontext();
      module_load_include( "inc", "saml_service_idp", "includes/authn" );
      $classReferences = $requestedContext->getClassRef();
      reset($classReferences);
      while( list(,$classReference) = each( $classReferences ) ){
	if( array_key_exists( $classReference, $registry ) && ( $info = $registry[ $classReference ] ) ){
	  
	  try {
	    if( !empty($info['file']) && ( $include_file = $info['file'] ) ){
	      if( !empty($info['path']) ){
		$include_file = "{$info['path']}/{$include_file}";
	      }
	      require_once( $include_file );
	    }

	    if( function_exists( $info['constructor callback'] ) ){
	      $arguments = $info['constructor arguments'];
	      if( !empty( $info['className'] ) ){
		array_unshift( $arguments, $info['className'] );
	      }
	      array_push($arguments, $info);
	      $instance = call_user_func_array( $info['constructor callback'], $arguments );
	      
	      if( $loop_assertion = $instance->validate( $nameID, $messageContext->request->getSubject(), $messageContext->request, $messageContext ) ){
		$assertion = $loop_assertion;
		break;
	      }
	      
	    }
	    
	  } catch( Exception $e ){
	    $messageContext->response = $this->createFailedResponse( $messageContext, $e->getMessage() );
	    return ;
	  }
	}
	
      }
      unset($classReferences);

    } unset($requestedContext);
    
    if( $user->uid == 0 ){
      $messageContext->response = $this->createFailedResponse( $messageContext, "Could not determine user within the requested context(s)." );
      return ;
    }
    
    $messageContext->response = $this->createSuccessfulResponse( $messageContext );
    
    if( $service = $messageContext->serviceDescriptor->getAssertionConsumerService($messageContext->bindingType) ){
      $messageContext->binding = $service;
      $messageContext->response->setDestination( $service->getLocation() );
    }
    
    if( $location = $messageContext->request->getAssertionConsumerServiceURL() ){
      $services = $messageContext->serviceDescriptor->getAssertionConsumerService();
      reset($services);
      while( list( ,$service) = each($services) ){
	if( $service->getLocation() == $location ){
	  $messageContext->binding = $service;
	  $messageContext->response->setDestination( $service->getLocation() );
	  break;
	}
      }
      unset($service); unset($services);
    }
    
    if(!$messageContext->binding){
      throw new Exception("Unable to locate binding.");
    }
    
    $assertion->setIssuer( $messageContext->response->getIssuer() );
    $assertion->setSignature($messageContext->response->getSignature());
    
    $conditions = new Saml20CodecConditions();
    $conditions->setNotOnOrAfter(strtotime( "+5 minutes" ));
    $conditions->setNotBefore(strtotime( "-1 minute" ));
    
    $audienceRestriction = new Saml20CodecAudienceRestriction();
    $audienceRestriction->addAudience( $messageContext->serviceEntity->getEntityId() );
    $audienceRestriction->addAudience( $messageContext->binding->getLocation() );
    $conditions->addCondition($audienceRestriction); unset($audienceRestriction);
    
    $assertion->setConditions( $conditions ); unset($conditions);
    
    $attributeStatement = new Saml20CodecAttributeStatement();
  
    $extensions = $messageContext->request->getExtensions();
    foreach( $extensions as $extension ){
      if( $extension instanceOf SAML20ExtenstionAssertionAttributes ){
	$attributes = $extension->getAttributes();
	foreach( $attributes as $extAttrib ){
	  $attrib = new Saml20CodecAttribute();
	  $attrib->setName( $extAttrib->getName() );
	  $attrib->setNameFormat($extAttrib->getNameFormat());
	  $attrib->setFriendlyName($extAttrib->getFriendlyName());
	  
	  $otherAttribs = $extAttrib->getOtherAttributes();
	  foreach( $otherAttribs as $otherAttrib ){
	    $otherAttribs->putOtherAttribute( $otherAttrib );
	  } unset($otherAttribs);
	  
	  $values = $extAttrib->getValues();
	  foreach( $values as $value ){
	    $attrib->addValue($value);
	  } unset( $values );
	  
	  $attributeStatement->addAttribute( $attrib ); unset($extAttrib);
	}
      }
    }
    
    {//default attributes added to all statements
      $attrib = new Saml20CodecAttribute();
      $attrib->setName( "session_id" );
      $attrib->setNameFormat( SAML20Names::NAMEIDFORMAT_2_0_TRANSIENT );
      $attrib->addValue(session_id());
      $attributeStatement->addAttribute( $attrib ); unset($attrib);
      
      $attrib = new Saml20CodecAttribute();
      $attrib->setName( "username" );
      $attrib->setNameFormat( SAML20Names::NAMEIDFORMAT_2_0_PERSISTENT );
      $attrib->addValue($user->name);
      $attributeStatement->addAttribute( $attrib ); unset($attrib);
      
      if( !empty($user->mail) ){
	$attrib = new Saml20CodecAttribute();
	$attrib->setName( "mail" );
	$attrib->setNameFormat( SAML20Names::NAMEIDFORMAT_1_1_EMAILADDRESS ); 
	$attrib->addValue($user->mail);
	$attributeStatement->addAttribute( $attrib ); unset($attrib);
      }
    }
    
    {//add attributes to completed assertion;
      $modules = module_implements("saml_idp_attributes");
      reset($modules);
      while( list( , $module) = each($modules) ){
	$callback = "{$module}_saml_idp_attributes";
	if( function_exists($callback) ){
	  if( $attributes = $callback( $user ) ){
	    reset( $attributes );
	    while( list( , $attrib) = each($attributes) ){
	      $attributeStatement->addAttribute( $attrib ); 
	    } unset($attrib);
	  } unset( $attributes );
	}
      }
    }
    
    if( module_exists("rules") && function_exists("rules_invoke_event_by_args") ){
      rules_invoke_event_by_args('attributeStatement_prepare', array('attributeStatement' => $attributeStatement, 'audience' => $messageContext->serviceEntity->getEntityId(), 'user' => $GLOBALS['user']));
    }
    
    $valid_nameFormatIDs = $messageContext->serviceDescriptor->getNameIDFormat();
    $nameFormatId_unspecified = false;

    if( is_array($valid_nameFormatIDs) ){
       $nameFormatId_unspecified = in_array("urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified", $valid_nameFormatIDs);
       if($nameFormatId_unspecified){
	  $valid_nameFormatIDs = array_filter( $valid_nameFormatIDs, create_function( '$a', 'return $a != "urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified";') );
       }
    }
    
    if( $valid_nameFormatIDs ){
      $nameID = new Saml20CodecNameID();
      
      $attributes = $attributeStatement->getAttributes();
      reset($attributes); 
      while( list(, $attrib) = each($attributes) ){
	if( count( $attrib->getValues() ) == 1 ){
	  if( in_array( $attrib->getNameFormat(), $valid_nameFormatIDs)  ){
	    $nameID->setFormat($attrib->getNameFormat());
	    $nameID->setValue( array_shift( array_slice( $attrib->getValues(), 0, 1) ) );
	    break;
	  }
	}
      }
      
      if( !$nameID->getValue() ){
	$messageContext->response = $this->createFailedResponse( $messageContext, "No subject with using the valid NameFormatId(s): ".implode( $valid_nameFormatIDs, ", ")."." );
	return ;
      }
    }
    
    if( !$nameID->getValue() && $nameFormatId_unspecified ){

      $nameFormatId = $nameID->getFormat();
      $nameID = new Saml20CodecNameID();
    
      switch( $nameFormatId ){
	case SAML20Names::NAMEIDFORMAT_2_0_PERSISTENT:
	  $nameID->setFormat( SAML20Names::NAMEIDFORMAT_2_0_PERSISTENT ); 
	  $nameID->setValue( $user->name );
	break;
	case SAML20Names::NAMEIDFORMAT_1_1_EMAILADDRESS:
	  $nameID->setFormat( SAML20Names::NAMEIDFORMAT_1_1_EMAILADDRESS ); 
	  $nameID->setValue( $user->mail );
	break;
	default:
	  $nameID->setFormat( SAML20Names::NAMEIDFORMAT_2_0_TRANSIENT ); 
	  $nameID->setValue( session_id() );
      }
      unset( $nameFormatId );
    }
    
    if( !$nameID->getValue() ){
      $messageContext->response = $this->createFailedResponse( $messageContext, "Could not determine the subject for the response." );
      return ;
    }
    
    $subject = new Saml20CodecSubject();
  
    $subject->setId( $nameID );
    $confirmation = new Saml20CodecSubjectConfirmation();
    $confirmation->setMethod( SAML20Names::METHOD_2_0_BEARER );
    $data = new Saml20CodecSubjectConfirmationData();
    $data->setNotOnOrAfter(strtotime( "+5 minutes" ));
    $data->setNotBefore(strtotime( "-1 minute" ));
    if( $messageContext->binding ){
      $data->setRecipient( $messageContext->binding->getLocation() );
    }
    $confirmation->addSubjectConfirmationData($data); unset($data);
    $subject->addSubjectConfirmation($confirmation); unset($confirmation);
    
    $assertion->setSubject( $subject ); unset($subject);
    
    $assertion->addStatement( $attributeStatement ); unset($attributeStatement);
    
    $hasAuthn = array();
    $statements = $assertion->getStatements();
    array_walk( $statements, create_function('$value, $key, $data','if( $value instanceof Saml20CodecAuthnStatement) { $data["hasData"]= true; }'), array( 'hasData' => &$hasAuthn ) );
    
    if( !$hasAuthn ){
      $authnStatement = new Saml20CodecAuthnStatement();
      $authnContext = new Saml20CodecAuthnContext();
      $authnContext->setAuthnContextClassRef(SAML20Names::AUTHNCONTEXT_CLASS_UNSPECIFIED);
      
      $authnStatement->setAuthnContext($authnContext); unset($authnContext);
      
      $authnStatement->setSessionIndex( session_id() );
      
      $assertion->addStatement( $authnStatement ); unset($authnStatement);
    }
    
    $messageContext->response->addAssertion($assertion);
    
  }
  
  public function handleSingleSignOn( SamlMessageContext &$messageContext ){
    $authnContext = $messageContext;
    if(
      $messageContext->bindingType == SAML20Names::BINDINGS_SOAP ||
      $messageContext->request instanceof SOAPMessage
    ){
      $authnContext = $messageContext->createChildContext();
      
      $authnContext->request = $messageContext->request->getBody();
    }
  
    $this->processAuthnRequest($authnContext);
    
    if( $authnContext->response ){
      if( $messageContext->bindingType == SAML20Names::BINDINGS_SOAP ){
	$SOAPResponse = new SOAPMessage();
	$SOAPResponse->setBody( $authnContext->response ); unset($authnContext);

	$messageContext->response = $SOAPResponse;
      }
      unset($authnContext);
      $modules = module_implements("saml_idp_build_response");
      reset($modules);
      while( list( , $module) = each($modules) ){
	$callback = "{$module}_saml_idp_response";
	if( function_exists($callback) ){
	  $callback( $messageContext->response );
	}
      }
    }
    
  }
  
  public function handleLogout( $idpMetadata ){
    $messageContext = new SamlMessageContext();
    switch( $_SERVER['REQUEST_METHOD']  ){
      case "GET":
	$messageContext->binding = $idpMetadata->getSingleLogoutService(SAML20Names::BINDINGS_HTTP_REDIRECT);

	$samlMessage = base64_decode($_GET['SAMLRequest']);
	if( isset( $_GET['SAMLEncoding']) ){
	  $messageContext->encoding = $_GET['SAMLEncoding'];
	}
	if( isset( $_GET['RelayState']) ){
	  $messageContext->relayState = $_GET['RelayState'];
	}
	
	$messageContext->requestXML = base64_decode($_GET['SAMLRequest']);
      break;
    }
    
    if( $messageContext->binding === false ){
      throw new Exception("Could not determine correct binding");
    }
    
    if( $messageContext->requestXML === false ){
      throw new Exception("Could not understand SAML message");
    }

    $messageContext->request = Saml::fromXML( $messageContext->requestXML );
    
    if( $messageContext->request instanceof Saml20LogoutRequest ){      
      $signature = new XmlDSignature();
      $signature->setCertificate($idpMetadata->getCertificate());
      
      $logout = new Saml20LogoutResponse();
      
      $logout->setIssuer( new Saml20CodecNameID() );
      $logout->getIssuer()->setValue( $idpMetadata->getEntityId() );
      
      $status = new Saml20Status();
      
      $statusCode = new Saml20StatusCode();
      $statusCode->setValue("urn:oasis:names:tc:SAML:2.0:status:Success");
      $status->setStatusCode($statusCode); unset($statusCode);
      
      $logout->setStatus($status); unset($status);
      
      $logout->setSignature($signature); unset($signature);
      
      $logout->setInResponseTo( $messageContext->request->getID() );
      
      if( empty( $messageContext->relayState ) ){
	$messageContext->relayState = "<front>";
	$destination = $messageContext->request->getDestination();
	if( !empty($destination) ){
	  $messageContext->relayState = $destination;
	} unset($destination);
      }
      
      if( !strstr( $messageContext->relayState, "http" ) ){
	$messageContext->relayState = "<front>";
      }
      
      return $messageContext;
    }
    
    throw new Exception("Request is not recognized");
  }
}


class SAMLServiceIDPLogger implements SAMLServiceLogger {

  public function logEvent( SAMLServiceLogEvent $event ){
    if( SAMLServiceLogging::hasFlag(SAML_SERVICE_IDP_LOG_ENTITY_ACCESS) && $event->hasFlag(SAML_SERVICE_IDP_LOG_ENTITY_ACCESS) ){
      $record = (object)array(
	'seid'   => $event->getEntity()->getId(),
	'uid'    => $event->getAccount()->uid,
	'access' => time()
      );
      drupal_write_record("saml_entity_access", $record);
      unset($record);
    }
  }
  
}