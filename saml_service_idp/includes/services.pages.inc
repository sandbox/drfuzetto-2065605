<?php


function saml_service_idp_v2_sss(){
  $messageContext = new SamlMessageContext();
  $messageContext->initialize();
    
  if( is_null( $messageContext->bindingType ) && !user_is_logged_in() ){
    return array( 
      "#type" => 'page',
      drupal_render_page(  drupal_get_form('user_login') )
    );
  }
  else if( isset($_GET['entityId']) ){
    $messageContext->bindingType = SAML20Names::BINDINGS_HTTP_REDIRECT;
  }
  
  $messageContext->authnEntity = SAML::getInstance()->getEntityById(1);
  if( !( $messageContext->authnRoleDescriptor = $messageContext->authnEntity->getDescriptor( SamlServiceRoleDecriptorFilter::createFilterCallback("SAML20CodecIDPSSODescriptor") ) ) ){
    throw new Exception("This application is not configured to handle SAML assertions, unable to find an IDPSSODescriptor.");
  }
   
  if( $messageContext->request ){
    global $user;
    if( $user->uid > 0 ){
      watchdog('user', 'Session closed for %name.', array('%name' => $user->name));

      module_invoke_all('user_logout', $user);
      
    }

    // Destroy the current session data, and reset $user to the anonymous user.
    drupal_session_regenerate();
    
  }
  else {
  
    if(
      in_array( $messageContext->bindingType, array( SAML20Names::BINDINGS_HTTP_REDIRECT, SAML20Names::BINDINGS_HTTP_POST ) )&&
      isset($_GET['entityId']) && !empty($_GET['entityId']) &&
      ( $serviceEntity = SAML::getInstance()->getEntity($_GET['entityId']) ) &&
      ( $serviceDescriptor = $serviceEntity->getDescriptor( SamlServiceRoleDecriptorFilter::createFilterCallback("SAML20CodecSPSSODescriptor") ) )
    ){
      $messageContext->serviceEntity = $serviceEntity; unset($serviceEntity);
      $messageContext->serviceDescriptor = $serviceDescriptor; unset($serviceDescriptor);
      
      if( $avail_service = $messageContext->serviceDescriptor->getAssertionConsumerService( array( SAML20Names::BINDINGS_HTTP_REDIRECT, SAML20Names::BINDINGS_HTTP_POST ) ) ){
	if( is_array( $avail_service ) ) $avail_service = array_shift($avail_service);
	$messageContext->bindingType = $avail_service->getBinding();
	$messageContext->binding = $avail_service;
      } unset($avail_service);
      
      $request = new SAML20CodecAuthnRequest();

      $issuer = new Saml20CodecNameID();
      $issuer->setValue( $messageContext->authnEntity->getEntityId() );
      $request->setIssuer( $issuer );
      
      if(
	( $keyDescriptor = array_shift( $messageContext->authnRoleDescriptor->getKeyDescriptors( SamlServiceKeyDecriptorFilter::createFilterCallback( SAML20CodecKeyDescriptor::USE_SIGNING ) ) ) )
      ){
	$signature = new XMLDigitalSignature(  );
	$signature->setIncludePubKey( true );
	$signature->setKeyPair( $keyDescriptor->getKeyPair() ); unset($signatureInfo);
	
	$request->setSignature( $signature ); unset($signature);
      }
      
      $subject = new Saml20CodecSubject();

      {//set requestors name 
	$nameID = new Saml20CodecNameID();
	$nameID->setValue( session_id() );
	$nameID->setFormat( Saml20NameIdFormatURN::TRANSIENT );
	$subject->setId($nameID); unset($nameID);
	
	$nameID = new Saml20CodecNameID();
	$request->setSubject( $subject ); unset( $subject );
      }
      
      {//set audience condition
	$conditions = new Saml20CodecConditions();
	$conditions->setNotOnOrAfter(strtotime( "+5 minutes" ));
	$conditions->setNotBefore(time());
	
	$audienceRestriction = new Saml20CodecAudienceRestriction();
	$audienceRestriction->addAudience( $messageContext->serviceEntity->getEntityId() );
	$conditions->addCondition($audienceRestriction); unset($audienceRestriction);
	
	$request->setConditions( $conditions ); unset($conditions);
      }
      
      {//set requestedauthncontext
	$requestedContext = new SAML20CodecRequestedAuthnContext();
	$requestedContext->addClassRef( SAML20Names::AUTHNCONTEXT_CLASS_PREVIOUS_SESSION );
	$request->setRequestedContext($requestedContext);
      }
      $messageContext->request = $request; unset( $request );
      
    }
  }
    
  if( $messageContext->request ){
    Saml::getInstance()->getService("saml2/idp")->handleSingleSignOn( $messageContext );
  }
  else {
    throw new Exception("Request Not Understood");
  }
  
  SAMLServiceLogging::getLogger()->logEvent( new SAMLServiceLogEvent(SAML_SERVICE_LOG_MESSAGE_CONTEXT | SAML_SERVICE_IDP_LOG_ENTITY_ACCESS, $messageContext->serviceEntity, $GLOBALS['user'], array( 'messageContext' => $messageContext )) );
  
  if(
    $messageContext->bindingType == SAML20Names::BINDINGS_SOAP ||
    ( isset($_SERVER['HTTP_ACCEPTS']) && $_SERVER['HTTP_ACCEPTS'] == "application/xml" )
  ){
    return array(
      '#markup' => $messageContext->responseXML
    );
  }
  
  return array(
    '#type' => 'page',
    'content' => saml_service_idp_saml2_assertion_post_page( $messageContext )
  );

}

function saml_service_idp_saml2_assertion_post_page( SamlMessageContext $messageContext ) {
  
  if( $messageContext->response->getStatus()->getStatusCode()->getValue() == SAML20Names::STATUS_FAILURE ){
    drupal_set_message( t("An error occured while processing your request the error is %message. Please report your error to the system adminitrator.", array( "%message" => $messageContext->response->getStatus()->getStatusMessage() ) ), 'error' );
    return array(
      '#markup' => theme('status_messages', array( 'display' => 'error' ) )
    );
  }
  
  $message = base64_encode( $messageContext->responseXML );
  
  switch( $messageContext->bindingType ){
    case SAML20Names::BINDINGS_HTTP_REDIRECT:
      if( strlen( $message  ) < 255 ){
	drupal_goto( $messageContext->binding->getLocation(), array( 'query' => array( 'SAMLRequest' => $message, "RelayState" => $messageContext->relayState ) )  );
      }
    break;
  }

  $acs = $messageContext->binding->getLocation();
  
  $relayState = $messageContext->relayState;
  if( empty($relayState) ){
    $relayState = $messageContext->binding->getResponseLocation();
  }
  
  $relayState = htmlspecialchars($relayState);
  $markup = <<<EO_HTML
<form method="post" action="{$acs}" id="saml20">
<input type="hidden" name="SAMLResponse" value="{$message}" />
<input type="hidden" name="RelayState" value="{$relayState}">
<input type="submit" value="Continue" id="action-button" />
<div class="message" style="display:none">
<h2>Processing login please wait.</h2>
</div>
</form>
EO_HTML;
  return array(
    array(
      '#markup' => $markup,
      '#attached' => array(
        'js' => array(drupal_get_path('module', 'saml_service_idp') . '/misc/saml_post.js'),
      )
    )
  );

}
