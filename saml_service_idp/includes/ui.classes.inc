<?php

class SamlServiceIDP_UI_Help {
  public static function hook( $path, $arg ){
    switch( $path ){
      case 'admin/config/development/saml/descriptor':
	return t("Use this interface to build a unique descriptor for a Service Provider.");
    }
  }
}