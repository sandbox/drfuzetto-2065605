<?php 

/**
 * Implements hook_schema
 */
function saml_service_idp_schema() {
  $schema = array();
  
  $schema['saml_service_entity_roles'] = array(
    'fields' => array(
      'id'   => array(
	'type' => 'serial'
      ),
      'rid'   => array(
	'type'     => 'int',
	'unsigned' => true,
	'length'   => 11,
	'not null' => true
      ),
      'entityid' => array(
	'type'     => 'int',
	'unsigned' => true,
	'length'   => 11,
	'not null' => true
      )
    ),
    'primary key' => array(
      'id',
    ),
    'unique keys' => array(
      'record'     => array( 'rid', 'entityid' )
    )
  );
  
  $schema['saml_entity_access'] = array(
    'fields' => array(
      'id'   => array(
	'type' => 'serial'
      ),
      'seid' => array(
	'type'     => 'int',
	'length'   => 10,
	'unsigned' => true
      ),
      'uid' => array(
	'type'     => 'int',
	'length'   => 10,
	'unsigned' => true
      ),
      'access' => array(
	'type'     => 'int',
	'length'   => 10,
	'unsigned' => true
      ),
    ),
    'primary key' => array(
      'id'
    ),
    'keys' => array(
      'uid'      => array( 'uid' ),
      'seid'     => array( 'seid' ),
      'seid_uid' => array( 'seid', "uid" )
    )
  );
  
  return $schema;
}

/**
 * Implements hook_enable
 */
function saml_service_idp_enable() {
  $api = Saml::getInstance();
  if( !$api->getService("saml2/idp") ){
    saml_service_idp_saml_service_api_init( $api, $api->getVersion() );
  }
  
  if(
    ($entity = saml_service_application_entity() ) &&
    !$entity->getDescriptor( SamlServiceRoleDecriptorFilter::createFilterCallback("SAML20CodecIDPSSODescriptor") )
  ){    
    $entityDescriptor = $entity->getDescriptor();
    
    $ssoDescriptor = new SAML20CodecIDPSSODescriptor( "urn:oasis:names:tc:SAML:2.0:protocol" );
    
    $keyDescriptor = new SAMLServiceKeyDescriptor();
    $keyDescriptor->setKeyPair( SAMLSignatureUtils::generateKeyPair(array( 'publicKey' => true )) );
    $keyDescriptor->setUse( SAMLServiceKeyDescriptor::USE_ENCRYPTION );
    $ssoDescriptor->addKeyDescriptor( $keyDescriptor );
    
    $keyDescriptor = new SAMLServiceKeyDescriptor();
    $keyDescriptor->setKeyPair( SAMLSignatureUtils::generateKeyPair(array( 'publicKey' => true )) );
    $keyDescriptor->setUse( SAMLServiceKeyDescriptor::USE_SIGNING );
    $ssoDescriptor->addKeyDescriptor( $keyDescriptor );
    
    unset($sig_array); unset($keyDescriptor);
    
    $entityDescriptor->addDescriptor( $ssoDescriptor );
    
    unset($sig_array);
    
    $entity->setDescriptor($entityDescriptor);
    SAML::getInstance()->saveEntity( $entity );
  }
  unset($api);
}

/**
 * Implements hook_enable
 */
function saml_service_idp_disable() {
  if(
    ($entity = saml_service_application_entity() ) &&
    $entity->getDescriptor( SamlServiceRoleDecriptorFilter::createFilterCallback("SAML20CodecIDPSSODescriptor") )
  ){
    $entityDescriptor = $entity->getDescriptor();
    $descriptors = $entity->getDescriptors();
    $entity->clearDescriptors();
    reset($descriptors);
    while( list(,$descriptor) = each($descriptors) ){
      if( !($descriptor instanceof SAML20CodecIDPSSODescriptor) ){
	$entityDescriptor->addDescriptor($descriptor);
      }
    }
    $entity->setDescriptor($entityDescriptor);
    SAML::getInstance()->saveEntity( $entity );
  }
}

/**
 * Upgrades existing entities to use the new SAMLEntity class and underlying
 * storage mechanism.
 */
function saml_service_idp_update_7200(&$sandbox){

  if( intval(drupal_get_installed_schema_version("saml_service")) < 7200 ){
    throw new DrupalUpdateException('Invalid saml_service schema version please update saml service first.');
  }

  //ensure the codecs module dependency is met before processing
  drupal_load("module", "codecs");
  registry_rebuild();
  drupal_get_schema(NULL, true);//refresh the schema before starting
  module_load_include( "lib.inc", "saml_service", "lib/7200/main" );

  $select = db_select( "saml_service_metadata", "saml_legacy" )
    ->fields("saml_legacy");
  $resultset = $select->execute();
  
  while( $record = $resultset->fetchAssoc() ){

    if( $data = unserialize( $record['data'] ) ){
      if( $record['name'] == "saml_service_idp:self" ){
	$record['id'] = 1;
      }
    
      $entity = new SamlEntity( $record['id'], $record['entityid'], $record['name'] );
      watchdog('saml_service', 'entity %entity', array('%entity' => var_export($entity, true) ), WATCHDOG_NOTICE, $link = NULL);
      watchdog('saml_service', 'record %record', array('%record' => var_export($record, true) ), WATCHDOG_NOTICE, $link = NULL);
      
      if( $record['cls'] == "SamlServiceIdpMetaData" ){
	$entityDescriptor = new SAML20CodecEntityDescriptor($record['entityid']);
      
	$ssoDescriptor = new SAML20CodecIDPSSODescriptor( "urn:oasis:names:tc:SAML:2.0:protocol" );
	    
	$sig_array = array(
	    'privateKeyPassphrase' => $data['setCertificate']->getPassphrase(),
	    'privateKey'           => $data['setCertificate']->getPrivate(),
	    'publicKey'            => $data['setCertificate']->getPublic(),
	);
	
	$keyDescriptor = new SAMLServiceKeyDescriptor();
	$keyDescriptor->setKeyPair( new StringXMLKeyPair( $sig_array ) );
	$keyDescriptor->setUse( SAMLServiceKeyDescriptor::USE_ENCRYPTION );
	$ssoDescriptor->addKeyDescriptor( $keyDescriptor );
	
	$keyDescriptor = new SAMLServiceKeyDescriptor();
	$keyDescriptor->setKeyPair( new StringXMLKeyPair( $sig_array ) );
	$keyDescriptor->setUse( SAMLServiceKeyDescriptor::USE_SIGNING );
	$ssoDescriptor->addKeyDescriptor( $keyDescriptor );
	
	unset($sig_array); unset($keyDescriptor);
	
	$entityDescriptor->addDescriptor( $ssoDescriptor );
	
	unset($sig_array);
	
	if( $service_list = $data['EndPoint']['SingleLogoutService'] ){
	  reset($service_list);
	  while( list(, $service) = each($service_list) ){
	    $entity_service = new SAML20CodecEndpoint();
	    $entity_service->setLocation( $service->getLocation() );
	    $entity_service->setResponseLocation( $service->getResponseLocation() );
	    $entity_service->setBinding( $service->getBinding() );
	    $ssoDescriptor->addSingleLogoutService($entity_service); unset($entity_service);
	  }
	}
	
	if( $service_list = $data['EndPoint']['SingleSignOnService'] ){
	  reset($service_list);
	  while( list(, $service) = each($service_list) ){
	    $entity_service = new SAML20CodecEndpoint();
	    $entity_service->setLocation( $service->getLocation() );
	    $entity_service->setResponseLocation( $service->getResponseLocation() );
	    $entity_service->setBinding( $service->getBinding() );
	    $ssoDescriptor->addSingleSignOnService($entity_service); unset($entity_service);
	  }
	}
      }
      
      if( $record['cls'] == "SamlSpMetaData" ){
      
	$entityDescriptor = new SAML20CodecEntityDescriptor($record['entityid']);
      
	$metadataSSODescriptor = new SAML20CodecSPSSODescriptor( "urn:oasis:names:tc:SAML:2.0:protocol" );
	    
	$sig_array = array(
	    'publicKey'            => $data['setCertificate']->getPublic(),
	);
	
	$keyDescriptor = new SAMLServiceKeyDescriptor();
	$keyDescriptor->setKeyPair( new StringXMLKeyPair( $sig_array ) );
	$keyDescriptor->setUse( SAMLServiceKeyDescriptor::USE_ENCRYPTION );
	$metadataSSODescriptor->addKeyDescriptor( $keyDescriptor );
	
	$keyDescriptor = new SAMLServiceKeyDescriptor();
	$keyDescriptor->setKeyPair( new StringXMLKeyPair( $sig_array ) );
	$keyDescriptor->setUse( SAMLServiceKeyDescriptor::USE_SIGNING );
	$metadataSSODescriptor->addKeyDescriptor( $keyDescriptor );
	
	unset($sig_array); unset($keyDescriptor);
	
	$metadataSSODescriptor->addNameIDFormat( $data['setNameIDFormat'] );
	
	$entityDescriptor->addDescriptor( $metadataSSODescriptor );
	
	unset($sig_array);
	
	if( $service_list = $data['EndPoint']['SingleLogoutService'] ){
	  reset($service_list);
	  while( list(, $service) = each($service_list) ){
	    $entity_service = new SAML20CodecEndpoint();
	    $entity_service->setLocation( $service->getLocation() );
	    $entity_service->setResponseLocation( $service->getResponseLocation() );
	    $entity_service->setBinding( $service->getBinding() );
	    $metadataSSODescriptor->addSingleLogoutService($entity_service); unset($entity_service);
	  }
	}
	
	if( $service_list = $data['EndPoint']['AssertionConsumerService'] ){
	  reset($service_list);
	  while( list(, $service) = each($service_list) ){
	    $entity_service = new SAML20CodecIndexedEndpoint();
	    $entity_service->setIndex( $service->getIndex() );
	    $entity_service->setDefault( $service->isDefault() );
	    $entity_service->setLocation( $service->getLocation() );
	    $entity_service->setResponseLocation( $service->getResponseLocation() );
	    $entity_service->setBinding( $service->getBinding() );
	    $metadataSSODescriptor->addAssertionConsumerService($entity_service); unset($entity_service);
	  }
	}
      
      }
      
      if( $entityDescriptor ){
	$entity->setDescriptor($entityDescriptor);
	SAML::getInstance()->saveEntity( $entity );
      }
    }
    
  }
}


/**
 * Upgrades existing entities to use the new SAMLEntity class and underlying
 * storage mechanism.
 */
function saml_service_idp_update_7224(&$sandbox){
  db_create_table(
    'saml_entity_access',
    array(
      'fields' => array(
	'id'   => array(
	  'type' => 'serial'
	),
	'seid' => array(
	  'type'     => 'int',
	  'length'   => 10,
	  'unsigned' => true
	),
	'uid' => array(
	  'type'     => 'int',
	  'length'   => 10,
	  'unsigned' => true
	),
	'access' => array(
	  'type'     => 'int',
	  'length'   => 10,
	  'unsigned' => true
	),
      ),
      'primary key' => array(
	'id'
      ),
      'keys' => array(
	'uid'      => array( 'uid' ),
	'seid'     => array( 'seid' ),
	'seid_uid' => array( 'seid', "uid" )
      )
    )
  );
}