<?php

/**
 * Defines a constant to use for the logging level.
 */
define( "SAML_SERVICE_IDP_LOG_ENTITY_ACCESS", 2 );

/**
 * Implements hook_menu()
 */
function saml_service_idp_menu(){
  $items = array();
  
  $items['saml2/service/idp/singleSignonService'] = array(
    'page callback'     => 'saml_service_idp_v2_sss',
    'delivery callback' => 'saml_service_deliver_page',
    'access callback'   => true,
    'type'              => MENU_CALLBACK,
    'file'              => "includes/services.pages.inc",
  );
  
  $items['admin/reports/saml/messages'] = array(
    'title'             => 'Messages',
    'type'              => MENU_DEFAULT_LOCAL_TASK
  );
  
  $items['admin/reports/saml/access'] = array(
    'title'             => 'Access Logs',
    'description'       => 'Review access logs for the entire federation.',
    'page callback'     => 'saml_service_idp_access_overview',
    'access arguments'  => array( 'administer saml_service' ),
    'file'              => "includes/reports.pages.inc",
    'type'              => MENU_LOCAL_TASK,
    'weight'            => 1
  );
  
  $items['admin/reports/saml/access/download'] = array(
    'page callback'     => 'saml_service_idp_access_download',
    'access arguments'  => array( 'administer saml_service' ),
    'file'              => "includes/reports.pages.inc",
    'type'              => MENU_CALLBACK,
  );
  
    $items['admin/config/development/saml/descriptor'] = array(
    'title'             => 'Build Descriptor',
    'page callback'     => 'saml_service_idp_development_descriptor_page',
    'access arguments'  => array( 'administer saml_service' ),
    'file'              => "includes/development.pages.inc",
    'type'              => MENU_LOCAL_TASK
  );
  
  return $items;
}

/**
 * Implements hook_menu_alter()
 */
function saml_service_idp_menu_alter( &$items ){
  $items['admin/reports/saml']['title'] = "SAML Federation";
  $items['admin/reports/saml']['description'] = "Review the logs retained for this application's federation.";
}


/**
 * Implements hook_form_FORM_ID_alter()
 */
function saml_service_idp_form_user_login_alter( &$form, &$form_state, $form_id ){
  if( !isset($form_state['storage']['saml_service_idp']) ){
    $form_state['storage']['saml_service_idp'] = array(
      'request'    => null,
      'entityId'   => null,
      'relayState' => null
    );
  }
      
  if( isset($_REQUEST['SAMLRequest']) ){
    $form_state['storage']['saml_service_idp']['request'] = $_REQUEST['SAMLRequest'];
  }
  
  if( isset($_REQUEST['entityId']) ){
    $form_state['storage']['saml_service_idp']['entityId'] = $_REQUEST['entityId'];
  }
  
  if( isset($_REQUEST['RelayState']) ){
    $form_state['storage']['saml_service_idp']['relayState'] = $_REQUEST['RelayState'];
  }

  $form['#submit'][] = 'saml_serivce_idp_user_login_submit';
}

/**
 * Take action on the user_login form submit if the form has a saml_service_idp state item
 */
function saml_serivce_idp_user_login_submit( $form, &$form_state ){
  if( user_is_logged_in() ){
    if( isset($form_state['storage']['saml_service_idp']) ){
      $query = array();
      if( !is_null($form_state['storage']['saml_service_idp']['entityId']) ){
	$query['entityId'] = $form_state['storage']['saml_service_idp']['entityId'];
      }
      if( !is_null($form_state['storage']['saml_service_idp']['relayState']) ){
	$query['RelayState'] = $form_state['storage']['saml_service_idp']['relayState'];
      }
      if( !is_null($form_state['storage']['saml_service_idp']['request']) ){
	$query['SAMLRequest'] = $form_state['storage']['saml_service_idp']['request'];
      }

      if( isset($query['entityId'] ) ){ $form_state['redirect'] = array( 'saml2/service/idp/singleSignOnService', array( 'query' => $query ) ); unset($query);
      unset($form_state['storage']); }
    }
  }
}


/**
 * Implements saml_service_api_init()
 */
function saml_service_idp_saml_init( &$api ){
  $api->registerService("saml2/idp", new Saml20IdpService($api));
}

/**
 * Page callback override for user/logout
 */
function saml_service_idp_saml2_logout(){
  if( user_is_logged_in() ){
    $idpMetaData = saml_service_idp_self_metadata();
    
    if( $messageContext = Saml::getInstance()->getService("saml2/idp")->handleLogout( $idpMetaData ) ){
    
      switch( $messageContext->binding->getBinding() ){
	case SamlConst::BINDING_2_0_HTTP_REDIRECT:
	  
	break;
	default:
	  $messageContext->relayState = "<front>";
      }
      drupal_goto( "user/logout", array( "query" => array( "destination" => $messageContext->relayState ) ) );
    }
  }
  drupal_goto( "<front>" );
}

/**
 * Implements saml_metadata_update
 */
function saml_service_idp_saml_entity_update( &$entity, $api ){
  if(
    saml_service_idp_use_access_control() &&
    $entity->getId() != 1 &&
    !db_select( "saml_service_entity_roles", "acl" )->fields("acl", array("id") )->condition( "acl.entityid", $entity->getId() )->countQuery()->execute()->fetchColumn()
  ){
    $ace = (object) array(
      'rid' => DRUPAL_AUTHENTICATED_RID,
      'entityid' => $entity->getId()
    );
    drupal_write_record( "saml_service_entity_roles", $ace );
  }
}


/**
 * Implements hook_saml_role_info()
 */
function saml_service_idp_saml_role_info(){
  return array(
    "SAML20CodecSPSSODescriptor" => array(
      'name'       => 'Service Provider',
      'descriptor' => array(
	'namespace' => "urn:oasis:names:tc:SAML:2.0:metadata",
	'localName' => "SPSSODescriptor",
	'class'     => "SAML20CodecSPSSODescriptor"
      ),
      'tasks' => array(
	'access' => array(
	  'title'              => 'access',
	  'callback arguments' => array( 'saml_service_idp_admin_ssodescriptor_access_form' ),
	  'path'               => 'includes',
	  'file'               => 'admin.pages.inc'
	),
      )
    ),
    "SAML20CodecIDPSSODescriptor" => array(
      'name'       => 'Identify Provider',
      'descriptor' => array(
	'namespace' => "urn:oasis:names:tc:SAML:2.0:metadata",
	'localName' => "IDPSSODescriptor",
	'class'     => "SAML20CodecIDPSSODescriptor"
      ),
      'tasks' => array(
	'configure' => array(
	  'title'              => 'Configure',
	  'callback arguments' => array( 'saml_service_idp_admin_idpssodescriptor_config_form' ),
	  'path'               => 'includes',
	  'file'               => 'admin.pages.inc'
	),
	'keys' => array(
	  'title'              => 'Keys',
	  'callback'           => 'saml_service_role_task_keys',
	  'access'             => 'saml_service_is_application_entity',
	  'path'               => drupal_get_path('module','saml_service').'/includes',
	  'file'               => 'keys.task.inc'
	),
      )
    ),
  );
}

function saml_service_idp_use_access_control(){
  return variable_get( 'saml_serivce_idp:access_control', false );
}

/**
 * Add access control checkbox for configuration form
 */
function saml_service_idp_form_saml_service_admin_entity_form_alter( &$form, &$form_state, $form_id ){
  $form['entityid']['#weight'] = -5;
  $form['name']['#weight'] = -3;
  
  if( $form_state['entity']->getId() == 1 ){
    $form['name']['#access'] = false;
    
    $form['access_control'] = array(
      '#title'         => t('Access Control'),
      '#type'          => 'checkbox',
      '#description'   => t("When check this application uses role based access for all the applications in the federation."),
      '#weight'        => -1,
      '#default_value' => saml_service_idp_use_access_control()
    );

    $form['save']['#submit'][] = "saml_service_idp_access_control_form_submit";
  }
  else {
    $form['entityid']['#description'] = l( t("Launch Application"), "saml2/service/idp/singleSignonService", array( 'query' => array( 'entityId' => $form_state['entity']->getEntityId() ) ));
    
    $form['allowUnsignedAuthnRequests'] = array(
      '#title'         => t('Allow Unsigned AuthnRequests'),
      '#type'          => 'checkbox',
      '#default_value' => $form_state['entity']->getOption('allowUnsignedAuthnRequests'),
      '#description'   => t("Allow AuthnRequests to be processed from this service provider when no signature is present." ),
      '#weight'        => -1,
    );
    
    array_unshift( $form['save']['#submit'], "saml_service_idp_unsigned_form_submit" );
  }
}

function saml_service_idp_access_control_form_submit( $form, &$form_state ){
  variable_set( 'saml_serivce_idp:access_control',( isset( $form_state['values']['access_control'] ) && $form_state['values']['access_control']  ) );
}

function saml_service_idp_unsigned_form_submit( $form, &$form_state ){

  $form_state['entity']->putOption( 'allowUnsignedAuthnRequests',  isset($form_state['values']['allowUnsignedAuthnRequests']) && $form_state['values']['allowUnsignedAuthnRequests'] );
}


function saml_service_idp_authncontext( $name = null, $reset = false ){
  static $drupal_static_fast;
  if (!isset($drupal_static_fast)) {
    $drupal_static_fast['classReferences'] = &drupal_static(__FUNCTION__);
  }
  $classReferences = &$drupal_static_fast['classReferences'];


  if ( is_null($classReferences) || $reset ) {
    $classReferences = array();
    $cid = "saml_service_idp:authncontext:classReferences";
    if( !$reset && ($cache = cache_get($cid, "cache_bootstrap") ) ){
      return $cache->data;
    }
    else {
      $modules = module_implements("saml_authncontext_classReferences");
      foreach( $modules as $module ){
	$callback = "{$module}_saml_authncontext_classReferences";
	if( function_exists($callback) ){
	  if( $module_classReferences = $callback($classReferences) ){
	    reset($module_classReferences);
	    while( list( $index, $classReference ) = each($module_classReferences) ){
	      $classReferences[$index] = $classReference + array(
		'constructor callback'  => 'saml_service_idp_saml_authncontext_class_constructor',
		'constructor arguments' => array(),
		'clasName'              => '',
		'module'                => $module,
		'file'                  => false,
		'path'                  => drupal_get_path( 'module', $module )
	      );
	    }
	  }
	}
      }
      
      drupal_alter( "saml_authncontext_classReferences", $classReferences );
      
      cache_set( $cid, $classReferences, "cache_bootstrap" );
    }
  }
  
  if( !is_null($name) ){
    if( array_key_exists( $name, $classReferences ) ){
      return $classReferences[$name];
    }
    return ;
  }
  
  return $classReferences;
}

/**
 * Implementation of hook_saml_authncontext_classReferences
 */
function saml_service_idp_saml_authncontext_classReferences(){
  return array(
    SAML20Names::AUTHNCONTEXT_CLASS_PREVIOUS_SESSION => array(
      'className'             => 'SamlServicePreviousSessionAuthnEvaluator',
      'file'                  => "includes/authn.inc",
    )
  );
}
  
  
/**
 * Implementation of hook_requirements
 */
function saml_service_idp_requirements($phase) {
  $requirements = array();
  // Ensure translations don't break during installation.
  $t = get_t();

  if ($phase == 'runtime') {
    if( $entity = saml_service_application_entity() ){
      if( $descriptor = $entity->getDescriptor( SamlServiceRoleDecriptorFilter::createFilterCallback("SAML20CodecIDPSSODescriptor") ) ){
	if(
	  $keyDescriptor = array_shift( $descriptor->getKeyDescriptors( SamlServiceKeyDecriptorFilter::createFilterCallback( SAML20CodecKeyDescriptor::USE_SIGNING ) ) )
	){
	  $requirements['saml_service_idp_sign'] = array(
	    'title' => $t('SAML Role: Identity Provider - Signature Certificate'),
	  );
	  
	  if(
	    ( $keyPair = $keyDescriptor->getKeyPair()) &&
	    ( $public = $keyPair->getPublicKey() ) &&
	    ( $details = openssl_x509_parse( SAMLSignatureUtils::publicKeyToX509($public) ) )
	  ){
	    if( intval(gmdate("U")) > saml_service_openssl_to_timestamp($details['validTo']) ){
	      $requirements['saml_service_idp_sign'] += array(
		'severity' => REQUIREMENT_WARNING,
		'value'    => $t('Expired'),
		'description' => $t("The public certificate used for %use expired on %date.", array( "%use" => $t( strtoupper(SAML20CodecKeyDescriptor::USE_SIGNING) ) , "%name" => $t( 'Identity Provider'), '%date' => format_date( saml_service_openssl_to_timestamp($details['validTo']), 'long' ) ))
	    );
	    }
	    
	    $requirements['saml_service_idp_sign'] += array(
	      'severity' => REQUIREMENT_INFO,
	      'value'    => $t('Pass'),
	      'description' => $t("The public certificate used for %use will expire on %date.", array( "%use" => $t( strtoupper(SAML20CodecKeyDescriptor::USE_SIGNING) ) , "%name" => $t( 'Identity Provider'), '%date' => format_date( saml_service_openssl_to_timestamp($details['validTo']), 'long' ) ))
	    );
	  }
	  else {
	    $requirements['saml_service_idp_sign'] += array(
	      'severity' => REQUIREMENT_ERROR,
	      'value'    => $t('Missing and/or Invalid'),
	      'description' => $t("Could not locate a valid public key used for %use.", array( "%use" => $t( strtoupper(SAML20CodecKeyDescriptor::USE_SIGNING) ) , "%name" => $t( 'Identity Provider') ))
	    );
	  }

	}
	
	if(
	  $keyDescriptor = array_shift( $descriptor->getKeyDescriptors( SamlServiceKeyDecriptorFilter::createFilterCallback( SAML20CodecKeyDescriptor::USE_ENCRYPTION ) ) )
	){
	  $requirements['saml_service_idp_enc'] = array(
	    'title' => $t('SAML Role: Identity Provider - Encryption Certificate'), 
	  );
	  
	  if(
	    ( $keyPair = $keyDescriptor->getKeyPair()) &&
	    ( $public = $keyPair->getPublicKey() ) &&
	    ( $details = openssl_x509_parse( SAMLSignatureUtils::publicKeyToX509($public) ) )
	  ){
	    if( intval(gmdate("U")) > saml_service_openssl_to_timestamp($details['validTo']) ){
	      $requirements['saml_service_idp_enc'] += array(
	      'severity'   => REQUIREMENT_WARNING,
	      'value'       => $t('Expired'),
	      'description' => $t("The public certificate used for %use expired on %date.", array( "%use" => $t( strtoupper(SAML20CodecKeyDescriptor::USE_ENCRYPTION) ) , "%name" => $t( 'Identity Provider'), '%date' => format_date( saml_service_openssl_to_timestamp($details['validTo']), 'long' ) ))
	    );
	    }
	    
	    $requirements['saml_service_idp_enc'] += array(
	      'severity' => REQUIREMENT_INFO,
	      'value'       => $t('Pass'),
	      'description' => $t("The public certificate used for %use will expire on %date.", array( "%use" => $t( strtoupper(SAML20CodecKeyDescriptor::USE_ENCRYPTION) ) , "%name" => $t( 'Identity Provider'), '%date' => format_date( saml_service_openssl_to_timestamp($details['validTo']), 'long' ) ))
	    );
	  }
	  else {
	    $requirements['saml_service_idp_enc'] += array(
	      'severity' => REQUIREMENT_ERROR,
	      'value'    => $t('Missing and/or Invalid'),
	      'description' => $t("Could not locate a valid public key used for %use.", array( "%use" => $t( strtoupper(SAML20CodecKeyDescriptor::USE_ENCRYPTION) ) , "%name" => $t( 'Identity Provider') ))
	    );
	  }

	}
	
      }
      else {
	$requirements['saml_service_idp'] = array(
	  'title' => $t('SAML Role: Identity Provider'), 
	  'value'    => $t('Missing and/or Invalid'),
	  'severity' => REQUIREMENT_ERROR,
	  'description' => $t('Could not find the %name role in the SAML application configuration.', array( "%name" => $t( 'Identiy Provider') ))
	);
      }
    }
  }
  
  return $requirements;
}

/**
 * Implements hook_rules_event_info()
 */
function saml_service_idp_rules_event_info() {
  $defaults = array(
    'group' => t('SAML'),
    'module' => 'saml_service_idp',
  );
  
  return array(
    'attributeStatement_prepare' => $defaults+array(
      'label' => t('AttributeStatement is being prepared'),
      'variables' => array(
	'attributeStatement' => array( 'type' => 'saml_attributeStatement', 'label' => t('Attribute Statement') ),
	'audience'           => array( 'type' => 'text', 'label' => t('Audience') ),
	'user'               => array( 'type' => 'user', 'label' => t('user') ),
      )
    )
  );
}

/**
 * Implements hook_rules_data_info()
 */
function saml_service_idp_rules_data_info() {
  return array(
    'saml_attribute' => array(
      'label' => t('SAML Attribute'),
      'wrap' => false,
      'property info' => array(
	'name' => array(
	  'type' => 'text',
	  'label' => t('The name for the attribute.')
	),
	'nameFormat' => array(
	  'type' => 'text',
	  'label' => t('The nameFormat of the attribute.'),
	),
	
	'value' => array(
	  'type' => 'list<text>',
	  'label' => t('The value(s) for the attribute.'),
	),
      )
    ),
    'saml_attributeStatement' => array(
      'label' => t('SAML AttributeStatement'),
      'wrap' => false,
      'property info' => array(
	'attributes' => array(
	  'type' => 'list<saml_attribute>',
	  'label' => t('The value(s) for the attribute.'),
	),
      )
    ),
  );
}


/**
 * Implements hook_rules_action_info()
 */
function saml_service_idp_rules_action_info() {
  return array(
    'saml_attributeStatement_add_attribute' => array(
      'label' => t('Add an attribute to an Attribute Statement.'), 
      'parameter' => array(
        'saml_attributeStatement' => array(
          'type' => 'saml_attributeStatement',
          'label' => t('SAML Attribute Statement'),
        ),
	'name' => array(
          'type' => 'text',
          'label' => t('Name'),
          'optional' => true
        ),
        'nameFormat' => array(
          'type' => 'text',
          'label' => t('Name Format'),
          'optional' => true
        ),
        'friendlyName' => array(
          'type' => 'text',
          'label' => t('Friendly Name'),
          'optional' => true
        ),
        'value' => array(
          'type' => 'list<text>',
          'label' => t('Value(s)'),
        ),
      ), 
      'group' => t('SAML'),
      'base'  => 'saml_service_idp_rules_attributeStatement_add_attribute',
    ),
  );
}

function saml_service_idp_rules_attributeStatement_add_attribute( &$attributeStatement, $name, $nameFormat, $friendlyName, $values, $context=array()){
  
  $attrib = new Saml20CodecAttribute();
  $attrib->setName( $name );
  $attrib->setNameFormat($nameFormat);
  $attrib->setFriendlyName($friendlyName);
  
  foreach( $values as $value ){
    $attrib->addValue($value);
  }
  
  $attributeStatement->addAttribute( $attrib ); unset($attrib);
}

/**
 * Implementation of hook_form_FORM_ID_alter()
 */
function saml_service_idp_form_saml_service_development_general_form_alter( &$form, &$form_state ){
  SAMLServiceIDP_Development::development_general_form_alter($form, $form_state);
}

/**
 * Implementation of hook_cron()
 **/
function saml_service_idp_cron(){
  if( ( $retention =  variable_get( "saml_service_idp:access_retention", "-1 year" ) ) && $retention != -1 ){
    db_delete("saml_entity_access")->condition('access', strtotime($retention), "<" )->execute(); ;
  }
}

/**
 * Implementation of hook_user_delete()
 **/
function saml_service_idp_user_delete($account) {
  db_delete('saml_entity_access')->condition('uid', $account->uid)->execute();
}

/**
 * Implementation of hook_help()
 **/
function saml_service_idp_help($path, $arg) {
  if( class_exists('SamlServiceIDP_UI_Help') ){
    return SamlServiceIDP_UI_Help::hook( $path, $arg );
  }
}

/**
 * Implementation of hook_saml_log_canidates()
 **/
function saml_service_idp_saml_log_canidates(){
  return array(
    array(
      'flag'   => SAML_SERVICE_IDP_LOG_ENTITY_ACCESS,
      'class'  => 'SAMLServiceIDPLogger'
    )
  );
}