<?php

function saml_service_sp_admin_roledescriptor_config_form( $form, &$form_state ){
  $form['nameIDFormat_fieldset'] = array(
    '#title'         => t('Name ID Format'),
    '#type'          => 'fieldset',
    
    'nameIDFormat'   => array(
      '#title'         => t("Useable Format(s)"),
      '#type'          => 'select',
      '#options'       => array(
	SAML20Names::NAMEIDFORMAT_1_1_UNSPECIFIED  => t("Unspecified (Supply Value)"),
	SAML20Names::NAMEIDFORMAT_1_1_EMAILADDRESS => t("E-mail Address"),
	SAML20Names::NAMEIDFORMAT_2_0_TRANSIENT    => t("Transient (Session Id)"),
	SAML20Names::NAMEIDFORMAT_2_0_PERSISTENT   => t("Persistent (Username)")
      ), 
      '#required'      => true,
      '#multiple'      => true,
      '#default_value' => $form_state['storage']['nameIDFormat']
    ),

  );

  $form['bindings'] = array(
    '#title'        => t('Binding(s)'),
    '#type'         => 'fieldset',
    '#description'   => t("Configure the bindings that are accepted by ." ),
    '#tree'         => true,
    
    'accepted'      => array(
      '#title'         => t('Accepted'),
      '#type'          => 'select',
      '#options'       => array(
	SAML20Names::BINDINGS_HTTP_POST     => "HTTP Post",
	SAML20Names::BINDINGS_HTTP_REDIRECT => "HTTP Redirect",
	SAML20Names::BINDINGS_SOAP          => "SOAP",
      ),
      '#description'   => t("The bindings that are accepted by this application." ),
      '#required'      => true,
      '#multiple'      => true,
      '#default_value' => $form_state['storage']['bindings']
    ),
  );
  
  $form['#submit'][] = 'saml_service_sp_admin_roledescriptor_config_form_submit';
  
  return $form;
}

function saml_service_sp_admin_roledescriptor_config_form_submit( $form, &$form_state ){
  $form_state['storage']['bindings'] = $form_state['values']['bindings']['accepted'];
  $form_state['storage']['nameIDFormat'] =  $form_state['values']['nameIDFormat'];
}

/**
 * The callback for the configuration task of the SAML20CodecSPSSODescriptor role.
 */
function saml_service_sp_admin_ssodescriptor_config_form( $form, &$form_state ){
  if( !isset($form_state['storage']) ){
    $form_state['storage'] = array(
      'nameIDFormat'   => $form_state['descriptor']->getNameIDFormat(),
      'bindings'       => array(),
      'defaultBinding' => null,
      'keyDescriptors' => $form_state['descriptor']->getKeyDescriptors(),
      'roleMapping'    => array(
	'enabled'       => false,
	'attributeName' => ""
      )
    );
    
    $services = $form_state['descriptor']->getAssertionConsumerService();
    reset($services);
    while( list( ,$service ) = each($services) ){
      if( !in_array( $service->getBinding(), $form_state['storage']['bindings'] ) ){
	$form_state['storage']['bindings'][] = $service->getBinding();
      }
      
      if( $service->isDefault() ){
	$form_state['storage']['defaultBinding'] = $service->getBinding();
      }
    }
    unset($services);
  }
  
  $form = saml_service_sp_admin_roledescriptor_config_form( $form, $form_state );
  
  $form['bindings']['default'] = array(
    '#title'         => t('Default'),
    '#type'          => 'select',
    '#options'       => array(
	SAML20Names::BINDINGS_HTTP_POST     => "HTTP Post",
	SAML20Names::BINDINGS_HTTP_REDIRECT => "HTTP Redirect",
	SAML20Names::BINDINGS_SOAP          => "SOAP",
    ),
    '#description'   => t("The preferred binding to use by default when authenticating." ),
    '#required'      => true,
    '#default_value' =>  $form_state['storage']['defaultBinding']
  );
  
  $form['save'] = array(
    '#type'  => 'submit',
    '#value' => t( 'Save' )
  );
  
  $form['#submit'][] = 'saml_service_sp_admin_ssodescriptor_config_form_submit';
  
  return $form;
}

function saml_service_sp_admin_ssodescriptor_config_form_submit( $form, &$form_state ){
  $form_state['storage']['defaultBinding'] = $form_state['values']['bindings']['default'];

  $entity = $form_state['entity'];
  
  if( $entityDescriptor = $entity->getDescriptor() ){
    if( $descriptors = $entityDescriptor->getDescriptors() ){
      $entity->getDescriptor()->clearDescriptors();
      reset($descriptors);
      while( list(,$descriptor) = each($descriptors) ){
				if( $descriptor instanceof SAML20CodecSPSSODescriptor ){
					$descriptor->clearNameIDFormat();
					array_walk( $form_state['storage']['nameIDFormat'], array( $descriptor, "addNameIDFormat" ) );
					
					$descriptor->clearAssertionConsumerService();
					$descriptor->clearSingleLogoutService();
					
					$bindingIndex = 0;
				
					reset($form_state['storage']['bindings']);
					while( list(,$binding) = each($form_state['storage']['bindings']) ){
							$entity_service = new SAML20CodecEndpoint();
							$entity_service->setLocation( url( "saml2/service/sp/singleLogoutService", array( 'absolute' => true ) ) );
							$entity_service->setBinding( $binding );
							$descriptor->addSingleLogoutService($entity_service); unset($entity_service);
							
							$entity_service = new SAML20CodecIndexedEndpoint();
							$entity_service->setIndex( $bindingIndex++ );
							$entity_service->setDefault( $binding == $form_state['storage']['defaultBinding'] );
							$entity_service->setLocation( url( "saml2/service/sp/assertionConsumerService", array( 'absolute' => true ) ) );
							$entity_service->setBinding( $binding );
							$descriptor->addAssertionConsumerService($entity_service); unset($entity_service);
					}
					
				}
				
			$entity->getDescriptor()->addDescriptor($descriptor); unset($descriptor);
      }
    }
  }
  
  SAML::getInstance()->saveEntity($entity); unset($entity);

  $form_state['rebuild'] = false;
  unset($form_state['storage']);
}

/**
 * The callback for the configuration task of the SAML20CodecSPSSODescriptor role.
 */
function saml_service_sp_add_authnAuthorityDescriptor_page(){
  
  if( saml_service_entity_not_have_role( 1, "SAML20CodecAuthnAuthorityDescriptor" ) ){
    module_load_include( "pages.inc", "saml_service","includes/admin" );
    
    $entity =  SAML::getInstance()->getEntityById( 1 );

    $entity->getDescriptor()->addDescriptor( new SAML20CodecAuthnAuthorityDescriptor("urn:oasis:names:tc:SAML:2.0:protocol") );
    
    return saml_service_admin_descriptor_router($entity, "role", "SAML20CodecAuthnAuthorityDescriptor", "configure");
  }
  
  drupal_goto( "admin/config/services/saml/federation/1/role/".base64_encode("SAML20CodecAuthnAuthorityDescriptor") );
}

function saml_service_sp_admin_authndescriptor_config_form( $form, &$form_state ){
  if( !isset($form_state['storage']) ){
    $form_state['storage'] = array(
      'nameIDFormat'   => $form_state['descriptor']->getNameIDFormat(),
      'bindings'       => array()
    );
    
    $services = $form_state['descriptor']->getAuthnQueryService();
    reset($services);
    while( list( ,$service ) = each($services) ){
      if( !in_array( $service->getBinding(), $form_state['storage']['bindings'] ) ){
	$form_state['storage']['bindings'][] = $service->getBinding();
      }
    }
    unset($services);
  }
  
  $form = saml_service_sp_admin_roledescriptor_config_form( $form, $form_state );
  
  $form['save'] = array(
    '#type'  => 'submit',
    '#value' => t( 'Save' )
  );
  
  $form['#submit'][] = 'saml_service_sp_admin_authndescriptor_config_form_submit';
  
  return $form;
}

function saml_service_sp_admin_authndescriptor_config_form_submit( $form, &$form_state ){

  $entity = $form_state['entity'];
  
  if( $entityDescriptor = $entity->getDescriptor() ){
    if( $descriptors = $entityDescriptor->getDescriptors() ){
      $entity->getDescriptor()->clearDescriptors();
      reset($descriptors);
      while( list(,$descriptor) = each($descriptors) ){
	if( $descriptor instanceof SAML20CodecAuthnAuthorityDescriptor ){
	  $descriptor->clearNameIDFormat();
	  array_walk( $form_state['storage']['nameIDFormat'], array( $descriptor, "addNameIDFormat" ) );
	  
	  $descriptor->clearAuthnQueryService();
	  
	  $bindingIndex = 0;
	
	  reset($form_state['storage']['bindings']);
	  while( list(,$binding) = each($form_state['storage']['bindings']) ){
	      $entity_service = new SAML20CodecEndpoint();
	      $entity_service->setLocation( url( "saml2/service/authnAuthority/authnQueryService", array( 'absolute' => true ) ) );
	      $entity_service->setBinding( $binding );
	      $descriptor->addAuthnQueryService($entity_service); unset($entity_service);
	  }
	  
	}
	
	$entity->getDescriptor()->addDescriptor($descriptor); unset($descriptor);
      }
      
    }
  }

  SAML::getInstance()->saveEntity($entity); unset($entity);
  drupal_set_message( t("Descriptor Updated") );
  $form_state['rebuild'] = false;
  unset($form_state['storage']);
  $form_state['redirect'] = "admin/config/services/saml/federation/1/role/".base64_encode("SAML20CodecAuthnAuthorityDescriptor");
}

function saml_service_sp_admin_authn_default_entity_form( $form, &$form_state ){

  $form['statement'] = array(
    '#prefix' => '<h3>',
    '#suffix' => '</h3>',
    '#markup' => t('Are you sure you want to use this entity as the Authn Statement handler?')
  );
  
  $form['descriptor'] = array(
    '#type' => "fieldset",
    '#collapsible' => true,
    
    array(
      '#prefix' => "<xmp style=\"white-space:pre-wrap;\">",
      '#suffix' => "</xmp>",
      '#markup' => SAML::getInstance()->toXML( $form_state['entity']->getDescriptor(), array( "formatOutput" => true ) )
    )
  );

  $form['save'] = array(
    '#type'  => 'submit',
    '#value' => t( 'Yes' )
  );
  
  $form['no'] = array(
    '#markup' => l( t( 'No' ), "admin/config/services/saml/federation/".$form_state['entity']->getId() ."/role/".base64_encode("SAML20CodecIDPSSODescriptor"), array( "attributes" => array( 'class' => array( 'button' ) ) ) )
  );
  
  return $form;
}

function saml_service_sp_admin_authn_default_entity_form_submit( $form, &$form_state ){
  variable_set("saml_service_sp:default_authn_entity", $form_state['entity']->getId());
  drupal_set_message( t(
    "%entity was set as default Authnstatement handler.",
    array(
      "%entity" => $form_state['entity']->getName(). "( ".$form_state['entity']->getEntityId()." )"
    )
  ) );
  
  $form_state['redirect'] = "admin/config/services/saml/federation/".$form_state['entity']->getId() ."/role/".base64_encode("SAML20CodecIDPSSODescriptor");
}

/**
 * Removes the AuthnEntity role from the application entity
 */
function saml_service_sp_admin_authndescriptor_delete_form( $form, &$form_state ){
  $form['statement'] = array(
    '#prefix' => '<h3>',
    '#suffix' => '</h3>',
    '#markup' => t('Are you sure you want to remove the AuthnAuthority role from this application?')
  );
  
  $form['notice'] = array(
    '#prefix' => '<h4>',
    '#suffix' => '</h4>',
    '#markup' => t('This application will no longer be able to handle authentication requests and all associated key data will be removed.')
  );


  $form['save'] = array(
    '#type'  => 'submit',
    '#value' => t( 'Yes' )
  );
  
  $form['no'] = array(
    '#markup' => l( t( 'No' ), "admin/config/services/saml/federation/".$form_state['entity']->getId() ."/role/".base64_encode("SAML20CodecIDPSSODescriptor"), array( "attributes" => array( 'class' => array( 'button' ) ) ) )
  );
  
  return $form;
}

function saml_service_sp_admin_authndescriptor_delete_form_submit( $form, &$form_state ){
  
    $entity = $form_state['entity'];
  
  if( $entityDescriptor = $entity->getDescriptor() ){
    if( $descriptors = $entityDescriptor->getDescriptors() ){
      $entity->getDescriptor()->clearDescriptors();
      reset($descriptors);
      while( list(,$descriptor) = each($descriptors) ){
	if( $descriptor instanceof SAML20CodecAuthnAuthorityDescriptor ){
	  
	  $keyDescriptors = $descriptor->getKeyDescriptors();
	  reset($keyDescriptors);
	  while( list(,$keyDescriptor) = each($keyDescriptors) ){
	    if( $keyDescriptor instanceof  SAMLServiceKeyDescriptor){
	      db_delete( "saml_keypair" )->condition("id",$keyDescriptor->getKeyPairId())->execute();
	    }
	  }
	  
	  continue;
	}
	
	$entity->getDescriptor()->addDescriptor($descriptor); unset($descriptor);
      }
      
    }
  }

  SAML::getInstance()->saveEntity($entity); unset($entity);
  
  drupal_set_message( t(
    "%role was removed from the application.",
    array(
      "%role" => "AuthnAuthority"
    )
  ) );
  
  $form_state['redirect'] = "admin/config/services/saml";
}
