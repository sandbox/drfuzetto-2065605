<?php

class SAMLServiceSPFilter {

  public static function process($text, $filter, $format, $langcode, $cache, $cache_id){
    $matches = array();
    preg_match_all( "/\<object[^>]+\>(.(?<!\<\/object\>)){0,}\<\/object\>/ims", $text, $matches );
    if( $matches && $matches[0] ){
	reset($matches[0]);
	while( list($index,$tag) = each($matches[0]) ){
	  if( ( $object_dom = DOMDocument::loadHTML($tag) ) ){
	    $object_node = $object_dom->getElementsByTagName("object")->item(0);
	    if( (  $classid_attr = $object_node->attributes->getNamedItem( "classid" ) ) && is_a($classid_attr, "DOMAttr") && strtolower($classid_attr->value) == "saml_assertion" ){
	      $assertion_cfg = array(
		'sign'                  => variable_get("saml_service_sp:assert:filter:sign", false),
		'authnContext'          => variable_get("saml_service_sp:assert:filter:authnContext", SAML20Names::AUTHNCONTEXT_CLASS_PREVIOUS_SESSION),
		'spEntity'              => null,
		'authnEntity'           => null,
		'relayState'            => null,
		'extensionAttributes'   => array()
	      );
	      
	      if( $authnEntity = saml_service_sp_default_authn_entity() ){
		$assertion_cfg['authnEntity'] = $authnEntity->getEntityId();
	      } unset($authnEntity);
	    
	      $params = $object_dom->getElementsByTagName("param");
	      for( $i = 0; $i < $params->length; $i++ ){
		$param = $params->item($i);
		switch( strtolower( $param->attributes->getNamedItem( "name" )->value ) ){
		  case "sign":
		    $assertion_cfg['sign'] = $param->attributes->getNamedItem( "value" )->value;
		  break;
		  case "authncontext":
		    $assertion_cfg['authnContext'] = $param->attributes->getNamedItem( "value" )->value;
		  break;
		  case "spentity":
		    $assertion_cfg['spEntity'] = $param->attributes->getNamedItem( "value" )->value;
		  break;
		  case "authnentity":
		    $assertion_cfg['authnEntity'] = $param->attributes->getNamedItem( "value" )->value;
		  break;
		  case "relaystate":
		    $assertion_cfg['relayState'] = urlencode( $param->attributes->getNamedItem( "value" )->value );
		  break;
		  default:
		    if( ( $type = $param->attributes->getNamedItem( "type" )->value ) && $type == "saml_attribute"){
		      $assertion_cfg['extensionAttributes'][ $param->attributes->getNamedItem( "name" )->value ] = $param->attributes->getNamedItem( "value" )->value;
		    }
		}
	      } unset($param);
	      
	      $data = base64_encode(serialize($assertion_cfg));
	      if(
		( !empty($assertion_cfg['spEntity']) && SAML::getEntity($assertion_cfg['spEntity']) ) &&
		( !empty($assertion_cfg['authnEntity']) && SAML::getEntity($assertion_cfg['authnEntity']) ) &&
		strlen($data) < 2048
	      ){
		$text = str_replace( $tag, l( preg_replace("/\n|\r\n/","\\n",trim($object_node->textContent)), "saml2/service/relay/{$data}", array( 'html' => true )), $text );
	      }
	      else {
		$text = str_replace( $tag, "", $text );
	      }
	      unset($data); unset($assertion_cfg);
	       
	    }
	  }
	}
    }
    return $text;
  }

}
