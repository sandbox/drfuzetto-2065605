<?php

class SamlServiceSPUtils {
  public static function getBinding( $binding, $endPoints ){
    reset($endPoints);
    while( list(,$endPoint) = each($endPoints) ){
      if( $endPoint->getBinding() == $binding ){
		return $endPoint;
      }
    }
    unset($endPoint); unset($endPoints);
  }
  
}

/**
 * This class handles the Saml Service Interactions
 */
class Saml20SpService extends SamlService {
  
  public function handleAssertionConsumerService( SamlMessageContext &$messageContext ){
    
    $sp_binding = SamlServiceSPUtils::getBinding(
      $messageContext->bindingType,
      $messageContext->serviceDescriptor->getAssertionConsumerService()
    );
    
    if( $sp_binding === false ){
	  $sp_binding = array_pop( $messageContext->serviceDescriptor->getAssertionConsumerService() );
    }
    
    if( $sp_binding === false ){
      throw new Exception("Unable to locate any valid binding for Identity Provider");
    }
    
    if( !( $messageContext->response instanceof Saml20CodecResponse ) ){
      throw new Exception("Unable to locate SAMLResponse. Response was <xmp>{$messageContext->responseXML}</xmp>");
    }
    
    if( !$messageContext->authnEntity ){
      if( ($issuer = $messageContext->response->getIssuer() ) && ( $entityId = $issuer->getValue() ) && !empty($entityId) ){
		if( ( $messageContext->authnEntity = Saml::getInstance()->getEntity( $entityId ) )  ){
		  $descriptors = $messageContext->authnEntity->getDescriptor()->getDescriptors();
		  reset($descriptors);
		  while( list( ,$descriptor) = each($descriptors) ){
			if(
			  ( $descriptor instanceof SAML20CodecIDPSSODescriptor ) ||
			  ( $descriptor instanceof SAML20CodecAuthnAuthorityDescriptor )
			){
			  $messageContext->authnRoleDescriptor = $descriptor;
			}
		  }
		  unset($descriptor); unset($descriptors); unset($entity);
		}
      } unset( $issuer ); unset( $entityId );
    }
    
    if( !$messageContext->authnRoleDescriptor ){
      throw new Exception("Unable to determine the Source Identity Provider");
    }

    {//verify signature
      $signature = $messageContext->response->getSignature();
      if( !$signature ){
		throw new Exception("Message was not signed");
      }
      
      $validated = false;
      
      $keyDescriptors = $messageContext->authnRoleDescriptor->getKeyDescriptors();

      reset($keyDescriptors);
      while( list(,$keyDescriptor) = each($keyDescriptors) ){
		if( $keyDescriptor->getUse() == SAML20CodecKeyDescriptor::USE_SIGNING ){
		  $signature->validateElement( $keyDescriptor->getKeyPair() );
		  $validated = true;
		}
      }
      
      if( !$validated ){
		throw new Exception("Unable to validate message");
      }
    }
    
    $this->verifyResponseIsUnique($messageContext->response);
    
    if(
      ( $status = $messageContext->response->getStatus() ) &&
      ( $statusCode = $status->getStatusCode() ) &&
      ( $statusCode->getValue() != SAML20Names::STATUS_SUCCESS )
    ){
      throw new Exception(empty($message) ? "Assertion Recieved as Failure":$message);
    }    
    
    $assertions = $messageContext->response->getAssertions();
    
    reset($assertions);
    while( list( ,$assertion ) = each( $assertions ) ){
      //TO-DO: need to verify assertion
      $statements = $assertion->getStatements();
      
      if( $conditions = $assertion->getConditions() ){
		try{
		  $this->verifySaml20Conditions( $conditions );
		}
		catch( Exception $e ){
		  continue; //ignore statement and continue processing
		}
      } unset($conditions);
      
      $authnStatement = false;
      $attributeStatement = false;
      
      reset($statements);
      while( list( , $statement) = each($statements) ){
		if( $statement instanceof Saml20CodecAuthnStatement ){
		  $authnStatement = $statement;
		}
		if( $statement instanceof Saml20CodecAttributeStatement ){
		  $attributeStatement = $statement;
		}
      }
      
      if( $attributeStatement == false ){
		throw new Exception("Unable to locate AttributeStatement");
      }
      
      //TO-DO: need to verify authnStatement
      $attributes = $attributeStatement->getAttributes();
      
      if( !isset($_SESSION) || is_null($_SESSION) ){
		drupal_session_start();
      }
      
      if( !array_key_exists( 'saml', $_SESSION ) ){
		$_SESSION['saml'] = array();
      }
      $_SESSION['saml']['attributes'] = $attributes;
      
      reset($attributes);
      while( list(, $attribute) = each($attributes) ){
		if( $attribute->getName() == "mail" ){
		  if( ($values = $attribute->getValues()) && ($mail = array_shift( $values)) ){
			if( $account = user_load_by_mail($mail) ){
			  $form_state['uid'] = $account->uid;
			  user_login_submit(array(), $form_state);
			}
			else {
			  user_external_login_register( $mail, "saml_service_sp" );
			  user_save( $GLOBALS['user'], array( 'mail' => $mail ) );
			}
			break;
		  }
		}
      }
      
      if($GLOBALS['user']->uid > 0){
	$_SESSION['saml']['attributes'] = $attributes;
	$modules = module_implements("saml_role_sp_user_login");
	foreach( $modules as $module ){
	  $callback = "{$module}_saml_role_sp_user_login";
	  if( function_exists($callback) ){
	    $callback( $attributes, $assertion );
	  }
	}
	      
	unset( $assertion );//reclaim the memory from the assertion as it is no longer needed
      
	drupal_goto( $messageContext->relayState );
      }
    }
    
    throw new Exception("Unable to locate a valid statement");
    
  }
  
}

/**
 * This class handles the Saml Service Interactions
 */
class Saml20AuthnAuthorityService extends SamlService {
  public function createFailedResponse( SamlMessageContext &$messageContext, $message = "" ){
    $response = new Saml20CodecResponse();
        
    if(
      ( $keyDescriptor = array_shift( $messageContext->authnRoleDescriptor->getKeyDescriptors( SamlServiceKeyDecriptorFilter::createFilterCallback( SAML20CodecKeyDescriptor::USE_SIGNING ) ) ) )
    ){
      $signature = new XMLDigitalSignature(  );
      $signature->setIncludePubKey( true );
      $signature->setKeyPair( $keyDescriptor->getKeyPair() ); unset($signatureInfo);
      
      $response->setSignature( $signature );
    } unset($signature);
    
    $response->setIssuer( new Saml20CodecNameID() );
    $response->getIssuer()->setValue( $messageContext->authnEntity->getEntityId() );
    
    $status = new Saml20CodecStatus();

    $statusCode = new Saml20CodecStatusCode();
    $statusCode->setValue(SAML20Names::STATUS_FAILURE);
    $status->setStatusCode($statusCode); unset($statusCode);
    
    $status->setStatusMessage( $message );
    
    $response->setStatus($status); unset($status);
    $e = new Exception();
    watchdog( "saml_service_sp", "AuthnAuthority Failure: !message<br/>!trace", array( "!message" => $message, "!trace" => $e->getTraceAsString() ), WATCHDOG_ERROR ); unset($e);
    return $response;
  }

  public function handleAuthnQueryService( SamlMessageContext &$messageContext ){
    $messageContext->initialize();
    
    if( $messageContext->request instanceof SAML20CodecAuthnRequest ){
      $this->handleAuthnRequest($messageContext);

    }
    else if( $messageContext->request instanceof SOAPMessage ){
      $SOAPMessage = $messageContext->request;
      
      $authnContext = $messageContext->createChildContext();
      
      $authnContext->request = $SOAPMessage->getBody();
      
      $this->handleAuthnQueryService($authnContext);
      
      $SOAPResponse = new SOAPMessage();
      $SOAPResponse->setBody( $authnContext->response ); unset($authnContext);

      $messageContext->response = $SOAPResponse;
    }
  }
  
  private function handleAuthnRequest( SamlMessageContext &$messageContext ){
    $messageContext->initialize();
    
    if( !$messageContext->serviceEntity ){
      $messageContext->serviceEntity = $this->getApi()->getEntity( $messageContext->request->getIssuer()->getValue() );
      $messageContext->serviceDescriptor = $messageContext->authnEntity->getDescriptor( SamlServiceRoleDecriptorFilter::createFilterCallback( "SAML20CodecSPSSODescriptor" ) );
    }
  
    if( !$messageContext->serviceDescriptor ){
      $messageContext->response = $this->createFailedResponse( $messageContext, "Entity Id `".$messageContext->request->getIssuer()->getValue()."` does not belong to this federation." );
    }
    
    $nameID = false;
    
    if( $messageContext->request->getSubject() && $messageContext->request->getSubject()->getId() ){
      $nameID = $messageContext->request->getSubject()->getId();
      if(
	  ( $keyDescriptor = array_shift( $messageContext->authnRoleDescriptor->getKeyDescriptors(
		SamlServiceKeyDecriptorFilter::createFilterCallback( SAML20CodecKeyDescriptor::USE_ENCRYPTION )
	  ) ) ) &&
	  ( $nameID instanceof SAML20CodecEncryptedID ) )
	  {
		try {
		  $encKey = new XMLEncryption();
		  $encKey->setKeyPair( $keyDescriptor->getKeyPair() );
		  $nameID->setKey( $encKey );
		  
		  $nameID = SAML::fromXML( $nameID->getData() );
		} catch( Exception $e ) {
		  $messageContext->response = $this->createFailedResponse( $messageContext, "Unable to decrypt encryptedId in subject" );
		  return ;
		}
      }
    }
    
    if(
	( $entity = $this->getApi()->getEntity( $messageContext->request->getIssuer()->getValue() ) ) &&
	( $sp = $entity->getDescriptor( SamlServiceRoleDecriptorFilter::createFilterCallback( "SAML20CodecSPSSODescriptor" ) ) ) 
    ){
      $messageContext->serviceEntity = $entity; 
      $messageContext->serviceDescriptor = $sp; 
    } unset($sp);
    
    if( !$messageContext->serviceDescriptor ){
      $messageContext->response = $this->createFailedResponse( $messageContext, $messageContext->request->getIssuer()->getValue()." is not a member of this federation or does not have a Service provider role." );
      return;
    }

    if( $signature = $messageContext->request->getSignature() ){
      
      if( $keyDescriptor = array_shift( $messageContext->serviceDescriptor->getKeyDescriptors( SamlServiceKeyDecriptorFilter::createFilterCallback( SAML20CodecKeyDescriptor::USE_SIGNING ) ) ) ) {
		try {
		  $signature->validateElement( $keyDescriptor->getKeyPair() );
		} catch ( Exception $e ){
		  $messageContext->response = $this->createFailedResponse( $messageContext, "Unable to validate signature." );
		  return ;
		}
      }
      else {
		$messageContext->response = $this->createFailedResponse( $messageContext, "Unable to validate signature." );
		return ;
      }
    }
    
    $sp = $messageContext->authnEntity->getDescriptor( SamlServiceRoleDecriptorFilter::createFilterCallback( "SAML20CodecSPSSODescriptor" ) );
    if( !$sp ){
      $messageContext->response = $this->createFailedResponse( $messageContext, "The federation is not configured properly." );
      return ;
    }
    
    $idp = false;
    $binding = false;
    if( $default_idp_entity = saml_service_sp_default_authn_entity() ){
      if( $idp = $default_idp_entity->getDescriptor( SamlServiceRoleDecriptorFilter::createFilterCallback( "SAML20CodecAuthnAuthorityDescriptor" ) ) ){
		$binding = $idp->getAuthnQueryService( SamlConst::BINDING_2_0_SOAP );
      }
      else if( $idp = $default_idp_entity->getDescriptor( SamlServiceRoleDecriptorFilter::createFilterCallback( "SAML20CodecIDPSSODescriptor" ) ) ){
		$binding = $idp->getSingleSignOnService( SamlConst::BINDING_2_0_SOAP );
      }
    }
    
    
    if( !$binding ){
      $messageContext->response = $this->createFailedResponse( $messageContext, "Unable to validate request with correct authority." );
      return ;
    }
    
    if( $idp && $binding ){
      
      $request = new SAML20CodecAuthnRequest();

      $issuer = new Saml20CodecNameID();
      $issuer->setValue( $messageContext->authnEntity->getEntityId() );
      $request->setIssuer( $issuer );
      
      if( $keyDescriptor = array_shift( $sp->getKeyDescriptors( SamlServiceKeyDecriptorFilter::createFilterCallback( SAML20CodecKeyDescriptor::USE_SIGNING ) ) ) ) {
		$signature = new XMLDigitalSignature();
		$signature->setKeyPair( $keyDescriptor->getKeyPair() );
	
		$request->setSignature($signature); unset($signature);
      }
      
      $request->setRequestedContext($messageContext->request->getRequestedContext());
      
      $subject = new Saml20CodecSubject();

      $subject->setId($nameID); unset($nameID);
      
      $request->setSubject( $subject ); unset( $subject );
            
      $SOAPMessage = new SOAPMessage();
      $SOAPMessage->setBody( $request );
      
      $parentContext = $messageContext->getParentContext();
      if( $parentContext  && ($parentContext->request instanceof SOAPMessage) ){
		$headers = $parentContext->request->getHeaders();
		reset($headers);
		while( list( ,$header) = each($headers) ){
		  if(
			( $header instanceof WSSE11EncryptedHeader ) &&
			( $authnEncKey = array_shift( $messageContext->authnRoleDescriptor->getKeyDescriptors( SamlServiceKeyDecriptorFilter::createFilterCallback( SAML20CodecKeyDescriptor::USE_ENCRYPTION ) ) ) )
		  ) {
			try {
			  
			  $encKey = new XMLEncryption();
			  $encKey->setKeyPair( $authnEncKey->getKeyPair() );
			  $header->setKey( $encKey ); unset($encKey);
			  
			  $header = SAML::fromXML($header->getData() );
			} catch( Exception $e ) {
			  $messageContext->response = $service->createFailedResponse( $messageContext, "Unable to decrypt encryptedId in subject" );
			  return ;
			}
		  } unset($authnEncKey);
		  
		  if(
			( $header instanceof WSSEUsernameToken ) &&
			( $idpEncKey = array_shift( $idp->getKeyDescriptors( SamlServiceKeyDecriptorFilter::createFilterCallback( SAML20CodecKeyDescriptor::USE_ENCRYPTION ) ) ) )
		  ){
			$wsse11EncHeader = new WSSE11EncryptedHeader();
			$encKey = new XMLEncryption();
			$encKey->setKeyPair( $idpEncKey->getKeyPair()  );
			$wsse11EncHeader->setKey( $encKey ); unset($encKey);
			
			$wsse11EncHeader->setData($header); unset($header);
		  
			$SOAPMessage->addHeader($wsse11EncHeader); unset($wsse11EncHeader);
		  } unset($idpEncKey);
		}
      }
      
      $httpRequest = new CodecsHTTPRequest( $binding->getLocation() , CodecsHTTPRequest::METHOD_POST);
      $httpRequest->putHeader( "ACCEPTS", "application/soap+xml" );
      $httpRequest->putHeader( "Content-Type", "application/soap+xml" );
      $httpRequest->setContent( SAML::toXML( $SOAPMessage ) );
      
      $httpResponse = new CodecsBufferedHTTPResponse();
      
      try {
		CodecsHTTPClient::execute($httpRequest, $httpResponse);
	// 	unset($httpRequest);
		
		$message = SAML::fromXML( $httpResponse->getBuffer() );
		if( $message instanceof SOAPMessage ){
		  
		  $response = $message->getBody();
		  
		  if(
			$response &&
			( $status = $response->getStatus() ) &&
			( $statusCode = $status->getStatusCode() ) &&
			( $statusCode->getValue() != SAML20Names::STATUS_SUCCESS )
		  ){
			$message = $status->getStatusMessage();
			$messageContext->response = $this->createFailedResponse( $messageContext, empty($message) ? "Unknown Identity Provider Failure":$message );
			watchdog("saml_service_sp", "Identity Provider Failure !message", array( "!message" => "<pre>".htmlentities($httpResponse->getBuffer())."\n\n".htmlentities( var_export($httpRequest, true) )."</pre>" ), WATCHDOG_DEBUG);
			return;
		  }
		  
		  {//verify signature
			$signature = $response->getSignature();
			if( !$signature ){
			  $messageContext->response = $this->createFailedResponse( $messageContext, "Response from identity provider was not signed.");
			  return;
			}
			
			$validated = false;
			
			$keyDescriptors = $idp->getKeyDescriptors();
	
			reset($keyDescriptors);
			while( list(,$keyDescriptor) = each($keyDescriptors) ){
			  if( $keyDescriptor->getUse() == SAML20CodecKeyDescriptor::USE_SIGNING ){
				$signature->validateElement( $keyDescriptor->getKeyPair() );
				$validated = true;
			  }
			}
			
			if( !$validated ){
			  $messageContext->response = $this->createFailedResponse( $messageContext, "Unable to validate signature for response from identity provider.");
			  return;
			}
		  }
		  
		  $this->verifyMessageIsUnique($response);
		  
		  $assertions = $response->getAssertions();
		  
		  reset($assertions);
		  while( list( ,$assertion ) = each( $assertions ) ){
			$statements = $assertion->getStatements();
		  
			if( $conditions = $assertion->getConditions() ){
			  try{
				$this->verifySaml20Conditions( $conditions );
			  }
			  catch( Exception $e ){
				continue; //ignore statement and continue processing
			  }
			} unset($conditions);
			
			$authnStatement = false;
			$attributeStatement = false;
			
			reset($statements);
			while( list( , $statement) = each($statements) ){
			  if( $statement instanceof Saml20CodecAuthnStatement ){
				$authnStatement = $statement;
			  }
			  if( $statement instanceof Saml20CodecAttributeStatement ){
				$attributeStatement = $statement;
			  }
			}
			
			if( $attributeStatement == false ){
			  $messageContext->response = $this->createFailedResponse( $messageContext, "Unable to locate AttributeStatement");
			  return;
			}
			
			//TO-DO: need to verify authnStatement
			$attributes = $attributeStatement->getAttributes();
			reset($attributes);
			while( list(, $attribute) = each($attributes) ){
			  if( $attribute->getName() == "mail" ){
				if( ($values = $attribute->getValues()) && ($mail = array_shift( $values)) ){
				  
				  if( $account = user_load_by_mail($mail) ){
					$form_state['uid'] = $account->uid;
					user_login_submit(array(), $form_state);
				  }
				  else {
					user_external_login_register( $mail, "saml_service_sp" );
					user_save( $GLOBALS['user'], array( 'mail' => $mail ) );
				  }
				  
				  $response = new Saml20CodecResponse();
				  
				  if(
					( $keyDescriptor = array_shift( $messageContext->authnRoleDescriptor->getKeyDescriptors(
					  SamlServiceKeyDecriptorFilter::createFilterCallback( SAML20CodecKeyDescriptor::USE_SIGNING )
					) ) )
				  ){
					$signature = new XMLDigitalSignature(  );
					$signature->setIncludePubKey( true );
					
					$signature->setKeyPair( $keyDescriptor->getKeyPair() );
					
					$response->setSignature( $signature );
				  }
				  
				  $response->setIssuer( new Saml20CodecNameID() );
				  $response->getIssuer()->setValue( $messageContext->authnEntity->getEntityId() );
				  
				  $status = new Saml20CodecStatus();
				  
				  $statusCode = new Saml20CodecStatusCode();
				  $statusCode->setValue(SAML20Names::STATUS_SUCCESS);
				  $status->setStatusCode($statusCode); unset($statusCode);
				  
				  $response->setStatus($status); unset($status);
				  
				  $sp_assertion = new Saml20CodecAssertion();
				  $sp_assertion->setSubject( $assertion->getSubject() );
				  $sp_assertion->setIssuer( $response->getIssuer() );
				  $sp_assertion->setSignature( $response->getSignature() );
				  
				  $sp_assertion->addStatement( $attributeStatement ); unset($attributeStatement);
		
				  if($authnStatement) {
					$authnStatement->getAuthnContext()->addAuthenticatingAuthority(
					  $messageContext->authnEntity->getEntityId()
					);
					$sp_assertion->addStatement( $authnStatement ); unset($authnStatement);
				  }
				  
				  $conditions = new Saml20CodecConditions();
				  $conditions->setNotOnOrAfter(strtotime( "+5 minutes" ));
				  $conditions->setNotBefore(strtotime( "-1 minute" ));
				  
				  $audienceRestriction = new Saml20CodecAudienceRestriction();
				  $audienceRestriction->addAudience( $messageContext->serviceEntity->getEntityId() );
				  $conditions->addCondition($audienceRestriction); unset($audienceRestriction);
				  
				  $sp_assertion->setConditions( $conditions ); unset($conditions);
				  
				  $response->addAssertion($sp_assertion); unset($sp_assertion);
				  
				  $messageContext->response = $response; unset($response);
				  
				}
			  }
			}
			
			if( !$messageContext->response ){
			  $messageContext->response = $this->createFailedResponse( $messageContext, "Unable to locate AttributeStatement");
			  return;
			}
		  }
		  
		  if( !$messageContext->response ){
			$messageContext->response = $this->createFailedResponse( $messageContext, "Unable to verify any assertion returned from federated authority." );
		  }
		  
		}
		unset($httpResponse);
      } catch( Exception $e ){
		$messageContext->response = $this->createFailedResponse( $messageContext, $e->getTraceAsString() );
		return ;
      }
    }
    if( !$messageContext->response ){
      $messageContext->response = $this->createFailedResponse( $messageContext, "Unknown failure" );
    }
  }
  
}