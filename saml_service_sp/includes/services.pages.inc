<?php

function saml_service_2_sp_sls( $entityId = null){
  //TO DO Implement this service
  return MENU_NOT_FOUND;
}

function saml_service_sp_v2_acs( $entityId = null){

  //force a logout if the user is logged in
  if( user_is_logged_in() ){
    global $user;
    
    watchdog('user', 'Session closed for %name.', array('%name' => $user->name));

    module_invoke_all('user_logout', $user);
    
    $user = drupal_anonymous_user();

    // Destroy the current session data, and reset $user to the anonymous user.
    drupal_session_regenerate();
    
  }
  
  $messageContext = new SamlMessageContext();
  $messageContext->initialize();
  $messageContext->serviceEntity = SAML::getInstance()->getEntityById(1);
  if( !$messageContext->serviceEntity ){
    throw new Exception("This application is not configured to handle SAML assertions.");
  }
  
  $descriptors = $messageContext->serviceEntity->getDescriptor()->getDescriptors();
  reset($descriptors);
  while( list(,$descriptor) = each($descriptors) ){
    if( $descriptor instanceof SAML20CodecSPSSODescriptor ){
      $messageContext->serviceDescriptor = $descriptor;
      break;
    }
  } unset($descriptor); unset($descriptors);
  
  if( !$messageContext->serviceDescriptor ){
    throw new Exception("This application is not configured to handle SAML assertions.");
  }

  if( !is_null($entityId) ){
    $messageContext->authnEntity = SAML::getInstance()->getEntity( $entityId );

    if( !$messageContext->authnEntity ){
      throw new Exception("Unable to locate the given service provider");
    }
  }
  
  Saml::getInstance()->getService("Sp")->handleAssertionConsumerService( $messageContext );
}

function saml_service_sp_v2_aqs(){
   //force a logout if the user is logged in
  if( user_is_logged_in() ){
    global $user;
    
    watchdog('user', 'Session closed for %name.', array('%name' => $user->name));

    module_invoke_all('user_logout', $user);

    // Destroy the current session data, and reset $user to the anonymous user.
    drupal_session_regenerate();
    
  } 
  
  $messageContext = new SamlMessageContext();
  $messageContext->authnEntity = SAML::getInstance()->getEntityById(1);
  if( !$messageContext->authnEntity ){
    throw new Exception("This application is not configured to handle SAML assertions.");
  }
  
  $messageContext->authnRoleDescriptor = $messageContext->authnEntity->getDescriptor( SamlServiceRoleDecriptorFilter::createFilterCallback( "SAML20CodecAuthnAuthorityDescriptor" ) );
  
  if( $messageContext->authnRoleDescriptor ){
    Saml::getInstance()->getService("AuthnAuthority")->handleAuthnQueryService( $messageContext );
      
    if( $messageContext->response ){
      return array(
	'#markup' => SAML::toXML( $messageContext->response )
      );
    }
    
    throw new Exception("Could not determine binding used for SAML Message.");
  }
  
  throw new Exception("This application is not configured to accept AuthnStatements.");
}

function saml_service_sp_v2_relay( $cfg ){

  if(
    ( $sp_entity = SAML::getInstance()->getEntity( $cfg['spEntity'] ) ) &&
    ( $sp_descriptor = $sp_entity->getDescriptor( SamlServiceRoleDecriptorFilter::createFilterCallback("SAML20CodecSPSSODescriptor") ) ) &&
    ( $self_entity = saml_service_application_entity() ) &&
    ( $self_descriptor = $self_entity->getDescriptor( SamlServiceRoleDecriptorFilter::createFilterCallback("SAML20CodecSPSSODescriptor") ) ) &&
    ( $sp_service = $sp_descriptor->getAssertionConsumerService( "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST", ( ( array_key_exists('bindingIndex', $cfg) && is_numeric($cfg['bindingIndex']) ) ? $cfg['bindingIndex']: null ) ) ) &&
    count($sp_service) == 1
  ){
    
    $service = false;
    if(
      ( $authn_entity = SAML::getInstance()->getEntity( $cfg['authnEntity'] ) ) &&
      ( $authn_descriptor = $authn_entity->getDescriptor( SamlServiceRoleDecriptorFilter::createFilterCallback(array( "SAML20CodecAuthnAuthorityDescriptor", "SAML20CodecIDPSSODescriptor") ) ) ) &&
      ( $authn_service = $authn_descriptor->getSingleSignOnService( "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST" ) )
    ){
      $service = $authn_service;
    }
    
    if($service){
      $request = new SAML20CodecAuthnRequest();
      $request->setAssertionConsumerServiceURL($sp_service->getLocation());
      
      $requestedContext = new SAML20CodecRequestedAuthnContext();
      $requestedContext->addClassRef( $cfg['authnContext'] );
      $request->setRequestedContext($requestedContext);

      $issuer = new Saml20CodecNameID();
      $issuer->setValue( $sp_entity->getEntityId() );
      $request->setIssuer( $issuer );
      
      $subject = new Saml20CodecSubject();
      
      {//set condition
		$conditions = new Saml20CodecConditions();
		$conditions->setNotOnOrAfter(strtotime( "+5 minutes" ));
		$conditions->setNotBefore(strtotime( "-2 minutes" ));
		
		$request->setConditions( $conditions ); unset($conditions);
      }
      
      $attributes = new SAML20ExtenstionAssertionAttributes();
      
      reset($cfg['extensionAttributes']);
      while( list( $name, $value ) = each($cfg['extensionAttributes']) ){
	$attrib = new SAML20ExtenstionAssertionAttribute();
	$attrib->setName($name);
	$attrib->addValue($value);
	$attributes->addAttribute($attrib); unset($attrib);
      }

      $request->addExtension( $attributes ); unset($attributes);
      
      $messageContext = new SamlMessageContext();
      $messageContext->serviceEntity = $sp_entity;
      $messageContext->serviceDescriptor = $sp_descriptor;
      $messageContext->authnEntity = $authn_entity;
      $messageContext->authnRoleDescriptor = $authn_descriptor;
      $messageContext->request = $request;
      
      $modules = module_implements("saml_service_sp_prepare_relay");
      foreach( $modules as $module ){
		$callback = "{$module}_saml_service_sp_prepare_relay";
		if( function_exists($callback) ){
		  $callback($messageContext);
		}
      }
      
      $httpRequest = new CodecsHTTPRequest( $service->getLocation() , CodecsHTTPRequest::METHOD_POST);
      $httpRequest->putHeader( "ACCEPTS", "application/xml" );
      $httpRequest->putHeader( "Content-Type", "application/x-www-form-urlencoded" );
      $httpRequest->setContent( "SAMLRequest=".urlencode( base64_encode( $messageContext->requestXML ) ) );
      
      $httpResponse = new CodecsBufferedHTTPResponse();
      
      try{
		CodecsHTTPClient::execute($httpRequest, $httpResponse); unset($httpRequest);
      } catch( Exception $e ){
		watchdog( "saml_service_sp", "Error: !message", array( "!message" => $e->getMessage() ), WATCHDOG_NOTICE );
      }
      
      $messageContext->responseXML = $httpResponse->getBuffer(); unset($httpResponse);
      
      SAMLServiceLogging::getLogger()->logEvent( new SAMLServiceLogEvent(SAML_SERVICE_LOG_MESSAGE_CONTEXT, $messageContext->serviceEntity, $GLOBALS['user'], array( 'messageContext' => $messageContext )) );
      
      $js =<<<EO_JS
(function($){

Drupal.behaviors.samlPost = {
attach: function (context, settings) {
  $("form#samlpost",context).each(function(){
      var form = $(this);
      
      $("input[type='submit']", form).css("display","none");
      
      $(".message", form).css("display","");
      
      
      form.submit();
  });
}
};


})(jQuery);
EO_JS;

      $ret = array(
	'#type' => 'page',
	'content' => array(
	    '#attached' => array(
	      'js' => array(
		array(
		  'type' => 'inline',
		  'data' => $js
		)
	      ),
	    )
	)
      );
      
      if(
	!($messageContext->response instanceof SAML20CodecResponse) ||
	(
	  !$messageContext->response->getStatus() ||
	  !$messageContext->response->getStatus()->getStatusCode() ||
	  $messageContext->response->getStatus()->getStatusCode()->getValue() == SAML20Names::STATUS_FAILURE
	)
      ){
	watchdog( "saml_service_sp", "Relay Error: %message", array("%message" => $messageContext->response->getStatus()->getStatusMessage()), WATCHDOG_ERROR );
	return MENU_ACCESS_DENIED;
      }
      
      $acs = $messageContext->response->getDestination();
      $raw_message = $messageContext->responseXML;
      $message = base64_encode( $raw_message );
      $relayState = urldecode( $cfg['relayState'] );
    
      $ret['content']['#markup'] =<<<EO_HTML
<form method="post" action="{$acs}" id="samlpost">
<input type="hidden" name="SAMLResponse" value="{$message}" />
<input type="hidden" name="RelayState" value="{$relayState}">
<input type="submit" value="Continue" id="action-button" />
<div class="message" style="display:none">
<h2>Processing login please wait.</h2>
</div>
</form>
EO_HTML;
    
      return $ret;
    }
  }
  
  return array(
    '#prefix' => "<div class=\"messages error\">",
    '#suffix' => "</div>",
    '#markup' => t('This site is not properly configured, please contact this site administator.')
  );
}