<?php

class SamlServiceSpUserLogin {

  public static function validate( $form, &$form_state){
    //This methods needs to be updated to use the new authn methods
    $messageContext = new SamlMessageContext();
    
      if( saml_service_sp_preferred_binding() == SAML20Names::BINDINGS_SOAP &&
	( $messageContext->serviceEntity = saml_service_application_entity()) &&
	( $messageContext->serviceDescriptor = $messageContext->serviceEntity->getDescriptor(SamlServiceRoleDecriptorFilter::createFilterCallback( "SAML20CodecSPSSODescriptor" )) ) &&
	( $messageContext->serviceDescriptor instanceof SAML20CodecSPSSODescriptor ) &&
	( $messageContext->authnEntity = saml_service_sp_default_authn_entity() ) &&
	( $messageContext->authnRoleDescriptor = $messageContext->authnEntity->getDescriptor( SamlServiceRoleDecriptorFilter::createFilterCallback( array( "SAML20CodecAuthnAuthorityDescriptor", "SAML20CodecIDPSSODescriptor" ) ) ) ) &&
	( $messageContext->authnRoleDescriptor instanceof SAML20CodecRoleDescriptor) &&
	( $service = $messageContext->authnRoleDescriptor->getSingleSignOnService( SAML20Names::BINDINGS_SOAP ) )
      ){
	$request = new SAML20CodecAuthnRequest();

	$issuer = new Saml20CodecNameID();
	$issuer->setValue( $messageContext->serviceEntity->getEntityId() );
	$request->setIssuer( $issuer );
	
	if( $keyDescriptor = array_shift( $messageContext->serviceDescriptor->getKeyDescriptors( SamlServiceKeyDecriptorFilter::createFilterCallback( SAML20CodecKeyDescriptor::USE_SIGNING ) ) ) ){
	
	  $signature = new XMLDigitalSignature();
	  $signature->setKeyPair( $keyDescriptor->getKeyPair() );

	  $request->setSignature($signature); unset($signature);
	}
	
	{//set requestedauthncontext
	  $requestedContext = new SAML20CodecRequestedAuthnContext();
	  $requestedContext->addClassRef( "urn:bluecoat:names:SAML:2.0:authnContext:WSSEUsernameToken" );
	  $request->setRequestedContext($requestedContext);
	}

	{//set requestors name 
	  $subject = new Saml20CodecSubject();
	 
	  $nameID = new Saml20CodecNameID();
	  $nameID->setValue( $form_state['values']['name'] );
	  $nameID->setFormat( Saml20NameIdFormatURN::PERSISTENT );
	  
	  $subject->setId($nameID); unset($nameID);
	  
	  $request->setSubject( $subject ); unset( $subject );
	}
	      
	$messageContext->request = new SOAPMessage();
	$messageContext->request->setBody( $request );
	
	if(
	  ( $idpEncKey = array_shift( $messageContext->authnRoleDescriptor->getKeyDescriptors( SamlServiceKeyDecriptorFilter::createFilterCallback( SAML20CodecKeyDescriptor::USE_ENCRYPTION ) ) ) )
	){
	  $wsse11EncHeader = new WSSE11EncryptedHeader();
	  $encKey = new XMLEncryption();
	  $encKey->setKeyPair( $idpEncKey->getKeyPair()  );
	  
	  $encID = new SAML20CodecEncryptedID();
	  $encID->setData($request->getSubject()->getId()); unset($nameID);
	  $encID->setKey($encKey);
	  $request->getSubject()->setId($encID); unset($encID);
	  
	  
	  $wsse11EncHeader->setKey( $encKey ); unset($encKey);
	  
	  $wseeUsernameToken = new WSSEUsernameToken();
	  $wseeUsernameToken->setUsername( $form_state['values']['name'] );
	  $wseeUsernameToken->setPassword( $form_state['values']['pass'] );
	  
	  $wsse11EncHeader->setData($wseeUsernameToken); unset($wseeUsernameToken);
    
	  $messageContext->request->addHeader($wsse11EncHeader); unset($wsse11EncHeader);
	  
	  
	//enable customizations to the out bound message based on the form_state
	if( array_key_exists( 'saml', $form_state ) ){
	  if( array_key_exists( 'pre send', $form_state['saml'] ) ){
	    $arguments = array( &$messageContext  );
	    foreach( $form_state['saml']['pre send'] as  $callback_meta ){
	      if( is_callable( $callback_meta['callback'] ) ){
		$callback_args = $arguments;
		if( array_key_exists( 'callback arguments', $callback_meta ) ){
		  $callback_args = array_merge( $arguments, $callback_meta['callback arguments'] );
		}
		call_user_func_array( $callback_meta['callback'], $callback_args );
		unset($callback_args);
	      }
	    }
	    unset( $arguments );
	  }
	}
	  
	  $httpRequest = new CodecsHTTPRequest( $service->getLocation() , CodecsHTTPRequest::METHOD_POST);
	  $httpRequest->putHeader( "ACCEPTS", "application/soap+xml" );
	  $httpRequest->putHeader( "Content-Type", "application/soap+xml" );
	  $httpRequest->setContent( $messageContext->requestXML );
	  
	  $httpResponse = new CodecsBufferedHTTPResponse();
	  
	  try {
	    CodecsHTTPClient::execute($httpRequest, $httpResponse);
	    unset($httpRequest);
	    $messageContext->responseXML = $httpResponse->getBuffer();
	    
	    SAMLServiceLogging::getLogger()->logEvent( new SAMLServiceLogEvent(SAML_SERVICE_LOG_MESSAGE_CONTEXT, $messageContext->authnEntity, $GLOBALS['user'], array( 'messageContext' => $messageContext )) );
	    
	    $message = SAML::fromXML( $httpResponse->getBuffer() );
	    if( $messageContext->response instanceof SOAPMessage ){
	      $responseContext = $messageContext->createChildContext();
	      $responseContext->response = $messageContext->response->getBody();
	      Saml::getInstance()->getService("Sp")->handleAssertionConsumerService( $responseContext );
	      unset($responseContext);
	    }
	    unset($httpResponse);
	  } catch( Exception $e ){
	    watchdog( "saml_service_sp", "Error Authenticating [!message] : !error", array( "!message" => $e->getMessage(), "!error" => "<pre>".$e->getTraceAsString()."</pre>" ), WATCHDOG_DEBUG );
	  }
	
	} unset($idpEncKey);
      }

  }

}
