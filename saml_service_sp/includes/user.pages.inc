<?php


function saml_service_sp_user_login(  ) {
  global $user;
  
  if (!user_is_anonymous()) {
    menu_set_active_item('user/' . $user->uid);
    return menu_execute_active_handler();
  }
  
  if( isset($_GET['saml']) && is_array($_GET['saml']) && isset($_GET['saml']['native_login']) && $_GET['saml']['native_login'] ){
    return drupal_get_form('user_login');
  }

  global $conf;
  $conf['cache'] = false;
  
  $default_authnstatement_handler = SAML::getInstance()->getEntityById(variable_get("saml_service_sp:default_authn_entity",-1));
  if( !$default_authnstatement_handler ){
    module_load_include("pages.inc","user");
    return user_page();
  }
    
  if(
    ( $application_entity = saml_service_is_application_entity() ) &&
    ( $service_entity = $application_entity->getDescriptor(SamlServiceRoleDecriptorFilter::createFilterCallback( "SAML20CodecSPSSODescriptor" )) ) &&
    ( $default_binding = array_shift( array_filter( $service_entity->getAssertionConsumerService() , create_function('$service', 'return $service->isDefault();' )) ) )
  ){
      
    $endPoints = array();
    
    $descriptors = $default_authnstatement_handler->getDescriptor()->getDescriptors();
    reset($descriptors);
    while( list(,$descriptor) = each($descriptors) ){
      if( $descriptor instanceof SAML20CodecIDPSSODescriptor ){
	$endPoints = $descriptor->getSingleSignonService();
	break;
      }
      if( $descriptor instanceof SAML20CodecAuthnAuthorityDescriptor ){
	$endPoints = $descriptor->getAuthnQueryService();
	break;
      }
    }

    
    if( $service = array_shift( array_filter( $endPoints, create_function('$service', 'return $service->getBinding() == "'.$default_binding.'";') ) ) ){    
    
      switch($service->getBinding() ){
	case SAML20Names::BINDINGS_HTTP_REDIRECT: {
	  $url = $service->getLocation();
	  
	  $url.= "?entityId=".$application_entity->getEntityId();
	  
	  $relayState = "";
    
	  if( isset($_REQUEST['destination']) && !empty($_REQUEST['destination']) ){
	    $relayState = $_REQUEST['destination'];
	    unset($_REQUEST['destination']);
	  }
	  
	  if( isset($_REQUEST['edit']['destination']) && !empty($_REQUEST['destination']) ){
	    $relayState = $_REQUEST['edit']['destination'];
	    unset($_REQUEST['edit']['destination']);
	  }
	  
	  if( arg(0) != "user" ){
	    $relayState = $_GET['q'];
	  }
	  
	  if(!empty($relayState)){
	    if( strpos($url, "?") === false ){
	      $url .= "?";
	    }
	    else {
	      $url .= "&";
	    }
	    $url .= "RelayState=".url($relayState, array("absolute"=>true));
	  }
	  
	  while( ob_get_level() != 0 ){
	    ob_end_clean();
	  }
	  
	  $options = drupal_parse_url($url);
	  
	  drupal_goto($options['path'], $options );
	}
      }break;
    }
  }
  
  
  return drupal_get_form('user_login');
}