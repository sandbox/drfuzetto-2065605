<?php


function saml_service_sp_rules_action_saml_session_attribute_add($args, $element) {
  $criteria = array( new SamlAttributeFilterCriterion( $args['identifierName'], $args['identifierValue'] ) );
    
  if(
    ( $hits = array_filter( $_SESSION['saml']['attributes'], SamlAttributeFilter::createFilterCallback( $criteria ) ) ) &&
    count( $hits) == 1 &&
    ( $hit = array_shift( $hits) )
  ){
    return array('variable_added' => array_shift( $hit->getValues() ));
  }
  return array('variable_added' => $args['value']);
}

function saml_service_sp_rules_action_saml_session_attribute_add_info_alter(&$element_info, RulesAbstractPlugin $element) {
  if (isset($element->settings['type']) && $type = $element->settings['type']) {
    $cache = rules_get_cache();
    $type_info = $cache['data_info'][$type];
    $element_info['parameter']['value']['type'] = $type;
    $element_info['provides']['variable_added']['type'] = $type;

    // For lists, we default to an empty list so subsequent actions can add
    // items.
    if (entity_property_list_extract_type($type)) {
      $element_info['parameter']['value']['default value'] = array();
    }
  }
}