<?php

/**
 * Implements hook_rules_file_info() on behalf of the pseudo data module.
 */
function saml_service_sp_rules_file_info() {
  return array('saml_service_sp.eval');
}


/**
 * Implements hook_rules_event_info()
 */
function saml_service_sp_rules_event_info() {
  $defaults = array(
    'group' => t('SAML'),
    'module' => 'saml_service_sp',
  );
  
  return array(
    'saml_attribute_eval' => $defaults+array(
      'label' => t('User Authenticated Via Assertion'),
      'variables' => array(
	'attribute'    => array( 'type' => 'saml_attribute', 'label' => t('Attribute') ),
	'user'       => array( 'type' => 'user', 'label' => t('User') ),
      )
    )
  );
}

/**
 * Implements hook_rules_action_info()
 **/
function saml_service_sp_rules_action_info(){
  
  $return = array();
  
  $return['saml_session_attribute_add'] = array(
    'label' => t('Add a Session Attribute value'),
    'named parameter' => TRUE,
    'parameter' => array(
      'identifierName' => array(
	'type'         => 'text',
	'label'        => t('Identifier'),
	'description'  => t('The type of identifier used for the attribute.'),
	'restriction'  => 'input',
	'options list' => 'saml_service_sp_saml_session_contains_attribute_options'
      ),
      'identifierValue'      => array(
	'type'        => 'text',
	'label'       => t('Identifier Value'),
	'description' => t('The value for the specified identifier.'),
      ),
      'type' => array(
        'type' => 'text',
        'label' => t('Type'),
        'options list' => 'rules_data_action_variable_add_options',
        'description' => t('Specifies the type of the variable that should be added.'),
        'restriction' => 'input',
      ),
      'value' => array(
        'type' => 'unknown',
        'label' => t('Value'),
        'optional' => TRUE,
        'description' => t('Optionally, specify the initial value of the variable.')
      ),
    ),
    'provides' => array(
      'variable_added' => array(
        'type' => 'unknown',
        'label' => t('Added variable'),
      ),
    ),
    'group' => t('SAML'),
    'base' => 'saml_service_sp_rules_action_saml_session_attribute_add',
//     'callbacks' => array(
//       'form_alter' => 'rules_action_type_form_alter',
//     ),
  );
  
  return $return;
}

/**
 * Implements hook_rules_data_info()
 */
function saml_service_sp_rules_data_info() {
  return array(
    'saml_attribute' => array(
      'label' => t('SAML Attribute'),
      'wrap' => TRUE,
      'property info' => array(
	'name' => array(
	  'type' => 'text',
	  'label' => t('The name for the attribute'),
	  'getter callback' => 'saml_service_sp_attribute_get',
	),
	'value' => array(
	  'type' => 'list<text>',
	  'label' => t('The value(s) for the attribute'),
	  'getter callback' => 'saml_service_sp_attribute_get',
	),
	'nameFormat' => array(
	  'type' => 'text',
	  'label' => t('The nameFormat for the attribute'),
	  'getter callback' => 'saml_service_sp_attribute_get',
	),
	'friendlyName' => array(
	  'type' => 'text',
	  'label' => t('The friendlyName for the attribute'),
	  'getter callback' => 'saml_service_sp_attribute_get',
	),
      )
    ),
  );
}


/**
 * Implements hook_rules_condition_info()
 */
function saml_service_sp_rules_condition_info() {
  $conditions = array();

  $conditions['saml_session_contains_attribute'] = array(
    'label'     => t('SAML Session Contains Attribute'),
    'group '    => t('SAML'),
    'help'      => t('Determine if an attribute exists in the active session.'),
    'base'      => 'saml_session_contains_attribute',
    'parameter' => array(
      'identifier' => array(
	'type'         => 'text',
	'label'        => t('Identifier'),
	'description'  => t('The type of identifier used for the attribute.'),
	'restriction'  => 'input',
	'options list' => 'saml_service_sp_saml_session_contains_attribute_options'
      ),
      'value'      => array(
	'type'        => 'text',
	'label'       => t('Identifier Value'),
	'description' => t('The value for the specified identifier.'),
      ),
    ),
  );
  
  $conditions['saml_session_attribute_contains_value'] = array(
    'label'     => t('SAML Session Attribute Contains Value'),
    'group '    => t('SAML'),
    'help'      => t('Determine if an attribute with that contains the specified value exists in the active session.'), 
    'parameter' => array(
      'identifierName' => array(
	'type'         => 'text',
	'label'        => t('Identifier'),
	'description'  => t('The type of identifier used for the attribute.'),
	'restriction'  => 'input',
	'options list' => 'saml_service_sp_saml_session_contains_attribute_options'
      ),
      'identifierValue'      => array(
	'type'        => 'text',
	'label'       => t('Identifier Value'),
	'description' => t('The value for the specified identifier.'),
      ),
      'value'      => array(
	'type'        => 'text',
	'label'       => t('Value'),
	'description' => t('A value that exists for the attribute that matches the identifier.'),
      ),
    ),
  );

  $conditions['saml_attribute_contains_value'] = array(
    'label'     => t('SAML Attribute Value'),
    'group '    => t('SAML'),
    'help'      => t('Determine if an attribute with that contains the specified value exists in the active session.'), 
    'parameter' => array(
      'attribute' => array(
	'type'         => 'saml_attribute',
	'label'        => t('SAML Attribute'),
	'description'  => t('The attribute to evaulate.'),
      ),
      'value'      => array(
	'type'        => 'text',
	'label'       => t('Value'),
	'description' => t('A value that exists for the attribute that matches the identifier.'),
      ),
    ),
  );

  return $conditions;
}

function saml_service_sp_saml_session_contains_attribute_options(){
  return array(
    'name' => t('Name'),
    'nameFormat' => t('NameFormat'),
    'friendlyName' => t('FriendlyName')
  );
}

function saml_session_contains_attribute($identifier, $value, $settings, $state, $element ) {

  if(
    array_key_exists( "saml", $_SESSION ) &&
    is_array( $_SESSION['saml'] ) &&
    array_key_exists( "attributes", $_SESSION ['saml'] ) && 
    is_array($_SESSION ['saml']["attributes"])
  ){
    $criteria = array( new SamlAttributeFilterCriterion( $identifier, $value ) );
    $filtered = array_filter( $_SESSION['saml']['attributes'], SamlAttributeFilter::createFilterCallback( $criteria ) );
    unset($criteria);
    return count( $filtered ) > 0;
  }
}

/**
 * Assert the selected attribute.
 */
function saml_session_contains_attribute_assertions($element) {
  return array( 'saml_attribute' => array( 'type' => 'saml_attribute' ) );
}


function saml_session_attribute_contains_value($identifierName, $identifierValue, $value, $settings, $state, $element ) {
    if(
    array_key_exists( "saml", $_SESSION ) &&
    is_array($_SESSION ['saml']) &&
    array_key_exists( "attribute", $_SESSION ['saml'] ) && 
    is_array($_SESSION ['saml']["attribute"])
  ){
    $criteria = array( new SamlAttributeFilterCriterion( $identifierName, $identifierValue ) );
    
    if( $hits = array_filter( $_SESSION['saml']['attributes'], SamlAttributeFilter::createFilterCallback( $criteria ) ) ){
      foreach( $hits as $hit ){
	if( in_array( $value, $hit->getValues() ) ){
	  return true;
	}
      }
    }
    
    unset($criteria);
  }
}

function saml_attribute_contains_value( $attribute, $value ){
  if( in_array( $value, $attribute->getValues() ) ){
    return true;
  }
}